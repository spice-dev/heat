module hes_mc
    use mpi

    implicit none

    include 'dmumps_struc.h'

    type (DMUMPS_STRUC) :: mumps_par

    contains

    subroutine hes_mc_initialize(MPI_COMM_SOLVER, n_z, n_y, n_cells, matrix_row_i, matrix_col_i, matrix_elem)
        implicit none

        integer, intent(in) :: MPI_COMM_SOLVER
        integer, intent(in) :: n_z, n_y

        integer, intent(in):: n_cells
        integer, dimension(n_cells), intent(in) :: matrix_row_i
        integer, dimension(n_cells), intent(in) :: matrix_col_i
        real*8, dimension(n_cells), intent(in) :: matrix_elem


        mumps_par%JOB = -1
        mumps_par%SYM = 0
        mumps_par%PAR = 1
        mumps_par%COMM = MPI_COMM_SOLVER

        write (*,*) '*** PES *** MUMPS initialisation started'
        call dmumps(mumps_par)

        mumps_par%ICNTL(14) = 100
        mumps_par%ICNTL(1) = -1
        mumps_par%ICNTL(2) = -1
        mumps_par%ICNTL(3) = -1
        mumps_par%ICNTL(4) = -1

        mumps_par%NZ = n_cells
        mumps_par%N = (n_z + 2)*(n_y + 2)

        if (mumps_par%MYID.eq.0) then

            allocate(mumps_par%IRN(mumps_par%NZ))
            allocate(mumps_par%JCN(mumps_par%NZ))
            allocate(mumps_par%A(mumps_par%NZ))
            allocate(mumps_par%RHS(mumps_par%N))
            
            mumps_par%IRN = matrix_row_i
            mumps_par%JCN = matrix_col_i
            mumps_par%RHS = 1
        
        end if

        call hes_mc_restart(n_cells, matrix_elem)
    end subroutine hes_mc_initialize

    

    subroutine hes_mc_restart(n_cells, matrix_elem)
        implicit none

        integer, intent(in):: n_cells
        real*8, dimension(n_cells), intent(in) :: matrix_elem
        
        if (mumps_par%MYID.eq.0) then
            mumps_par%A = matrix_elem
        end if

        mumps_par%JOB = 4
        call dmumps(mumps_par)
        
        write (*,*) '*** PES *** MUMPS inversion done'

    end subroutine hes_mc_restart

    subroutine hes_mc_stop()
        implicit none

        if (mumps_par%MYID .eq. 0 )then
            deallocate(mumps_par%IRN)
            deallocate(mumps_par%JCN)
            deallocate(mumps_par%A)
            deallocate(mumps_par%RHS)
        endif

        mumps_par%JOB = -2
        call dmumps(mumps_par)
    end subroutine hes_mc_stop


    subroutine hes_solve_main(n_z, n_y, right_side, left_side, grid_T_next)
        implicit none

        ! input properties
        integer, intent(in) :: n_z, n_y
        real*8, dimension((n_z + 2)*(n_y + 2)), intent(in) :: right_side
        real*8, dimension((n_z + 2)*(n_y + 2)), intent(out) :: left_side
        real*8, dimension(n_z + 2, n_y + 2), intent(out):: grid_T_next

        integer :: z, y, m_i
        
        left_side = 0
        grid_T_next = 0

        write(*,*) 'Solving'

        if (mumps_par%MYID == 0) then
            mumps_par%RHS = right_side
            
            ! solving call
            mumps_par%JOB = 3
            call dmumps(mumps_par)
            ! solver finished

            left_side = mumps_par%RHS

            write(*,*) 'Reshaping'

            do m_i = 0, (n_z + 2)*(n_y + 2) - 1
                z = floor(real(m_i/(n_y + 2))) + 1
                y = mod(m_i, n_y + 2) + 1
                grid_T_next(z, y) = left_side(m_i + 1)
            enddo
            
        endif
    end subroutine hes_solve_main

    subroutine hes_solve_aux()
        implicit none

        write(*,*) 'Solving auxiliary, ', mumps_par%MYID

        if (mumps_par%MYID .ne. 0) then
            mumps_par%JOB = 3
            call dmumps(mumps_par)
        endif
    end subroutine hes_solve_aux

    subroutine hes_test_subrc(n_cells)
        implicit none
        integer, intent(in):: n_cells
        write(*,*) 'I am working', n_cells
    end subroutine hes_test_subrc
end module
