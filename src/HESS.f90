program HESS
	use getoptions
	use spice_processing
    use spice_types
    use mpi

    implicit none

    !version
    character*10:: version

    !functions
    real*8:: getDebyeLength

    !inputs
    character*2048:: inputFile, outputFile, spiceTFile, spiceOFile, outputFileInit, outputFileTemp, outputFileFinal, restoreFile, scalingFile
    character:: optionKey
    logical :: restore, lightRun, cooling, saveInitFile

    !variables loaded from the input file
    !field and time properties
    real*8:: bulkLength, gapLength, totalLength, debyeMultiple, duration, stepDuration

    integer:: numSteps, saveEvery
    real*8:: initTemp
    integer:: roundingMultiple, boundaryType

    ! boundary conditions handles
    real*8:: boundaryFluxRight, boundaryFluxBottom, boundaryFluxLeft, boundaryTempRight, boundaryTempBottom, boundaryTempLeft, boundaryTopEmissivity
    logical:: boundaryPeriodic, boundaryOverwrite

    !material constants
    !should support variable diffusivity and specific heat sometimes
    character*2048:: materialName
    real*8:: thermalConductivity, specificHeat, density, maxT

    !coating material constants
    !should support variable diffusivity and specific heat sometimes
    integer:: coating
    character*2048:: coatingMaterialName
    real*8:: coatingThermalConductivity, coatingSpecificHeat, coatingDensity, coatingMaxT

    !block align
    ! used in two block scenarios. valid values = 0 (align bottom, default), 1 (align top)
    integer :: align

    ! cooling pipe
    integer :: pipe
    real*8 :: pipe_real_z, pipe_real_y, pipe_real_r, pipe_T

    !plasma properties
    real*8:: mksN0, mksTe, mksB, mksMainIonQ, mksMainIonM

    !spice blocks
    integer:: blockCount
    integer, dimension(99):: leftObjectIds, rightObjectIds, middleObjectIds, fluxSource

    !SPICE properties
    real*8:: deltaY, deltaZ

    ! diagnostics
    integer :: nDiag

    !calculated variables
    !working field parameters, calculated
    real*8:: dX

    integer :: nZSpice, nYSpice, nSpecSpice

    !run parameters and flags
    logical:: correctInit
    integer:: tempCounter
    real*8:: tempStoreReal
    integer:: tempStoreInt
    integer*8:: currentTimeStamp

    ! model options
    logical:: enableSolver, enableScaling

    ! MPI
    integer :: proc_no, proc_max, i_err, par_size, status, threading

    inputFile = 'test.inp'
    outputFile = 'out.mat'
    spiceTFile = 'spicet.mat'
    spiceOFile = 'spiceo.mat'
    outputFileInit = 'oi.mat'
    outputFileTemp = 'ot.mat'
    outputFileFinal = 'of.mat'
    restoreFile = 'rf.mat'
    scalingFile = 'sf.dat'
    optionKey = '0'
    restore = .false.
    lightRun = .false.
    cooling = .false.
    saveInitFile = .true.

    version = '2.0'

    bulkLength = 0
    gapLength = 0
    totalLength = 0
    debyeMultiple = 0
    duration = 0
    stepDuration = 0
    numSteps = 0
    saveEvery = 0
    initTemp = 0
    roundingMultiple = 0
    materialName = 'material'
    thermalConductivity = 0
    specificHeat = 0
    density = 0
    maxT = 0

    mksN0 = 0
    mksTe = 0
    mksB = 0
    mksMainIonQ = 0
    mksMainIonM = 0
    blockCount = 0
    leftObjectIds = 0
    rightObjectIds = 0
    middleObjectIds = 0
    fluxSource = 0
    coating = 0

    align = 0

    deltaY = 0
    deltaZ = 0

    nDiag = 0

    dX = 0

    nZSpice = 0
    nYSpice = 0
    nSpecSpice = 0

    correctInit = 0
    tempCounter = 0
    tempStoreReal = 0
    tempStoreInt = 0

    enableSolver = .true.
    enableScaling = .false.

    call MPI_INIT_THREAD(MPI_THREAD_MULTIPLE, threading, i_err)
    call MPI_COMM_RANK(MPI_COMM_WORLD, proc_no, i_err)
    call MPI_COMM_SIZE(MPI_COMM_WORLD, proc_max, i_err)

    write(*,*) '++++++++++++++++++++++++++++++++++++++++++++++++++++++'
    write(*,*) '+ Heat Equation Solver for SPICE = HESS              +'
    write(*,*) '++++++++++++++++++++++++++++++++++++++++++++++++++++++'

    write(*,*) 'Version '//trim(version)
    write(*,*) '------------------------------------------------------'
    write(*,*) '- Preloading sequence started                        -'
    write(*,*) '------------------------------------------------------'

    if (threading .ne. MPI_THREAD_MULTIPLE) then
        write(*,*) 'Warning: MPI threading was not provided, running non-threaded'
    endif
    write(*,*) 'MPI initalization provided threading', threading
    write(*,*) 'MPI proc no', proc_no
    write(*,*) 'MPI proc max', proc_max


    call gettime(currentTimeStamp)
    write(*,*) 'Timestamp:', currentTimeStamp

!     call UMFTest()
!     write(*,*) 'UMF Test done'

    tempCounter = 0
    do
        optionKey = getopt('i:o:T:O:I:r:s:xSlacf')
        !optionKey = getopt('i:')
        write(*,*) 'Loaded input key '//trim(optionKey)

        if(optionKey.eq.'>') exit

        if(optionKey.eq.'!') then
            write(*,*) 'unknown option: ', trim(optarg)
            stop
        elseif(optionKey.eq.'i') then
        ! Specify input properties from .hin file
            inputFile = trim(optarg)
            write(*,*) 'Model input properties will be read from '//trim(optarg)
            tempCounter = tempCounter + 1
        elseif(optionKey.eq.'o') then
        ! Output file name prefix
            outputFile = trim(optarg)
            outputFileInit = trim(optarg)//'-i.mat'
            outputFileTemp = trim(optarg)//'-t.mat'
            outputFileFinal = trim(optarg)//'-f.mat'
            write(*,*) 'Output will be written to '//trim(outputFileInit)//', '//trim(outputFileTemp)//' and '//trim(outputFileFinal)
            tempCounter = tempCounter + 1
        elseif(optionKey.eq.'T') then
        ! Spice file to load the data from (field specifications)
            spiceTFile = trim(optarg)
            write(*,*) 'SPICE t-file will be loaded from '//trim(optarg)
            tempCounter = tempCounter + 1
        elseif(optionKey.eq.'O') then
        ! Spice file -- fluxes
            spiceOFile = trim(optarg)
            write(*,*) 'SPICE o-file will be loaded from '//trim(optarg)
            tempCounter = tempCounter + 1
        elseif(optionKey.eq.'r') then
        ! Optional: -f file from another simulation to set the initial temperature (has to have correct dimensions else rubbish will be read)
            restore = .true.
            restoreFile = trim(optarg)
            write(*,*) 'Initial temperature will be loaded from '//trim(optarg)
            ! this is omitted due to optionality of the parameter
            !tempCounter = tempCounter + 1
        elseif(optionKey.eq.'s') then
        ! Optional: file which contains the scaling function
            enableScaling = .true.
            scalingFile = trim(optarg)
            write(*,*) 'Scaling function will be loaded from '//trim(scalingFile)
            ! this is omitted due to optionality of the parameter
            !tempCounter = tempCounter + 1
        elseif(optionKey.eq.'x') then
        ! Optional: disable heat equation calculation (creates only an initial file and runs empty cycles)
            enableSolver = .false.
            ! this is omitted due to optionality of the parameter
            !tempCounter = tempCounter + 1
        elseif(optionKey.eq.'S') then
        ! Optional: silences the output (the initial file will not be created)
            saveInitFile = .false.
            ! this is omitted due to optionality of the parameter
            !tempCounter = tempCounter + 1
        elseif(optionKey.eq.'l') then
        ! Optional: silences the output (the initial file will not be created)
            lightRun = .true.
            ! this is omitted due to optionality of the parameter
            !tempCounter = tempCounter + 1
        elseif(optionKey.eq.'c') then
        ! Optional: switches of flux input from SPICE
            cooling = .true.
            ! this is omitted due to optionality of the parameter
            !tempCounter = tempCounter + 1
        endif
    enddo

    if (tempCounter == 4) correctInit = .true.

    ! checks the required parameters
    if (correctInit.eq..false.) then
        write(*,*) 'Wrong number of required input parameters found (', tempCounter, '), should be 4'
        stop
    endif


    ! namelistRead
    ! read model properties
    call readFieldProperties(inputFile, gapLength, totalLength, bulkLength, duration, stepDuration, numSteps, saveEvery, initTemp, roundingMultiple, boundaryType)

    ! read boundary conditions
    if (boundaryType == 1) then
        ! namelistRead
        call readBoundaryConditionsFlux(inputFile, boundaryFluxRight, boundaryFluxBottom, boundaryFluxLeft, boundaryPeriodic, boundaryOverwrite, boundaryTopEmissivity)
    elseif (boundaryType == 2) then
        ! namelistRead
        call readBoundaryConditionsTemperature(inputFile, boundaryTempRight, boundaryTempBottom, boundaryTempLeft, boundaryPeriodic, boundaryOverwrite, boundaryTopEmissivity)
    elseif (boundaryType == 3) then
        ! namelistRead
        call readBoundaryConditionsCombined(inputFile, boundaryFluxRight, boundaryTempBottom, boundaryFluxLeft, boundaryPeriodic, boundaryOverwrite, boundaryTopEmissivity)
    endif

    ! namelistRead
    ! read material properties
    call readMaterialProperties(inputFile, materialName, thermalConductivity, specificHeat, density, maxT)
    ! namelistRead
    ! read the geometry reordering specifications
    call readBlocks(inputFile, blockCount, leftObjectIds, rightObjectIds, middleObjectIds, fluxSource, coating, align, pipe, pipe_real_z, pipe_real_y, pipe_real_r, pipe_T)

    if (coating > 0) then
        call readCoatingProperties(inputFile, coatingMaterialName, coatingThermalConductivity, coatingSpecificHeat, coatingDensity, coatingMaxT)
    endif

    ! matlabInterface
    ! load the field specifications
    call loadTFilePhase1(spiceTFile, nZSpice, nYSpice, debyeMultiple, mksB, mksTe, mksMainIonM, mksMainIonQ, mksN0)
    ! matlabInterface
    ! load the particle data (is needed to have the correct flux data)
    call loadOFilePhase1(spiceOFile, nSpecSpice)

    ! if the blockCount is 1, spice results are transposed
    ! the spice model works 90 deg rotated when this mode of operation was used to do the simulation
    if (blockCount == 1) then
        tempStoreInt = nZSpice
        nZSpice = nYSpice
        nYSpice = tempStoreInt
    endif

    ! cell dimension
    dX = debyeMultiple * getDebyeLength(mksTe, mksN0)

    ! one diagnostic variable (max temperature) will be taken every simulation
    nDiag = nDiag + 1

!
!     do while (mod(nZ, roundingMultiple) .ne. 0)
!         nZ = nZ + 1
!     enddo
!
!     do while (mod(nY, roundingMultiple) .ne. 0)
!         nY = nY + 1
!     enddo
!
    call gettime(currentTimeStamp)
    write(*,*) 'Timestamp:', currentTimeStamp

    write(*,*) '------------------------------------------------------'
    write(*,*) '- Preloading sequence finished                       -'
    write(*,*) '------------------------------------------------------'


    call MPI_BARRIER(MPI_COMM_WORLD, i_err)

    ! HESS
    ! dimensions were loaded, the model results can be processed
    call initHessStage1(inputFile, spiceTFile, spiceOFile, outputFile, outputFileInit, outputFileTemp, outputFileFinal, restoreFile, scalingFile, restore, saveInitFile, enableSolver, enableScaling, cooling, gapLength, totalLength, bulkLength, debyeMultiple, duration, stepDuration, numSteps, saveEvery, initTemp, roundingMultiple, boundaryType, boundaryFluxRight, boundaryFluxBottom, boundaryFluxLeft, boundaryTempRight, boundaryTempBottom, boundaryTempLeft, boundaryPeriodic, boundaryOverwrite, boundaryTopEmissivity, dX, nDiag, mksN0, mksTe, mksB, mksMainIonQ, mksMainIonM, nZSpice, nYSpice, nSpecSpice, blockCount, leftObjectIds, rightObjectIds, middleObjectIds, fluxSource, materialName, thermalConductivity, specificHeat, density, maxT, coating, coatingMaterialName, coatingThermalConductivity, coatingSpecificHeat, coatingDensity, coatingMaxT, align, pipe, pipe_real_z, pipe_real_y, pipe_real_r, pipe_T, proc_no, proc_max)

    ! spice_processing
    call deallocate_arrays(proc_no)

    write(*,*) 'Exiting.'
    call MPI_FINALIZE(i_err)
    write(*,*) 'Done.'

end program HESS

subroutine initHessStage1(inputFile, spiceTFile, spiceOFile, outputFile, outputFileInit, outputFileTemp, outputFileFinal, restoreFile, scalingFile, restore, saveInitFile, enableSolver, enableScaling, cooling, gapLength, totalLength, bulkLength, debyeMultiple, duration, stepDuration, numSteps, saveEvery, initTemp, roundingMultiple, boundaryType, boundaryFluxRight, boundaryFluxBottom, boundaryFluxLeft, boundaryTempRight, boundaryTempBottom, boundaryTempLeft, boundaryPeriodic, boundaryOverwrite, boundaryTopEmissivity, dX, nDiag, mksN0, mksTe, mksB, mksMainIonQ, mksMainIonM, nZSpice, nYSpice, nSpecSpice, blockCount, leftObjectIds, rightObjectIds, middleObjectIds, fluxSource, materialName, thermalConductivity, specificHeat, density, maxT, coating, coatingMaterialName, coatingThermalConductivity, coatingSpecificHeat, coatingDensity, coatingMaxT, align, pipe, pipe_real_z, pipe_real_y, pipe_real_r, pipe_T, proc_no, proc_max)
	use spice_processing
    use spice_types
    use mpi
	implicit none
    ! functions
    ! real*8 :: getDeltaT

    ! parameters
    character*2048, intent(in):: inputFile, spiceTFile, spiceOFile, outputFile, outputFileInit, outputFileTemp, outputFileFinal, restoreFile, scalingFile
    logical, intent(in) :: restore, saveInitFile, cooling, enableSolver, enableScaling

    real*8, intent(in):: gapLength, totalLength, bulkLength, debyeMultiple, duration, stepDuration
    integer, intent(in):: numSteps, saveEvery

    real*8, intent(in):: initTemp
    integer, intent(in):: roundingMultiple, boundaryType

    real*8, intent(in):: boundaryFluxRight, boundaryFluxBottom, boundaryFluxLeft, boundaryTempRight, boundaryTempBottom, boundaryTempLeft, boundaryTopEmissivity
    logical, intent(in):: boundaryPeriodic, boundaryOverwrite

    real*8, intent(in):: dX

    integer, intent(in):: nDiag

    real*8, intent(in):: mksN0, mksTe, mksB, mksMainIonQ, mksMainIonM

    integer, intent(in):: nZSpice, nYSpice, nSpecSpice

    integer, intent(in):: blockCount
    integer, dimension(99), intent(in):: leftObjectIds, rightObjectIds, middleObjectIds, fluxSource


    character*2048, intent(in):: materialName
    real*8, intent(in):: thermalConductivity, specificHeat, density, maxT

    integer, intent(in):: coating
    character*2048, intent(in):: coatingMaterialName
    real*8, intent(in):: coatingThermalConductivity, coatingSpecificHeat, coatingDensity, coatingMaxT

    integer, intent(in) :: align, pipe
    real*8, intent(in) :: pipe_real_z, pipe_real_y, pipe_real_r, pipe_T



    integer, intent(in) :: proc_no, proc_max

    ! automatic determination of the runmode
    ! duration == -1 && stepDuration > 0--> runMode = 1 // run the model until some element starts melting
    ! duration > 0 && numSteps == 0 && stepDuration > 0 --> runMode = 2 // run the model for desired duration using the step length specified in the input file
    ! duration > 0 && numSteps > 0 && stepDuration == 0 --> runMode = 3 // run the model for desired duration using the calculated step length
    ! duration == 0 && numSteps > 0 --> runMode = 4 // run the model for desired number of steps using the step length specified in the input file
    integer:: runMode

    ! modelControls
    integer:: realStepCount
    real*8:: dTime, realDuration
    integer:: nDiagTimes

    ! model grid dimensions
    integer:: nZ, nY, nZFlux, nYFlux

    ! extracted object data
    real*8, dimension(4):: leftBlockBounds, rightBlockBounds, middleBlockBounds
    real*8, dimension(2):: leftBlockModelPosition, rightBlockModelPosition, middleBlockModelPosition

    !temps
    integer:: i, diagCount, i_err
    integer*8 :: currentTimeStamp

    real*8 :: tempDeltaT, tempFluxMax, tempFluxMin

    runMode = 0
    realStepCount = 0
    dTime = 0
    realDuration = 0
    objectsEnumSpice = 0
    edgesSpice = 0
    edgeEnergyFluxSpice = 0
    denormalizedFlux = 0
    massesSpice = 0
    chargesSpice = 0
    nZ = 0
    nY = 0
    nZFlux = 0
    nYFlux = 0
    leftBlockBounds = 0
    rightBlockBounds = 0
    middleBlockBounds = 0
    leftBlockModelPosition = 0
    rightBlockModelPosition = 0
    middleBlockModelPosition = 0
    i = 0
    currentTimeStamp = 0
    tempDeltaT = 0

    write(*,*) '------------------------------------------------------'
    write(*,*) '- Initialization sequence 1 started                  -'
    write(*,*) '------------------------------------------------------'

    call gettime(currentTimeStamp)
    write(*,*) 'Timestamp:', currentTimeStamp

    write(*,*) 'Spice:'
    write(*,*) '    t-input:', trim(spiceOFile)
    write(*,*) '    o-input:', trim(spiceTFile)
    write(*,*) '    Nz:', nZSpice
    write(*,*) '    Ny:', nYSpice
    write(*,*) '    Species:', nSpecSpice

    if (proc_no .eq. 0) then
        ! spice_processing
        call allocate_spice_arrays(proc_no, nSpecSpice, nZSpice, nYSpice)

        ! matlabInterface
        ! loads flux results from the spice models as well as the geometry data
        ! we have to transpose the data if using one block on top
        write(*,*) 'SPICE block count', blockCount
      
        if (blockCount == 1) then
            call loadSpiceResultsTransposed(spiceOFile, spiceTFile, nZSpice, nYSpice, nSpecSpice, objectsEnumSpice, edgesSpice, edgeEnergyFluxSpice, massesSpice, chargesSpice)
        else
            call loadSpiceResults(spiceOFile, spiceTFile, nZSpice, nYSpice, nSpecSpice, objectsEnumSpice, edgesSpice, edgeEnergyFluxSpice, massesSpice, chargesSpice)
        endif

        write(*,*) 'SPICE results loaded'

        if (cooling) then
            write(*,*) 'Cooling switch on. SPICE flux will be ignored.'
            edgeEnergyFluxSpice = 0
        endif

        ! spiceProcessing
        ! denormalizes and adds the simulated flux into the denormalizedFlux array
        call denormalizeEEFlux(nYSpice, nZSpice, nSpecSpice, edgeEnergyFluxSpice, massesSpice, chargesSpice, fluxSource, mksN0, mksTe, mksB, mksMainIonQ, mksMainIonM, denormalizedFlux)
        
        write(*,*) 'SPICE flux processed'

        ! prepare the grids for creation
        if (blockCount.eq.1) then
            write(*,*) 'Block count 1 detected. Getting middle object bounds'
            write(*,*) 'Object ids', middleObjectIds
            call getObjectBounds(nZSpice, nYSpice, middleObjectIds, middleBlockBounds)
        else if (blockCount.eq.2) then
            write(*,*) 'Block count 2 detected. Getting left object bounds'
            write(*,*) 'Object ids', leftObjectIds
            call getObjectBounds(nZSpice, nYSpice, leftObjectIds, leftBlockBounds)
            write(*,*) 'Getting right object bounds'
            write(*,*) 'Object ids', rightObjectIds
            call getObjectBounds(nZSpice, nYSpice, rightObjectIds, rightBlockBounds)
        else
            write(*,*) 'Unsupported value found: blockCount ', blockCount
            write(*,*) 'Aborting execution'
            stop
        endif

        ! HESS
        ! loads the spice block positions in the
        call loadDimensions(gapLength, totalLength, bulkLength, dX, blockCount, leftObjectIds, rightObjectIds, middleObjectIds, leftBlockBounds, rightBlockBounds, middleBlockBounds, align, nZ, nY, nZFlux, nYFlux, leftBlockModelPosition, rightBlockModelPosition, middleBlockModelPosition)

        write(*,*) 'SPICE blocks loaded'
    else
        write (*,*) 'This processor does not take part in the initialization:', proc_no
    endif

    ! Model timings
    ! calculates the number of steps for the simulation
    if ((duration.eq.-1).and.(stepDuration.gt.0)) then
        runMode = 1
        dTime = stepDuration
    elseif ((duration.gt.0).and.(numSteps.eq.0).and.(stepDuration.gt.0)) then
        realStepCount = idint(duration/stepDuration)
        dTime = stepDuration
        if (realStepCount.gt.0) then
            runMode = 2
        endif
    elseif ((duration.gt.0).and.(numSteps.gt.0).and.(stepDuration.eq.0)) then
        realStepCount = numSteps
        dTime = duration/numSteps
        if ((realStepCount.gt.0).and.(dTime.gt.0)) then
            runMode = 3
        endif
    elseif ((duration.eq.0).and.(numSteps.gt.0).and.(stepDuration.gt.0)) then
        realStepCount = numSteps
        dTime = stepDuration
        runMode = 4
    endif

    if (proc_no .eq. 0) then
        !test time stepDuration
        write (*,*) 'Checking maximum temperature difference in flux'
        tempFluxMax = maxval(denormalizedFlux)
        tempFluxMin = minval(denormalizedFlux)

        write (*,*) '    Maximum flux: ', tempFluxMax, getDeltaT(tempFluxMax, density, specificHeat, dTime, dX, 0.0_8, initTemp, initTemp)
        write (*,*) '    Minimum flux: ', tempFluxMin, getDeltaT(tempFluxMin, density, specificHeat, dTime, dX, 0.0_8, initTemp, initTemp)
        tempDeltaT = getDeltaT(tempFluxMax, density, specificHeat, dTime, dX, 0.0_8, initTemp, initTemp)
        

        realDuration = dTime * realStepCount

        nDiagTimes = realStepCount / 5

        nDiagTimes = nDiagTimes + 2


        !test time stepDuration
        write (*,*) 'Checking maximum temperature difference in flux'
        tempFluxMax = maxval(denormalizedFlux)
        tempFluxMin = minval(denormalizedFlux)

        write (*,*) '    Maximum flux: ', tempFluxMax, getDeltaT(tempFluxMax, density, specificHeat, dTime, dX, 0.0_8, initTemp, initTemp)
        write (*,*) '    Minimum flux: ', tempFluxMin, getDeltaT(tempFluxMin, density, specificHeat, dTime, dX, 0.0_8, initTemp, initTemp)
        tempDeltaT = getDeltaT(tempFluxMax, density, specificHeat, dTime, dX, 0.0_8, initTemp, initTemp)

        write(*,*) 'Dimensions:', bulkLength, gapLength, '[y, z] [m]'
        write(*,*) '    dX:', dX, '[m]'
        write(*,*) '    nZ:', nZ
        write(*,*) '    nY:', nY
        write(*,*) '    nZFlux:', nZFlux
        write(*,*) '    nYFlux:', nYFlux

        if (boundaryType == 1) then
            write(*,*) 'Boundary: flux'
            write(*,*) '    left:', boundaryFluxLeft, '[K]'
            write(*,*) '    bottom:', boundaryFluxBottom, '[K]'
            write(*,*) '    right:', boundaryFluxRight, '[K]'
            write(*,*) '    periodic:', boundaryPeriodic, '[K]'
            write(*,*) '    overwrite sides:', boundaryOverwrite, '[K]'
            write(*,*) '    radiative emissivity (top):', boundaryTopEmissivity, '[rel.]'
        elseif (boundaryType == 2) then
            write(*,*) 'Boundary: temperature'
            write(*,*) '    left:', boundaryTempLeft, '[K]'
            write(*,*) '    bottom:', boundaryTempBottom, '[K]'
            write(*,*) '    right:', boundaryTempRight, '[K]'
            write(*,*) '    periodic:', boundaryPeriodic, '[K]'
            write(*,*) '    overwrite sides:', boundaryOverwrite, '[K]'
            write(*,*) '    radiative emissivity (top):', boundaryTopEmissivity, '[rel.]'
        elseif (boundaryType == 3) then
            write(*,*) 'Boundary: combined'
            write(*,*) '    left - flux:', boundaryFluxLeft, '[K]'
            write(*,*) '    bottom:', boundaryTempBottom, '[K]'
            write(*,*) '    right - flux:', boundaryFluxRight, '[K]'
            write(*,*) '    periodic:', boundaryPeriodic, '[K]'
            write(*,*) '    overwrite sides:', boundaryOverwrite, '[K]'
            write(*,*) '    radiative emissivity (top):', boundaryTopEmissivity, '[rel.]'
        else
            write(*,*) 'Boundary: default'
        endif

        write(*,*) 'Material: '//trim(materialName)
        write(*,*) '    thermal conductivity:', thermalConductivity, '[W/m.K]'
        write(*,*) '    specific heat:', specificHeat, '[J/kg.K]'
        write(*,*) '    density:', density, '[kg/m^3]'
        write(*,*) '    maxT:', maxT, '[K] (melting point)'

        write(*,*) 'Plasma:'
        write(*,*) '    particle density:', mksN0, '[1/m^3]'
        write(*,*) '    electron temperature:', mksTe, '[eV]'
        write(*,*) '    magnetic B:', mksB, '[T]'
        write(*,*) '    main ion charge:', mksMainIonQ, '[q0]'
        write(*,*) '    main ion mass:', mksMainIonM, '[mp]'
        write(*,*) '    species count:', nSpecSpice


        write(*,*) 'Time properties initialized into runmode ', runMode
        write(*,*) '    number of steps', realStepCount
        write(*,*) '    step duration', dTime, '[s]'
        write(*,*) '    heating duration', realDuration, '[s]'
        write(*,*) '    diagnostics taken', nDiag, '[counts]'

        call gettime(currentTimeStamp)
        write(*,*) 'Timestamp:', currentTimeStamp

        write(*,*) '------------------------------------------------------'
        write(*,*) '- Initialization sequence 1 finished                 -'
        write(*,*) '------------------------------------------------------'
    else
        write(*,*) 'This processor does not take part in the initialization:', proc_no
    endif 

    call MPI_BARRIER(MPI_COMM_WORLD, i_err)

    ! HESS
    call initHessStage2(outputFile, outputFileInit, outputFileTemp, outputFileFinal, restoreFile, scalingFile, restore, saveInitFile, enableSolver, enableScaling, gapLength, totalLength, bulkLength, debyeMultiple, duration, stepDuration, numSteps, saveEvery, initTemp, boundaryType, boundaryFluxRight, boundaryFluxBottom, boundaryFluxLeft, boundaryTempRight, boundaryTempBottom, boundaryTempLeft, boundaryPeriodic, boundaryOverwrite, boundaryTopEmissivity, dX, nDiag, nDiagTimes, mksN0, mksTe, mksB, mksMainIonQ, mksMainIonM, nZSpice, nYSpice, nSpecSpice, blockCount, leftObjectIds, rightObjectIds, middleObjectIds, materialName, thermalConductivity, specificHeat, density, maxT, coating, coatingMaterialName, coatingThermalConductivity, coatingSpecificHeat, coatingDensity, coatingMaxT, runMode, realStepCount, dTime, realDuration, leftBlockBounds, rightBlockBounds, middleBlockBounds, nZ, nY, nZFlux, nYFlux, leftBlockModelPosition, rightBlockModelPosition, middleBlockModelPosition, pipe, pipe_real_z, pipe_real_y, pipe_real_r, pipe_T, proc_no, proc_max)

    return
end subroutine initHessStage1

subroutine initHessStage2(outputFile, outputFileInit, outputFileTemp, outputFileFinal, restoreFile, scalingFile, restore, saveInitFile, enableSolver, enableScaling, gapLength, totalLength, bulkLength, debyeMultiple, duration, stepDuration, numSteps, saveEvery, initTemp, boundaryType, boundaryFluxRight, boundaryFluxBottom, boundaryFluxLeft, boundaryTempRight, boundaryTempBottom, boundaryTempLeft, boundaryPeriodic, boundaryOverwrite, boundaryTopEmissivity, dX, nDiag, nDiagTimes, mksN0, mksTe, mksB, mksMainIonQ, mksMainIonM, nZSpice, nYSpice, nSpecSpice, blockCount, leftObjectIds, rightObjectIds, middleObjectIds, materialName, thermalConductivity, specificHeat, density, maxT, coating, coatingMaterialName, coatingThermalConductivity, coatingSpecificHeat, coatingDensity, coatingMaxT, runMode, realStepCount, dTime, realDuration, leftBlockBounds, rightBlockBounds, middleBlockBounds, nZ, nY, nZFlux, nYFlux, leftBlockModelPosition, rightBlockModelPosition, middleBlockModelPosition, pipe, pipe_real_z, pipe_real_y, pipe_real_r, pipe_T, proc_no, proc_max)
	use spice_processing
    use spice_types
    use mpi
    implicit none

    character*2048, intent(in):: outputFile, outputFileInit, outputFileTemp, outputFileFinal, restoreFile, scalingFile
    logical, intent(in) :: restore, saveInitFile, enableSolver, enableScaling

    real*8, intent(in):: gapLength, totalLength, bulkLength, debyeMultiple, duration, stepDuration
    integer, intent(in):: numSteps, saveEvery

    real*8, intent(in):: initTemp

    integer, intent(in):: boundaryType

    real*8, intent(in):: boundaryFluxRight, boundaryFluxBottom, boundaryFluxLeft, boundaryTempRight, boundaryTempBottom, boundaryTempLeft, boundaryTopEmissivity
    logical, intent(in):: boundaryPeriodic, boundaryOverwrite

    real*8, intent(in):: dX

    integer, intent(in) :: nDiag, nDiagTimes

    real*8, intent(in):: mksN0, mksTe, mksB, mksMainIonQ, mksMainIonM

    integer, intent(in):: nZSpice, nYSpice, nSpecSpice

    integer, intent(in):: blockCount
    integer, dimension(99), intent(in):: leftObjectIds, rightObjectIds, middleObjectIds

    character*2048, intent(in):: materialName
    real*8, intent(in):: thermalConductivity, specificHeat, density, maxT

    integer, intent(in):: coating
    character*2048, intent(in):: coatingMaterialName
    real*8, intent(in):: coatingThermalConductivity, coatingSpecificHeat, coatingDensity, coatingMaxT

    ! model grid dimensions
    integer, intent(in):: nZ, nY, nZFlux, nYFlux

    ! extracted object data
    real*8, dimension(4), intent(in):: leftBlockBounds, rightBlockBounds, middleBlockBounds
    real*8, dimension(2), intent(in):: leftBlockModelPosition, rightBlockModelPosition, middleBlockModelPosition

    ! pipe
    integer, intent(in) :: pipe
    real*8, intent(in) :: pipe_real_y, pipe_real_z, pipe_real_r, pipe_T

    ! automatic determination of the runmode
    integer, intent(in):: runMode

    ! modelControls
    integer, intent(in):: realStepCount
    real*8, intent(in):: dTime, realDuration

    integer, intent(in) :: proc_no, proc_max

    ! diagnostics
    integer, dimension(nDiagTimes) :: diagnosticTimes

    ! flux scaling

    real*8, dimension(realStepCount) :: scalingFunction

    ! diagnostics
    integer :: diagStep

    ! temp
    integer :: i, i_err
    integer*8 :: currentTimeStamp

    ! pipe auxiliary
    integer :: pipe_z, pipe_y, pipe_r

    diagStep = 0
    diagnosticTimes = 0
    currentTimeStamp = 0
    scalingFunction = 1d0

    write(*,*) '------------------------------------------------------'
    write(*,*) '- Initialization sequence 2 started                  -'
    write(*,*) '------------------------------------------------------'

    call gettime(currentTimeStamp)
    write(*,*) 'Timestamp:', currentTimeStamp

    if (proc_no == 0) then
        call allocate_arrays(proc_no, nZ, nY, nZFlux, nYFlux)
    
        write(*,*) 'Loading scaling function from: ', trim(scalingFile)

        ! dataLoad
        call LoadScalingFunction(scalingFile, realStepCount, dTime, scalingFunction)

        ! initialize diagnostic times

        diagStep = 5

        do i = 0, nDiagTimes - 1
            diagnosticTimes(i + 1) = i * diagStep
        enddo
        write(*,*) 'Diagnostic step: ', diagStep

        write(*,*) 'Diagnostics will be taken at times: ', diagnosticTimes

        ! spiceProcessing
        call copySpiceData(nZSpice, nYSpice, blockCount, leftObjectIds, rightObjectIds, middleObjectIds, nZ, nY, nZFlux, nYFlux, dX, leftBlockBounds, rightBlockBounds, middleBlockBounds, leftBlockModelPosition, rightBlockModelPosition, middleBlockModelPosition, initTemp, boundaryType, boundaryFluxRight, boundaryFluxBottom, boundaryFluxLeft, boundaryTempRight, boundaryTempBottom, boundaryTempLeft, boundaryPeriodic, boundaryOverwrite, boundaryTopEmissivity)

        
        if (pipe > 0) then
            pipe_r = pipe_real_r / dX
            pipe_z = pipe_real_z / dX
            pipe_y = pipe_real_y / dX
            
            call add_cooling_pipe(pipe_y, pipe_z, pipe_r, pipe_T)
        endif

        call allocate_sparse_arrays(proc_no, nZ, nY)

        if (restore) then
            write (*,*) 'Restoring the grid temperature'
            call loadFinalTemperature(restoreFile, nZ, nY, gridTInit)
            write (*,*) 'Temperature restored'
        endif

        call gettime(currentTimeStamp)
        write(*,*) 'Timestamp:', currentTimeStamp
        write(*,*) '------------------------------------------------------'
        write(*,*) '- Initialization sequence 2 finished                 -'
        write(*,*) '------------------------------------------------------'
        
    else
        write(*,*) 'This processor does not take part in the initialization:', proc_no
    endif

    call MPI_BARRIER(MPI_COMM_WORLD, i_err)

    ! HESS
    call initHessStage3(outputFile, outputFileInit, outputFileTemp, outputFileFinal, gapLength, totalLength, bulkLength, debyeMultiple, duration, stepDuration, numSteps, saveEvery, initTemp, dX, nDiag, nDiagTimes, diagnosticTimes, mksN0, mksTe, mksB, mksMainIonQ, mksMainIonM, nZSpice, nYSpice, nSpecSpice, blockCount, leftObjectIds, rightObjectIds, middleObjectIds, materialName, thermalConductivity, specificHeat, density, maxT, coating, coatingMaterialName, coatingThermalConductivity, coatingSpecificHeat, coatingDensity, coatingMaxT, runMode, realStepCount, dTime, realDuration, leftBlockBounds, rightBlockBounds, middleBlockBounds, nZ, nY, nZFlux, nYFlux, leftBlockModelPosition, rightBlockModelPosition, middleBlockModelPosition, scalingFunction, saveInitFile, enableSolver, enableScaling, boundaryPeriodic, proc_no, proc_max)


end subroutine initHessStage2

logical function canContinue(runMode, currentStepNumber, realStepCount, nZ, nY, gridTCurrent, maxT)
    implicit none
    integer, intent(in):: runMode, currentStepNumber, realStepCount, nZ, nY
    real*8, dimension(nZ, nY), intent(in):: gridTCurrent
    real*8, intent(in):: maxT

    canContinue = .false.

    if (runMode.eq.1) then
        ! this should check the temp
        if (currentStepNumber.lt.10) then
            canContinue = .true.
        endif
    elseif (runMode.eq.2) then
        if (currentStepNumber.le.realStepCount) then
            canContinue = .true.
        endif
    elseif (runMode.eq.3) then
        if (currentStepNumber.le.realStepCount) then
            canContinue = .true.
        endif
    elseif (runMode.eq.4) then
        if (currentStepNumber.le.realStepCount) then
            canContinue = .true.
        endif
    endif

end function canContinue

subroutine loadDimensions(gapLength, totalLength, bulkLength, dX, blockCount, leftObjectIds, rightObjectIds, middleObjectIds, leftBlockBounds, rightBlockBounds, middleBlockBounds, align, nZ, nY, nZFlux, nYFlux, leftBlockModelPosition, rightBlockModelPosition, middleBlockModelPosition)
    implicit none

    real*8, intent(inout):: gapLength, totalLength
    real*8, intent(in):: bulkLength

    real*8, intent(in):: dX

    integer, intent(in):: blockCount
    integer, dimension(99), intent(in):: leftObjectIds, rightObjectIds, middleObjectIds

    real*8, dimension(4), intent(in):: leftBlockBounds, rightBlockBounds, middleBlockBounds
    integer, intent(in) :: align

    integer, intent(out):: nZ, nY, nZFlux, nYFlux

    real*8, dimension(2), intent(out):: leftBlockModelPosition, rightBlockModelPosition, middleBlockModelPosition

    integer:: nZRequired, nYRequired
    integer:: nZTemp, nYTemp
    real*8:: lengthZ, lengthY
    integer:: minY, maxY, i, topOffset
    integer:: gapLengthBlocks, bulkLengthBlocks
    integer*8 :: currentTimeStamp

    nZ = 0
    nY = 0
    nZFlux = 0
    nYFlux = 0
    leftBlockModelPosition = 0
    rightBlockModelPosition = 0
    middleBlockModelPosition = 0
    nZRequired = 0
    nYRequired = 0
    nZTemp = 0
    nYTemp = 0
    lengthZ = 0
    lengthY = 0
    minY = 1000000
    maxY = 0
    i = 0
    topOffset = 0
    gapLengthBlocks = 0
    bulkLengthBlocks = 0
    currentTimeStamp = 0

    write(*,*) '......................................................'
    write(*,*) '. Loading dimensions of model grids                  .'
    write(*,*) '......................................................'

    call gettime(currentTimeStamp)
    write(*,*) 'Timestamp:', currentTimeStamp

    if (blockCount.eq.2 .and.totalLength.gt.0) then
        write(*,*) 'Adjusting gap length from', gapLength
        gapLength = max(0.0, totalLength - real(abs(leftBlockBounds(3) - leftBlockBounds(1)) + abs(rightBlockBounds(3) - rightBlockBounds(1))) * dX)
        write(*,*) 'to', gapLength
    endif

    gapLengthBlocks = idint(gapLength / dX)
    bulkLengthBlocks = idint(bulkLength / dX)

    write(*,*) 'Block count', blockCount
    write(*,*) 'Gap between left and right block', gapLength, gapLengthBlocks
    write(*,*) 'Total material length', totalLength
    write(*,*) 'Bulk material size', bulkLength, bulkLengthBlocks
    write(*,*) 'Cell width', dX
    write(*,*) 'Left block', leftBlockBounds
    write(*,*) 'Right block', rightBlockBounds
    write(*,*) 'Middle block', middleBlockBounds

    if (blockCount.eq.1) then
        write(*,*) 'Blockcount 1 detected.'
        write(*,*) 'The model will use the specified middle block from SPICE and it will add the bulk block'
        write(*,*) 'Middle height', middleBlockBounds(3) - middleBlockBounds(1)
        nZRequired = abs(middleBlockBounds(3) - middleBlockBounds(1)) + bulkLengthBlocks
        nYRequired = abs(middleBlockBounds(4) - middleBlockBounds(2))
        middleBlockModelPosition = (/0, 0/)
    else if (blockCount.eq.2) then
        write(*,*) 'Blockcount 2 detected.'
        write(*,*) 'The model will use specified blocks (left and right) from SPICE and it will add the bulk block'
        write(*,*) 'Left height', leftBlockBounds(3) - leftBlockBounds(1)
        write(*,*) 'Right height', rightBlockBounds(3) - rightBlockBounds(1)
        nZRequired = abs(leftBlockBounds(3) - leftBlockBounds(1)) + abs(rightBlockBounds(3) - rightBlockBounds(1)) + gapLengthBlocks

        minY = min(leftBlockBounds(2), leftBlockBounds(4), rightBlockBounds(2), rightBlockBounds(4))
        maxY = max(leftBlockBounds(2), leftBlockBounds(4), rightBlockBounds(2), rightBlockBounds(4))
        write(*,*) 'min Y', minY
        write(*,*) 'max Y', maxY

        topOffset = abs(maxY - minY)
        write(*,*) 'Top offset', topOffset
        nYRequired = topOffset + bulkLengthBlocks

    else
        write(*,*) 'Unsupported value found: blockCount ', blockCount
        write(*,*) 'Aborting execution'
        stop
    endif



    if ((nZRequired.le.0).or.(nYRequired.le.0)) then
        write(*,*) 'Sub-zero dimensions detected.'
        write(*,*) 'nZRequired', nZRequired
        write(*,*) 'nYRequired', nYRequired
        write(*,*) 'Aborting execution.'
        stop
    endif

    nZ = nZRequired! + 2
    nY = nYRequired! + 2

    nZFlux = 2 * nZRequired + 1
    nYFlux = 2 * nYRequired + 1

    write(*,*) 'nZ', nZ
    write(*,*) 'nY', nY
    write(*,*) 'nZFlux', nZFlux
    write(*,*) 'nYFlux', nYFlux

    if (blockCount.eq.1) then
        nZTemp = nZ - abs(middleBlockBounds(3) - middleBlockBounds(1)) + 1
        nYTemp = nY - abs(middleBlockBounds(4) - middleBlockBounds(2)) + 1
        middleBlockModelPosition = (/nZTemp, nYTemp/)
        write(*,*) 'Middle block will be positioned at', middleBlockModelPosition
    else if (blockCount.eq.2) then
        minY = min(leftBlockBounds(2), leftBlockBounds(4), rightBlockBounds(2), rightBlockBounds(4))
        maxY = max(leftBlockBounds(2), leftBlockBounds(4), rightBlockBounds(2), rightBlockBounds(4))
        topOffset = abs(maxY - minY)
        nYRequired = topOffset + idint(bulkLength / dX)

        if (align.eq.1) then
            minY = min(leftBlockBounds(2), leftBlockBounds(4))
            maxY = max(leftBlockBounds(2), leftBlockBounds(4))
            topOffset = abs(maxY - minY)
            nYTemp = nY - topOffset + 1
            leftBlockModelPosition = (/1, nYTemp/)

            minY = min(rightBlockBounds(2), rightBlockBounds(4))
            maxY = max(rightBlockBounds(2), rightBlockBounds(4))
            topOffset = abs(maxY - minY)
            nYTemp = nY - topOffset + 1
            nZTemp = nZ - abs(rightBlockBounds(3) - rightBlockBounds(1)) + 1

            rightBlockModelPosition = (/nZTemp, nYTemp/)
            write(*,*) 'Left block will be positioned at', leftBlockModelPosition
            write(*,*) 'Right block will be positioned at', rightBlockModelPosition
        else
            nYTemp = nY - topOffset + 1

            leftBlockModelPosition = (/1, nYTemp/)

            nZTemp = nZ - abs(rightBlockBounds(3) - rightBlockBounds(1)) + 1

            rightBlockModelPosition = (/nZTemp, nYTemp/)
            write(*,*) 'Left block will be positioned at', leftBlockModelPosition
            write(*,*) 'Right block will be positioned at', rightBlockModelPosition
        endif
    endif


    call gettime(currentTimeStamp)
    write(*,*) 'Timestamp:', currentTimeStamp
    write(*,*) '......................................................'
    write(*,*) '. Model grids dimensions loaded                      .'
    write(*,*) '......................................................'

end subroutine loadDimensions

subroutine initHessStage3(outputFile, outputFileInit, outputFileTemp, outputFileFinal, gapLength, totalLength, bulkLength, debyeMultiple, duration, stepDuration, numSteps, saveEvery, initTemp, dX, nDiag, nDiagTimes, diagnosticTimes, mksN0, mksTe, mksB, mksMainIonQ, mksMainIonM, nZSpice, nYSpice, nSpecSpice, blockCount, leftObjectIds, rightObjectIds, middleObjectIds, materialName, thermalConductivity, specificHeat, density, maxT, coating, coatingMaterialName, coatingThermalConductivity, coatingSpecificHeat, coatingDensity, coatingMaxT, runMode, realStepCount, dTime, realDuration, leftBlockBounds, rightBlockBounds, middleBlockBounds, nZ, nY, nZFlux, nYFlux, leftBlockModelPosition, rightBlockModelPosition, middleBlockModelPosition, scalingFunction, saveInitFile, enableSolver, enableScaling, boundaryPeriodic, proc_no, proc_max)
	use spice_processing
    use spice_types
    use mpi
    implicit none

    character*2048, intent(in):: outputFile, outputFileInit, outputFileTemp, outputFileFinal

    real*8, intent(in):: gapLength, totalLength, bulkLength, debyeMultiple, duration, stepDuration
    integer, intent(in):: numSteps, saveEvery

    real*8, intent(in):: initTemp

    real*8, intent(in):: dX

    integer, intent(in) :: nDiag, nDiagTimes
    integer, dimension(nDiagTimes), intent(in) :: diagnosticTimes

    real*8, intent(in):: mksN0, mksTe, mksB, mksMainIonQ, mksMainIonM

    integer, intent(in):: nZSpice, nYSpice, nSpecSpice

    integer, intent(in):: blockCount
    integer, dimension(99), intent(in):: leftObjectIds, rightObjectIds, middleObjectIds

    character*2048, intent(in):: materialName
    real*8, intent(in):: thermalConductivity, specificHeat, density, maxT

    integer, intent(in):: coating
    character*2048, intent(in):: coatingMaterialName
    real*8, intent(in):: coatingThermalConductivity, coatingSpecificHeat, coatingDensity, coatingMaxT

    ! model grid dimensions
    integer, intent(in):: nZ, nY, nZFlux, nYFlux

    ! extracted object data
    real*8, dimension(4), intent(in):: leftBlockBounds, rightBlockBounds, middleBlockBounds
    real*8, dimension(2), intent(in):: leftBlockModelPosition, rightBlockModelPosition, middleBlockModelPosition

    ! automatic determination of the runmode
    integer, intent(in):: runMode

    ! modelControls
    integer, intent(in):: realStepCount
    real*8, intent(in):: dTime, realDuration

    !scaling
    real*8, dimension(realStepCount), intent(in) :: scalingFunction

    !solver
    logical, intent(in):: saveInitFile, enableSolver, enableScaling, boundaryPeriodic

    !mpi
    integer, intent(in) :: proc_no, proc_max

    !temp
    integer*8 :: currentTimeStamp
    integer :: i_err

    currentTimeStamp = 0


    write(*,*) '------------------------------------------------------'
    write(*,*) '- Initialization sequence 3 started                  -'
    write(*,*) '------------------------------------------------------'

    call gettime(currentTimeStamp)
    write(*,*) 'Timestamp:', currentTimeStamp, proc_no

    if (proc_no == 0) then

        write(*,*) '    Thermal conductivity', thermalConductivity
        write(*,*) '    Material density', density
        write(*,*) '    Material specific heat', specificHeat

        if (coating > 0) then
                write(*,*) 'Coating depth', coating
                write(*,*) '    Thermal conductivity', coatingThermalConductivity
                write(*,*) '    Material density', coatingDensity
                write(*,*) '    Material specific heat', coatingSpecificHeat
            endif

        call initMaterialGrid(nZ, nY, gridObjectFlags, gridMaterial, thermalConductivity, specificHeat, density, coating, coatingThermalConductivity, coatingSpecificHeat, coatingDensity)

        if (enableSolver) then
            ! spiceProcessing
            call getSparseMatrixVectors(nZ, nY, nZFlux, nYFlux, dTime, dX)
        endif

        call gettime(currentTimeStamp)
        write(*,*) 'Timestamp:', currentTimeStamp
        write(*,*) '------------------------------------------------------'
        write(*,*) '- Initialization sequence 3 finished                 -'
        write(*,*) '------------------------------------------------------'

    else
        write(*,*) 'This processor does not take part in the initialization:'
        write(*,*) 'This processor does not take part in the initialization:', proc_no
    endif

    call MPI_BARRIER(MPI_COMM_WORLD, i_err)

    ! HESS
    call modelLoop(outputFile, outputFileInit, outputFileTemp, outputFileFinal, gapLength, totalLength, bulkLength, debyeMultiple, duration, stepDuration, numSteps, saveEvery, initTemp, dX, nDiag, nDiagTimes, diagnosticTimes, mksN0, mksTe, mksB, mksMainIonQ, mksMainIonM, nZSpice, nYSpice, nSpecSpice, blockCount, leftObjectIds, rightObjectIds, middleObjectIds, materialName, thermalConductivity, specificHeat, density, maxT, runMode, realStepCount, dTime, realDuration,  leftBlockBounds, rightBlockBounds, middleBlockBounds, nZ, nY, nZFlux, nYFlux, leftBlockModelPosition, rightBlockModelPosition, middleBlockModelPosition, scalingFunction, saveInitFile, enableSolver, enableScaling, boundaryPeriodic, proc_no, proc_max)

end subroutine initHessStage3


subroutine modelLoop(outputFile, outputFileInit, outputFileTemp, outputFileFinal, gapLength, totalLength, bulkLength, debyeMultiple, duration, stepDuration, numSteps, saveEvery, initTemp, dX, nDiag, nDiagTimes, diagnosticTimes, mksN0, mksTe, mksB, mksMainIonQ, mksMainIonM, nZSpice, nYSpice, nSpecSpice, blockCount, leftObjectIds, rightObjectIds, middleObjectIds, materialName, thermalConductivity, specificHeat, density, maxT, runMode, realStepCount, dTime, realDuration, leftBlockBounds, rightBlockBounds, middleBlockBounds, nZ, nY, nZFlux, nYFlux, leftBlockModelPosition, rightBlockModelPosition, middleBlockModelPosition,  scalingFunction, saveInitFile, enableSolver, enableScaling, boundaryPeriodic, proc_no, proc_max)

    use mpi
    use spice_processing
    use hes_mc
    use hes_uf

    implicit none

    character*2048, intent(in):: outputFile, outputFileInit, outputFileTemp, outputFileFinal

    real*8, intent(in):: gapLength, totalLength, bulkLength, debyeMultiple, duration, stepDuration
    integer, intent(in):: numSteps, saveEvery

    real*8, intent(in):: initTemp

    real*8, intent(in):: dX

    integer, intent(in) :: nDiag, nDiagTimes
    integer, dimension(nDiagTimes), intent(in) :: diagnosticTimes

    real*8, intent(in):: mksN0, mksTe, mksB, mksMainIonQ, mksMainIonM

    integer, intent(in):: nZSpice, nYSpice, nSpecSpice

    integer, intent(in):: blockCount
    integer, dimension(99), intent(in):: leftObjectIds, rightObjectIds, middleObjectIds

    character*2048, intent(in):: materialName
    real*8, intent(in):: thermalConductivity, specificHeat, density, maxT

    ! model grid dimensions
    integer, intent(in):: nZ, nY, nZFlux, nYFlux

    ! extracted object data
    real*8, dimension(4), intent(in):: leftBlockBounds, rightBlockBounds, middleBlockBounds
    real*8, dimension(2), intent(in):: leftBlockModelPosition, rightBlockModelPosition, middleBlockModelPosition

    ! automatic determination of the runmode
    integer, intent(in):: runMode

    ! modelControls
    integer, intent(in):: realStepCount
    real*8, intent(in):: dTime, realDuration

    !scaling
    real*8, dimension(realStepCount), intent(in) :: scalingFunction

    !solver
    logical, intent(in) :: saveInitFile, enableSolver, enableScaling, boundaryPeriodic

    !mpi
    integer, intent(in) :: proc_no, proc_max

    !controls
    ! real*8, dimension((nZ + 2)*(nY + 2)) :: rightSide, leftSide, deltaTData
    ! real*8, dimension(nZ + 2, nY + 2):: gridTCurrent
    ! real*8, dimension(nZ + 2, nY + 2):: gridTNext
    
    real*8, dimension(:), allocatable :: rightSide, leftSide, deltaTData
    real*8, dimension(:,:), allocatable:: gridTCurrent
    real*8, dimension(:,:), allocatable:: gridTNext
    
    integer :: stepIndex, bulkIndex
    character*2048:: outputFileStep

    !diagnostics
    integer :: diagIndex
    real*8, dimension(nDiag, nDiagTimes) :: diagnostics
    real*8 :: currentMaxT
    real*8, dimension(realStepCount) :: time_stats
    real*8, dimension(realStepCount, proc_max) :: vmem_total, rssmem_total
    integer*8, dimension(proc_max) :: vmrss, vmsize

    ! temp
    integer*8 :: currentTimeStamp, nextTimeStamp

    rightSide = 0
    leftSide = 0
    deltaTData = 0
    gridTNext = 0
    stepIndex = 0
    outputFileStep = 'outs.mat'
    diagIndex = 0
    diagnostics = 0
    time_stats = 0

    write(*,*) '------------------------------------------------------'
    write(*,*) '- Entering main model loop                           -'
    write(*,*) '------------------------------------------------------'
    write(*,*) 'Processors (number, max): ', proc_no, proc_max
    write(*,*) 'Steps: ', realStepCount
    call gettime(currentTimeStamp)
    write(*,*) 'Timestamp:', currentTimeStamp

    bulkIndex = maxval(gridObjects)
    write(*,*) 'Bulk object index:', bulkIndex

    if (proc_no == 0) then
        allocate(rightSide((nZ + 2)*(nY + 2)))
        allocate(leftSide((nZ + 2)*(nY + 2)))
        allocate(deltaTData((nZ + 2)*(nY + 2)))
        allocate(gridTCurrent(nZ + 2, nY + 2))
        allocate(gridTNext(nZ + 2, nY + 2))

        gridTCurrent = gridTInit
        gridTNext = gridTInit
    else
        allocate(rightSide((1 + 2)*(1 + 2)))
        allocate(leftSide((1 + 2)*(1 + 2)))
        allocate(deltaTData((1 + 2)*(1 + 2)))
        allocate(gridTCurrent(1 + 2, 1 + 2))
        allocate(gridTNext(1 + 2, 1 + 2))
    endif

    ! HESS
    if (saveInitFile.and.proc_no.eq.0) then
        call saveInitialOutputFile(outputFileInit, gapLength, totalLength, bulkLength, debyeMultiple, duration, stepDuration, numSteps, initTemp, dX, mksN0, mksTe, mksB, mksMainIonQ, mksMainIonM, nZSpice, nYSpice, nSpecSpice, blockCount, leftObjectIds, rightObjectIds, middleObjectIds, materialName, thermalConductivity, specificHeat, density, maxT, objectsEnumSpice, edgesSpice, edgeEnergyFluxSpice, denormalizedFlux, nZ, nY, nZFlux, nYFlux, leftBlockBounds, rightBlockBounds, middleBlockBounds, leftBlockModelPosition, rightBlockModelPosition, middleBlockModelPosition, runMode, realStepCount, dTime, realDuration, gridObjects, gridEdges, gridObjectFlags, gridEdgeFlags, gridEdgeFlux, gridTCurrent, gridTInit, gridEmissivity, gridMaterial, enableScaling, scalingFunction, cellCount, matrix_row_i, matrix_col_i, matrix_elem, aP, aI, aX, sparseMatrix, rightSide)
    endif

    stepIndex = 1

    if (enableSolver) then
        ! HESolver
        if (proc_max .gt. 1) then
            call hes_mc_initialize(MPI_COMM_WORLD, nZ, nY, cellCount, matrix_row_i, matrix_col_i, matrix_elem)
        else
            call HESolverInitialisation(nZ, nY, cellCount, aP, aI, aX)
        endif
        !call UMFTest()

        if (proc_no == 0) then
            ! spiceProcessing
            call getRightSide(nZ, nY, nZFlux, nYFlux, boundaryPeriodic, dTime, dX, initTemp, stepIndex, bulkIndex, realStepCount, gridTCurrent, enableScaling, scalingFunction, rightSide, deltaTData)
        endif
    endif


    call get_cluster_memory_usage_kb(vmrss, vmsize)
    rssmem_total(stepIndex,:) = vmrss
    vmem_total(stepIndex,:) = vmsize

    write (outputFileStep, "(A,A3,I10.10,A4)") trim(outputFile), '-s-', stepIndex, '.mat'


    ! matlabInterface
    if (proc_no .eq. 0) then
        call saveStepOutputFile(outputFileStep, nZ, nY, gridTCurrent, stepIndex, leftSide, rightSide, deltaTData, nDiag, nDiagTimes, diagnostics, dTime, dX)
    endif
    diagIndex = 1

    if (proc_no == 0) then
        currentMaxT = maxval(gridTInit)
        diagnostics(1, diagIndex) = currentMaxT
        write(*,*) 'Maximum value diagnostics: ', currentMaxT
        ! other diagnostics should be added here
        diagIndex = diagIndex + 1;
    endif

    do stepIndex = 2, realStepCount
        write(*,*) 'Calculating step', stepIndex
        call gettime(currentTimeStamp)
        write(*,*) 'Timestamp:', currentTimeStamp
        if (enableSolver) then
            !HESolver
            if (proc_max .gt. 1) then
                if (proc_no == 0) then
                    call hes_solve_main(nZ, nY, rightSide, leftSide, gridTNext)
                else
                    call hes_solve_aux()
                endif
            else
                call HESolverSolve(nZ, nY, rightSide, leftSide, gridTNext)
            endif

            ! spiceProcessing
            if (proc_no == 0) then
                gridTCurrent = gridTNext
                call getRightSide(nZ, nY, nZFlux, nYFlux, boundaryPeriodic, dTime, dX, initTemp, stepIndex, bulkIndex, realStepCount, gridTCurrent, enableScaling, scalingFunction, rightSide, deltaTData)
            endif
        endif

        if (proc_no == 0) then
            if (mod(stepIndex, saveEvery) == 1) then
                write (outputFileStep, "(A,A3,I10.10,A4)") trim(outputFile), '-s-', stepIndex, '.mat'
                ! matlabInterface
                call saveStepOutputFile(outputFileStep, nZ, nY, gridTCurrent, stepIndex, leftSide, rightSide, deltaTData, nDiag, nDiagTimes, diagnostics, dTime, dX)
            endif

            if (stepIndex == diagnosticTimes(diagIndex)) then
                currentMaxT = maxval(gridTCurrent)
                diagnostics(1, diagIndex) = currentMaxT
                write(*,*) 'Maximum value diagnostics: ', currentMaxT
                ! other diagnostics should be added here
                diagIndex = diagIndex + 1;
            endif
        endif

        if (proc_no == 0) then
            if (currentMaxT > maxT) then
                ! exit
            endif
        endif


        write (*,*) 'Step', stepIndex, 'done', mod(stepIndex, 100)
        call gettime(nextTimeStamp)
        write(*,*) 'Timestamp:', nextTimeStamp
        time_stats(stepIndex) = 1.0*(nextTimeStamp-currentTimeStamp)

        call get_cluster_memory_usage_kb(vmrss, vmsize)
        rssmem_total(stepIndex,:) = vmrss
        vmem_total(stepIndex,:) = vmsize
    enddo

    if (enableSolver.and.proc_max.eq.1) then
        call umf4fnum(umfNumeric)
    endif

    ! matlabInterface
    if (proc_no.eq.0) then
        call saveFinalOutputFile(outputFileFinal, nZ, nY, gridTInit, gridTCurrent, leftSide, rightSide, nDiag, nDiagTimes, diagnostics, stepIndex, realStepCount, time_stats, proc_max, rssmem_total, vmem_total)
    endif


    deallocate(rightSide)
    deallocate(leftSide)
    deallocate(deltaTData)
    deallocate(gridTCurrent)
    deallocate(gridTNext)

    write(*,*) 'Loop variables deallocated.'
    
    call gettime(currentTimeStamp)
    write(*,*) 'Timestamp:', currentTimeStamp
    write(*,*) '------------------------------------------------------'
    write(*,*) '- Main model loop complete.                          -'
    write(*,*) '------------------------------------------------------'
    
    if (proc_max .gt. 1) then
        call hes_mc_stop()
    endif

end subroutine modelLoop
