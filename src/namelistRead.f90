subroutine readFieldProperties(inputFile, gapLength, totalLength, bulkLength, duration, stepDuration, numSteps, saveEvery, initTemp, roundingMultiple, boundaryType)
    use spice_types
    implicit none

    ! Namelist definition
    namelist /field/ gapLength, totalLength, bulkLength, duration, stepDuration, numSteps, saveEvery, initTemp, roundingMultiple, boundaryType

    ! Input variables
    character*2048:: inputFile

    ! Ouptut variables
    real*8, intent(out):: gapLength, totalLength, bulkLength, duration, stepDuration
    integer, intent(out):: numSteps, saveEvery
    real*8, intent(out):: initTemp
    integer, intent(out):: roundingMultiple, boundaryType

    gapLength = 0
    totalLength = -1
    bulkLength = 0
    duration = 0
    stepDuration = 0
    saveEvery = 0
    stepDuration = 0
    initTemp = 0
    roundingMultiple = 0
    boundaryType = 0

    write(*,*) 'Loading field properties'

    open(10, file = trim(inputFile)//char(0), status='unknown')
    read(10, nml = field)
    close(10)

    return
end subroutine readFieldProperties

subroutine readBoundaryConditionsFlux(inputFile, boundaryFluxRight, boundaryFluxBottom, boundaryFluxLeft, boundaryPeriodic, boundaryOverwrite, boundaryTopEmissivity)
    implicit none
    include "dataTypes.h"

    ! Namelist definition
    namelist /flux/ boundaryFluxRight, boundaryFluxBottom, boundaryFluxLeft, boundaryPeriodic, boundaryOverwrite, boundaryTopEmissivity

    ! Input variables
    character*2048:: inputFile

    ! Ouptut variables
    real*8, intent(out):: boundaryFluxRight, boundaryFluxBottom, boundaryFluxLeft
    logical, intent(out):: boundaryPeriodic, boundaryOverwrite
    real*8, intent(out):: boundaryTopEmissivity

    boundaryFluxRight = 0
    boundaryFluxBottom = 0
    boundaryFluxLeft = 0
    boundaryPeriodic = .false.
    boundaryOverwrite = .false.
    boundaryTopEmissivity = 0

    write(*,*) 'Loading boundary conditions (flux)'

    open(10, file = trim(inputFile)//char(0), status='unknown')
    read(10, nml = flux)
    close(10)

    return
end subroutine readBoundaryConditionsFlux

subroutine readBoundaryConditionsTemperature(inputFile, boundaryTempRight, boundaryTempBottom, boundaryTempLeft, boundaryPeriodic, boundaryOverwrite, boundaryTopEmissivity)
    implicit none
    include "dataTypes.h"

    ! Namelist definition
    namelist /temperature/ boundaryTempRight, boundaryTempBottom, boundaryTempLeft, boundaryPeriodic, boundaryOverwrite, boundaryTopEmissivity

    ! Input variables
    character*2048:: inputFile

    ! Ouptut variables
    real*8, intent(out):: boundaryTempRight, boundaryTempBottom, boundaryTempLeft
    logical, intent(out):: boundaryPeriodic, boundaryOverwrite
    real*8, intent(out):: boundaryTopEmissivity

    boundaryTempRight = 0
    boundaryTempBottom = 0
    boundaryTempLeft = 0
    boundaryPeriodic = .false.
    boundaryOverwrite = .false.
    boundaryTopEmissivity = 0

    write(*,*) 'Loading boundary conditions (temperature)'

    open(10, file = trim(inputFile)//char(0), status='unknown')
    read(10, nml = temperature)
    close(10)

    return
end subroutine readBoundaryConditionsTemperature

subroutine readBoundaryConditionsCombined(inputFile, boundaryFluxRight, boundaryTempBottom, boundaryFluxLeft, boundaryPeriodic, boundaryOverwrite, boundaryTopEmissivity)
    implicit none
    include "dataTypes.h"

    ! Namelist definition
    namelist /combined/ boundaryFluxRight, boundaryTempBottom, boundaryFluxLeft, boundaryPeriodic, boundaryOverwrite, boundaryTopEmissivity

    ! Input variables
    character*2048:: inputFile

    ! Ouptut variables
    real*8, intent(out):: boundaryFluxRight, boundaryTempBottom, boundaryFluxLeft
    logical, intent(out):: boundaryPeriodic, boundaryOverwrite
    real*8, intent(out):: boundaryTopEmissivity

    boundaryFluxRight = 0
    boundaryTempBottom = 0
    boundaryFluxLeft = 0
    boundaryPeriodic = .false.
    boundaryOverwrite = .false.
    boundaryTopEmissivity = 0

    write(*,*) 'Loading boundary conditions (combined)'

    open(10, file = trim(inputFile)//char(0), status='unknown')
    read(10, nml = combined)
    close(10)

    return
end subroutine readBoundaryConditionsCombined

subroutine readMaterialProperties(inputFile, materialName, thermalConductivity, specificHeat, density, maxT)
    implicit none
    include "dataTypes.h"

    ! Namelist definition
    namelist /material/ materialName, thermalConductivity, specificHeat, density, maxT

    ! Input variables
    character*2048:: inputFile

    ! Ouptut variables
    character*2048, intent(out):: materialName
    real*8, intent(out):: thermalConductivity, specificHeat, density, maxT

    materialName = 'none'
    thermalConductivity = 0
    specificHeat = 0
    density = 0
    maxT = 0

    write(*,*) 'Loading material properties'
    open(10, file = trim(inputFile)//char(0), status='unknown')
    read(10, nml = material)
    close(10)

    return
end subroutine readMaterialProperties

subroutine readCoatingProperties(inputFile, materialName, thermalConductivity, specificHeat, density, maxT)
    implicit none
    include "dataTypes.h"

    ! Namelist definition
    namelist /coating/ materialName, thermalConductivity, specificHeat, density, maxT

    ! Input variables
    character*2048:: inputFile

    ! Ouptut variables
    character*2048, intent(out):: materialName
    real*8, intent(out):: thermalConductivity, specificHeat, density, maxT

    materialName = 'none'
    thermalConductivity = 0
    specificHeat = 0
    density = 0
    maxT = 0

    write(*,*) 'Loading material properties'
    open(10, file = trim(inputFile)//char(0), status='unknown')
    read(10, nml = coating)
    close(10)

    return
end subroutine readCoatingProperties

subroutine readMksInfo(inputFile, mks_n0, mks_Te, mks_B, mks_main_ion_q, mks_main_ion_m, mks_par1, mks_par2, mks_par3)
    implicit none

    ! Namelist definition
    namelist /mks/ mks_n0, mks_Te, mks_B, mks_main_ion_q, mks_main_ion_m, mks_par1, mks_par2, mks_par3

    ! Input variables
    character*2048:: inputFile

    ! Ouptut variables
    real*8, intent(out):: mks_n0, mks_Te, mks_B, mks_main_ion_q, mks_main_ion_m, mks_par1, mks_par2, mks_par3

    mks_n0 = 0
    mks_Te = 0
    mks_B = 0
    mks_main_ion_q = 0
    mks_main_ion_m = 0
    mks_par1 = 0
    mks_par2 = 0
    mks_par3 = 0

    write(*,*) 'Loading MKS info'
    open(10, file = trim(inputFile)//char(0), status='unknown')
    read(10, nml = mks)
    close(10)

  return
end subroutine readMksInfo

subroutine readBlocks(inputFile, blockCount, leftObjectIds, rightObjectIds, middleObjectIds, fluxSource, coating, align, pipe, pipe_real_z, pipe_real_y, pipe_real_r, pipe_T)
    implicit none

    ! Namelist definition
    namelist /blocks/ blockCount, leftObjectIds, rightObjectIds, middleObjectIds, fluxSource, coating, align, pipe, pipe_real_z, pipe_real_y, pipe_real_r, pipe_T

    ! Input variables
    character*2048:: inputFile

    ! Ouptut variables
    integer, intent(out) :: blockCount
    integer, dimension(99), intent(out) :: leftObjectIds, rightObjectIds, middleObjectIds, fluxSource
    integer, intent(out) :: coating, align, pipe
    real*8, intent(out) :: pipe_real_z, pipe_real_y, pipe_real_r, pipe_T

    blockCount = 0
    leftObjectIds = 0
    rightObjectIds = 0
    middleObjectIds = 0
    coating = 0
    align = 0
    pipe = 0
    pipe_real_z = 0
    pipe_real_y = 0
    pipe_real_r = 0
    pipe_T = 0

    write(*,*) 'Loading blocks'
    open(10, file = trim(inputFile)//char(0), status='unknown')
    read(10, nml = blocks)
    close(10)

  return
end subroutine readBlocks

subroutine readModelDetails(spiceInpFile, deltaZ)
    implicit none

    namelist /plasma/ ksi, tau, mu, P0, PL, alpha_xz, alpha_yz
    namelist /geom/ Ly, Lz, dy, dz, Lz_low, Lz_high, ta, tc, tp, Npc, Npts_ratio, scenario, automatic_ta

    ! Input variables
    character*2048, intent(in):: spiceInpFile

    ! Loading variables
    real*8:: ksi, tau, mu, P0, PL, alpha_xz, alpha_yz

    integer:: Ly, Lz
    real*8:: dy, dz, Lz_low, Lz_high, ta, tc, tp
    integer:: Npc
    real*8:: Npts_ratio
    integer:: scenario, automatic_ta

    !output variables
    real*8, intent(out):: deltaZ
    real*8:: deltaY
    integer:: scenarioType

    integer:: i

    open(10, file = trim(spiceInpFile)//char(0), status='unknown')

    read(10, nml = plasma)
    write(*,*) '% Scenario type'
    if ((P0 .eq. 0) .and. (PL .eq. 0)) then
        scenarioType = 2
    else
        scenarioType = 1
    endif

    read(10, nml = geom)
    deltaZ = dz;
    deltaY = dy;
    write(*,*) '% Geometry properties'
    write(*,*) '%    deltaZ', deltaZ
    write(*,*) '%    deltaY', deltaY



    write(*,*) '%    scenario', scenarioType

    close(10)

  return
end subroutine readModelDetails
