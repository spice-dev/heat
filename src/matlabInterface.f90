subroutine saveTestOutputFile(outputFile, n, cellcount, ap, ai, ax, x, b)
    use matio

    implicit none


    ! System variables

    integer:: useCompression, ierr, version
    character*2048:: outputFile
    type(MAT_T)     :: mat
    type(MATVAR_T)  :: matvar

    ! variables to write

    integer, intent(in) :: n
    integer, intent(in) :: cellcount
    integer, dimension(n + 1), intent(in) :: ap
    integer, dimension(cellcount), intent(in) :: ai
    real*8, dimension(cellcount), intent(in) :: ax
    real*8, dimension(n) :: x
    real*8, dimension(n) :: b

    version = 50
    useCompression = 0

    write (6,*) '%    Saving final input file.'
    write (6,*) '%    Writing results to ',trim(outputFile)

    CALL fmat_loginit('Umftest')
    ! OPEN FILE FOR WRITING
    ierr = fmat_create(trim(outputFile)//char(0), mat)

    ierr = fmat_varcreate('n', n, matvar)
    ierr = fmat_varwrite(mat, matvar, n, useCompression)
    ierr = fmat_varfree(matvar)
    ierr = fmat_varcreate('cellcount', cellcount, matvar)
    ierr = fmat_varwrite(mat, matvar, cellcount, useCompression)
    ierr = fmat_varfree(matvar)
    ierr = fmat_varcreate('ap', ap, matvar)
    ierr = fmat_varwrite(mat, matvar, ap, useCompression)
    ierr = fmat_varfree(matvar)
    ierr = fmat_varcreate('ai', ai, matvar)
    ierr = fmat_varwrite(mat, matvar, ai, useCompression)
    ierr = fmat_varfree(matvar)
    ierr = fmat_varcreate('ax', ax, matvar)
    ierr = fmat_varwrite(mat, matvar, ax, useCompression)
    ierr = fmat_varfree(matvar)
    ierr = fmat_varcreate('x', x, matvar)
    ierr = fmat_varwrite(mat, matvar, x, useCompression)
    ierr = fmat_varfree(matvar)
    ierr = fmat_varcreate('b', b, matvar)
    ierr = fmat_varwrite(mat, matvar, b, useCompression)
    ierr = fmat_varfree(matvar)


    ierr = fmat_close(mat)

    write (6,*) '%    Results written.'

    return
end subroutine saveTestOutputFile

subroutine saveFinalOutputFile(outputFileFinal, nZ, nY, gridTInit, gridTCurrent, leftSide, rightSide, nDiag, nDiagTimes, diagnostics, stepIndex, timeCount, time_stats, proc_max, rssmem_total, vmem_total)
    use matio

    implicit none

    ! System variables

    integer:: useCompression, ierr, version
    character*2048:: outputFileFinal
    type(MAT_T)     :: mat
    type(MATVAR_T)  :: matvar

    ! variables to write

    integer, intent(in):: nZ, nY

    real*8, intent(in), dimension(nZ + 2, nY + 2):: gridTInit, gridTCurrent
    real*8, intent(in), dimension((nZ + 2)*(nY + 2)):: leftSide
    real*8, intent(in), dimension((nZ + 2)*(nY + 2)):: rightSide
    integer, intent(in) :: nDiag, nDiagTimes, timeCount
    real*8, dimension(nDiag, nDiagTimes), intent(in) :: diagnostics
    real*8, intent(in), dimension(timeCount) :: time_stats
    integer, intent(in) :: proc_max
    real*8, intent(in), dimension(timeCount, proc_max) :: rssmem_total, vmem_total

    real*8, dimension(nDiagTimes, nDiag) :: diagnosticsT
    integer :: stepIndex

    version = 50
    useCompression = 0

    diagnosticsT = transpose(diagnostics)

    write (6,*) '%    Saving final input file.'
    write (6,*) '%    Writing results to ',trim(outputFileFinal)

    CALL fmat_loginit('HessResults')
    ! OPEN FILE FOR WRITING
    ierr = fmat_create(trim(outputFileFinal)//char(0), mat)

    !MK changed order of saving to allow restore
    ! first goes the file version
    ierr = fmat_varcreate('version', version, matvar)
    ierr = fmat_varwrite(mat, matvar, version, useCompression)
    ierr = fmat_varfree(matvar)

    ierr = fmat_varcreate('nY', nY, matvar)
    ierr = fmat_varwrite(mat, matvar, nY, useCompression)
    ierr = fmat_varfree(matvar)

    ierr = fmat_varcreate('nZ', nZ, matvar)
    ierr = fmat_varwrite(mat, matvar, nZ, useCompression)
    ierr = fmat_varfree(matvar)

    ierr = fmat_varcreate('gridTInit', gridTInit, matvar)
    ierr = fmat_varwrite(mat, matvar, gridTInit, useCompression)
    ierr = fmat_varfree(matvar)

    ierr = fmat_varcreate('gridTCurrent', gridTCurrent, matvar)
    ierr = fmat_varwrite(mat, matvar, gridTCurrent, useCompression)
    ierr = fmat_varfree(matvar)

    ierr = fmat_varcreate('leftSide', leftSide, matvar)
    ierr = fmat_varwrite(mat, matvar, leftSide, useCompression)
    ierr = fmat_varfree(matvar)

    ierr = fmat_varcreate('rightSide', rightSide, matvar)
    ierr = fmat_varwrite(mat, matvar, rightSide, useCompression)
    ierr = fmat_varfree(matvar)

    ierr = fmat_varcreate('diagnostics', diagnosticsT, matvar)
    ierr = fmat_varwrite(mat, matvar, diagnosticsT, useCompression)
    ierr = fmat_varfree(matvar)

    ierr = fmat_varcreate('stepIndex', stepIndex, matvar)
    ierr = fmat_varwrite(mat, matvar, stepIndex, useCompression)
    ierr = fmat_varfree(matvar)

    ierr = fmat_varcreate('time_stats', time_stats, matvar)
    ierr = fmat_varwrite(mat, matvar, time_stats, useCompression)
    ierr = fmat_varfree(matvar)

    ierr = fmat_varcreate('rssmem_total', rssmem_total, matvar)
    ierr = fmat_varwrite(mat, matvar, rssmem_total, useCompression)
    ierr = fmat_varfree(matvar)

    ierr = fmat_varcreate('vmem_total', vmem_total, matvar)
    ierr = fmat_varwrite(mat, matvar, vmem_total, useCompression)
    ierr = fmat_varfree(matvar)

    ierr = fmat_close(mat)

    write (6,*) '%    Results written.'

    return
end subroutine saveFinalOutputFile

subroutine saveStepOutputFile(outputFileStep, nZ, nY, gridTCurrent, stepIndex, leftSide, rightSide, deltaTData, nDiag, nDiagTimes, diagnostics, dTime, dX)
    use matio

    implicit none

    ! System variables

    integer:: useCompression, ierr, version
    character*2048:: outputFileStep
    character*2048:: varName
    type(MAT_T)     :: mat
    type(MATVAR_T)  :: matvar

    ! variables to write

    integer, intent(in):: nZ, nY

    real*8, intent(in), dimension(nZ + 2, nY + 2):: gridTCurrent
    integer :: stepIndex
    real*8, intent(in), dimension((nZ + 2)*(nY + 2)):: leftSide, rightSide, deltaTData

    integer, intent(in) :: nDiag, nDiagTimes
    real*8, dimension(nDiag, nDiagTimes), intent(in) :: diagnostics
    real*8, intent(in) :: dTime, dX

    real*8, dimension(nDiagTimes, nDiag) :: diagnosticsT

    version = 50
    useCompression = 0

    diagnosticsT = transpose(diagnostics)

    write (6,*) '%    Saving step input file.'
    write (6,*) '%    Writing results to ',trim(outputFileStep)

    CALL fmat_loginit('HessResults')
    ! OPEN FILE FOR WRITING
    ierr = fmat_create(trim(outputFileStep)//char(0), mat)

    !MK changed order of saving to allow restore
    ! first goes the file version
    ierr = fmat_varcreate('version', version, matvar)
    ierr = fmat_varwrite(mat, matvar, version, useCompression)
    ierr = fmat_varfree(matvar)

    !write(varName, '(A,I10.10)') 'gridTCurrent', stepIndex
    !ierr = fmat_varcreate(trim(varName)//char(0), gridTCurrent, matvar)
    ierr = fmat_varcreate('gridTCurrent', gridTCurrent, matvar)
    ierr = fmat_varwrite(mat, matvar, gridTCurrent, useCompression)
    ierr = fmat_varfree(matvar)

    ! write(varName, '(A,I10.10)') 'leftSide', stepIndex
    ! ierr = fmat_varcreate(trim(varName)//char(0), leftSide, matvar)
    ! ierr = fmat_varcreate('leftSide', leftSide, matvar)
    ! ierr = fmat_varwrite(mat, matvar, leftSide, useCompression)
    ! ierr = fmat_varfree(matvar)

    ! write(varName, '(A,I10.10)') 'rightSide', stepIndex
    ! ierr = fmat_varcreate(trim(varName)//char(0), rightSide, matvar)
    ! ierr = fmat_varcreate('rightSide', rightSide, matvar)
    ! ierr = fmat_varwrite(mat, matvar, rightSide, useCompression)
    ! ierr = fmat_varfree(matvar)

    ! write(varName, '(A,I10.10)') 'deltaTData', stepIndex
    ! ierr = fmat_varcreate(trim(varName)//char(0), deltaTData, matvar)
    ! ierr = fmat_varcreate('deltaTData', deltaTData, matvar)
    ! ierr = fmat_varwrite(mat, matvar, deltaTData, useCompression)
    ! ierr = fmat_varfree(matvar)

    ierr = fmat_varcreate('stepIndex', stepIndex, matvar)
    ierr = fmat_varwrite(mat, matvar, stepIndex, useCompression)
    ierr = fmat_varfree(matvar)

    ierr = fmat_varcreate('diagnostics', diagnosticsT, matvar)
     ierr = fmat_varwrite(mat, matvar, diagnosticsT, useCompression)
    ierr = fmat_varfree(matvar)

    ierr = fmat_varcreate('dTime', dTime, matvar)
    ierr = fmat_varwrite(mat, matvar, dTime, useCompression)
    ierr = fmat_varfree(matvar)

    ierr = fmat_varcreate('dX', dX, matvar)
    ierr = fmat_varwrite(mat, matvar, dX, useCompression)
    ierr = fmat_varfree(matvar)

    ierr = fmat_close(mat)

    write (6,*) '%    Results written.'

    return
end subroutine saveStepOutputFile

subroutine saveInitialOutputFile(outputFileInit, gapLength, totalLength, bulkLength, debyeMultiple, duration, stepDuration, numSteps, initTemp, dX, mksN0, mksTe, mksB, mksMainIonQ, mksMainIonM, nZSpice, nYSpice, nSpecSpice, blockCount, leftObjectIds, rightObjectIds, middleObjectIds, materialName, thermalConductivity, specificHeat, density, maxT, objectsEnumSpice, edgesSpice, edgeEnergyFluxSpice, denormalizedFlux, nZ, nY, nZFlux, nYFlux, leftBlockBounds, rightBlockBounds, middleBlockBounds, leftBlockModelPosition, rightBlockModelPosition, middleBlockModelPosition, runMode, realStepCount, dTime, realDuration, gridObjects, gridEdges, gridObjectFlags, gridEdgeFlags, gridEdgeFlux, gridTCurrent, gridTInit, gridEmissivity, gridMaterial, enableScaling, scalingFunction, cellCount, matrix_row_i, matrix_col_i, matrix_elem, aP, aI, aX, sparseMatrix, rightSide)
    use matio

    implicit none

    ! System variables

    integer:: useCompression, ierr, version
    character*2048, intent(in):: outputFileInit
    type(MAT_T)     :: mat
    type(MATVAR_T)  :: matvar

    ! variables to write

    real*8, intent(in):: gapLength, totalLength, bulkLength, debyeMultiple, duration, stepDuration
    integer, intent(in):: numSteps

    real*8, intent(in):: initTemp

    real*8, intent(in):: dX

    real*8, intent(in):: mksN0, mksTe, mksB, mksMainIonQ, mksMainIonM

    integer, intent(in):: nZSpice, nYSpice, nSpecSpice

    integer, intent(in):: blockCount
    integer, dimension(99), intent(in):: leftObjectIds, rightObjectIds, middleObjectIds

    character*2048, intent(in):: materialName
    real*8, intent(in):: thermalConductivity, specificHeat, density, maxT

    !data from spice
    integer, dimension(nZSpice, nYSpice), intent(in):: objectsEnumSpice, edgesSpice
    real*8, dimension(nSpecSpice, nZSpice, nYSpice), intent(in):: edgeEnergyFluxSpice
    real*8, dimension(nZSpice, nYSpice), intent(in):: denormalizedFlux

    ! model grid dimensions
    integer, intent(in):: nZ, nY, nZFlux, nYFlux

    ! extracted object data
    real*8, dimension(4), intent(in):: leftBlockBounds, rightBlockBounds, middleBlockBounds
    real*8, dimension(2), intent(in):: leftBlockModelPosition, rightBlockModelPosition, middleBlockModelPosition

    ! automatic determination of the runmode
    integer, intent(in):: runMode

    ! modelControls
    integer, intent(in):: realStepCount
    real*8, intent(in):: dTime, realDuration


    ! grids
    integer, dimension(nZ + 2, nY + 2), intent(in):: gridObjects, gridEdges, gridObjectFlags, gridEdgeFlags
    real*8, dimension(nZFlux, nYFlux), intent(in):: gridEdgeFlux
    real*8, dimension(nZ + 2, nY + 2), intent(in):: gridTCurrent, gridEmissivity, gridTInit
    real*8, dimension(3, nZ + 2, nY + 2), intent(in):: gridMaterial

    !scaling
    logical, intent(in) :: enableScaling
    real*8, dimension(realStepCount) :: scalingFunction

    ! sparse matrix

    integer, intent(in):: cellCount

    integer, dimension(cellCount), intent(in) :: matrix_row_i
    integer, dimension(cellCount), intent(in) :: matrix_col_i
    real*8, dimension(cellCount), intent(in) :: matrix_elem
    integer*8, intent(in), dimension((nZ + 2)*(nY + 2) + 1) :: aP
    integer*8, intent(in), dimension(cellCount) :: aI
    real*8, intent(in), dimension(cellCount) :: aX
    integer, dimension(cellCount, 7), intent(in) :: sparseMatrix
    real*8, intent(in), dimension((nZ + 2)*(nY + 2)) :: rightSide


    real*8, dimension((nZ + 2)*(nY + 2) + 1) :: aP_r
    real*8, dimension(cellCount) :: aI_r
    integer * 8 :: i

    version = 50
    useCompression = 0
    
    aP_r = 0
    aI_r = 0

    do i = 1, (nZ + 2)*(nY + 2) + 1
        aP_r(i) = 1.0 * aP(i)
    enddo

    do i = 1, cellCount
        aI_r(i) = 1.0 * aI(i)
    enddo


    write (6,*) '%    Saving initial input file.'
    write (6,*) '%    Writing results to ',trim(outputFileInit)

    CALL fmat_loginit('HessResults')
    ! OPEN FILE FOR WRITING
    ierr = fmat_create(trim(outputFileInit)//char(0), mat)

    ierr = fmat_varcreate('gapLength', gapLength, matvar)
    ierr = fmat_varwrite(mat, matvar, gapLength, useCompression)
    ierr = fmat_varfree(matvar)
    ierr = fmat_varcreate('bulkLength', bulkLength, matvar)
    ierr = fmat_varwrite(mat, matvar, bulkLength, useCompression)
    ierr = fmat_varfree(matvar)
    ierr = fmat_varcreate('debyeMultiple', debyeMultiple, matvar)
    ierr = fmat_varwrite(mat, matvar, debyeMultiple, useCompression)
    ierr = fmat_varfree(matvar)
    ierr = fmat_varcreate('duration', duration, matvar)
    ierr = fmat_varwrite(mat, matvar, duration, useCompression)
    ierr = fmat_varfree(matvar)
    ierr = fmat_varcreate('stepDuration', stepDuration, matvar)
    ierr = fmat_varwrite(mat, matvar, stepDuration, useCompression)
    ierr = fmat_varfree(matvar)
    ierr = fmat_varcreate('numSteps', numSteps, matvar)
    ierr = fmat_varwrite(mat, matvar, numSteps, useCompression)
    ierr = fmat_varfree(matvar)
    ierr = fmat_varcreate('initTemp', initTemp, matvar)
    ierr = fmat_varwrite(mat, matvar, initTemp, useCompression)
    ierr = fmat_varfree(matvar)
    ierr = fmat_varcreate('dX', dX, matvar)
    ierr = fmat_varwrite(mat, matvar, dX, useCompression)
    ierr = fmat_varfree(matvar)
    ierr = fmat_varcreate('mksN0', mksN0, matvar)
    ierr = fmat_varwrite(mat, matvar, mksN0, useCompression)
    ierr = fmat_varfree(matvar)
    ierr = fmat_varcreate('mksTe', mksTe, matvar)
    ierr = fmat_varwrite(mat, matvar, mksTe, useCompression)
    ierr = fmat_varfree(matvar)
    ierr = fmat_varcreate('mksB', mksB, matvar)
    ierr = fmat_varwrite(mat, matvar, mksB, useCompression)
    ierr = fmat_varfree(matvar)
    ierr = fmat_varcreate('mksMainIonQ', mksMainIonQ, matvar)
    ierr = fmat_varwrite(mat, matvar, mksMainIonQ, useCompression)
    ierr = fmat_varfree(matvar)
    ierr = fmat_varcreate('mksMainIonM', mksMainIonM, matvar)
    ierr = fmat_varwrite(mat, matvar, mksMainIonM, useCompression)
    ierr = fmat_varfree(matvar)
    ierr = fmat_varcreate('nZSpice', nZSpice, matvar)
    ierr = fmat_varwrite(mat, matvar, nZSpice, useCompression)
    ierr = fmat_varfree(matvar)
    ierr = fmat_varcreate('nYSpice', nYSpice, matvar)
    ierr = fmat_varwrite(mat, matvar, nYSpice, useCompression)
    ierr = fmat_varfree(matvar)
    ierr = fmat_varcreate('nSpecSpice', nSpecSpice, matvar)
    ierr = fmat_varwrite(mat, matvar, nSpecSpice, useCompression)
    ierr = fmat_varfree(matvar)
    ierr = fmat_varcreate('leftObjectIds', leftObjectIds, matvar)
    ierr = fmat_varwrite(mat, matvar, leftObjectIds, useCompression)
    ierr = fmat_varfree(matvar)
    ierr = fmat_varcreate('rightObjectIds', rightObjectIds, matvar)
    ierr = fmat_varwrite(mat, matvar, rightObjectIds, useCompression)
    ierr = fmat_varfree(matvar)
    ierr = fmat_varcreate('middleObjectIds', middleObjectIds, matvar)
    ierr = fmat_varwrite(mat, matvar, middleObjectIds, useCompression)
    ierr = fmat_varfree(matvar)
    ierr = fmat_varcreate('thermalConductivity', thermalConductivity, matvar)
    ierr = fmat_varwrite(mat, matvar, thermalConductivity, useCompression)
    ierr = fmat_varfree(matvar)
    ierr = fmat_varcreate('specificHeat', specificHeat, matvar)
    ierr = fmat_varwrite(mat, matvar, specificHeat, useCompression)
    ierr = fmat_varfree(matvar)
    ierr = fmat_varcreate('density', density, matvar)
    ierr = fmat_varwrite(mat, matvar, density, useCompression)
    ierr = fmat_varfree(matvar)
    ierr = fmat_varcreate('maxT', maxT, matvar)
    ierr = fmat_varwrite(mat, matvar, maxT, useCompression)
    ierr = fmat_varfree(matvar)
    ierr = fmat_varcreate('objectsEnumSpice', objectsEnumSpice, matvar)
    ierr = fmat_varwrite(mat, matvar, objectsEnumSpice, useCompression)
    ierr = fmat_varfree(matvar)
    ierr = fmat_varcreate('edgesSpice', edgesSpice, matvar)
    ierr = fmat_varwrite(mat, matvar, edgesSpice, useCompression)
    ierr = fmat_varfree(matvar)
    ierr = fmat_varcreate('edgeEnergyFluxSpice', edgeEnergyFluxSpice, matvar)
    ierr = fmat_varwrite(mat, matvar, edgeEnergyFluxSpice, useCompression)
    ierr = fmat_varfree(matvar)
    ierr = fmat_varcreate('denormalizedFlux', denormalizedFlux, matvar)
    ierr = fmat_varwrite(mat, matvar, denormalizedFlux, useCompression)
    ierr = fmat_varfree(matvar)
    ierr = fmat_varcreate('nZ', nZ, matvar)
    ierr = fmat_varwrite(mat, matvar, nZ, useCompression)
    ierr = fmat_varfree(matvar)
    ierr = fmat_varcreate('nY', nY, matvar)
    ierr = fmat_varwrite(mat, matvar, nY, useCompression)
    ierr = fmat_varfree(matvar)
    ierr = fmat_varcreate('nZFlux', nZFlux, matvar)
    ierr = fmat_varwrite(mat, matvar, nZFlux, useCompression)
    ierr = fmat_varfree(matvar)
    ierr = fmat_varcreate('nYFlux', nYFlux, matvar)
    ierr = fmat_varwrite(mat, matvar, nYFlux, useCompression)
    ierr = fmat_varfree(matvar)
    ierr = fmat_varcreate('leftBlockBounds', leftBlockBounds, matvar)
    ierr = fmat_varwrite(mat, matvar, leftBlockBounds, useCompression)
    ierr = fmat_varfree(matvar)
    ierr = fmat_varcreate('rightBlockBounds', rightBlockBounds, matvar)
    ierr = fmat_varwrite(mat, matvar, rightBlockBounds, useCompression)
    ierr = fmat_varfree(matvar)
    ierr = fmat_varcreate('middleBlockBounds', middleBlockBounds, matvar)
    ierr = fmat_varwrite(mat, matvar, middleBlockBounds, useCompression)
    ierr = fmat_varfree(matvar)
    ierr = fmat_varcreate('leftBlockModelPosition', leftBlockModelPosition, matvar)
    ierr = fmat_varwrite(mat, matvar, leftBlockModelPosition, useCompression)
    ierr = fmat_varfree(matvar)
    ierr = fmat_varcreate('rightBlockModelPosition', rightBlockModelPosition, matvar)
    ierr = fmat_varwrite(mat, matvar, rightBlockModelPosition, useCompression)
    ierr = fmat_varfree(matvar)
    ierr = fmat_varcreate('middleBlockModelPosition', middleBlockModelPosition, matvar)
    ierr = fmat_varwrite(mat, matvar, middleBlockModelPosition, useCompression)
    ierr = fmat_varfree(matvar)
    ierr = fmat_varcreate('runMode', runMode, matvar)
    ierr = fmat_varwrite(mat, matvar, runMode, useCompression)
    ierr = fmat_varfree(matvar)
    ierr = fmat_varcreate('realStepCount', realStepCount, matvar)
    ierr = fmat_varwrite(mat, matvar, realStepCount, useCompression)
    ierr = fmat_varfree(matvar)
    ierr = fmat_varcreate('dTime', dTime, matvar)
    ierr = fmat_varwrite(mat, matvar, dTime, useCompression)
    ierr = fmat_varfree(matvar)
    ierr = fmat_varcreate('realDuration', realDuration, matvar)
    ierr = fmat_varwrite(mat, matvar, realDuration, useCompression)
    ierr = fmat_varfree(matvar)
    ierr = fmat_varcreate('gridObjects', gridObjects, matvar)
    ierr = fmat_varwrite(mat, matvar, gridObjects, useCompression)
    ierr = fmat_varfree(matvar)
    ierr = fmat_varcreate('gridEdges', gridEdges, matvar)
    ierr = fmat_varwrite(mat, matvar, gridEdges, useCompression)
    ierr = fmat_varfree(matvar)
    ierr = fmat_varcreate('gridObjectFlags', gridObjectFlags, matvar)
    ierr = fmat_varwrite(mat, matvar, gridObjectFlags, useCompression)
    ierr = fmat_varfree(matvar)
    ierr = fmat_varcreate('gridEdgeFlags', gridEdgeFlags, matvar)
    ierr = fmat_varwrite(mat, matvar, gridEdgeFlags, useCompression)
    ierr = fmat_varfree(matvar)
    ierr = fmat_varcreate('gridEdgeFlux', gridEdgeFlux, matvar)
    ierr = fmat_varwrite(mat, matvar, gridEdgeFlux, useCompression)
    ierr = fmat_varfree(matvar)
    ierr = fmat_varcreate('gridTCurrent', gridTCurrent, matvar)
    ierr = fmat_varwrite(mat, matvar, gridTCurrent, useCompression)
    ierr = fmat_varfree(matvar)
    ierr = fmat_varcreate('gridTInit', gridTInit, matvar)
    ierr = fmat_varwrite(mat, matvar, gridTInit, useCompression)
    ierr = fmat_varfree(matvar)
    ierr = fmat_varcreate('gridEmissivity', gridEmissivity, matvar)
    ierr = fmat_varwrite(mat, matvar, gridEmissivity, useCompression)
    ierr = fmat_varfree(matvar)
    ierr = fmat_varcreate('gridThermalConductivity', gridMaterial(1,:,:), matvar)
    ierr = fmat_varwrite(mat, matvar, gridMaterial(1,:,:), useCompression)
    ierr = fmat_varfree(matvar)
    ierr = fmat_varcreate('gridSpecificHeat', gridMaterial(2,:,:), matvar)
    ierr = fmat_varwrite(mat, matvar, gridMaterial(2,:,:), useCompression)
    ierr = fmat_varfree(matvar)
    ierr = fmat_varcreate('gridDensity', gridMaterial(3,:,:), matvar)
    ierr = fmat_varwrite(mat, matvar, gridMaterial(3,:,:), useCompression)
    ierr = fmat_varfree(matvar)

    if (enableScaling) then
        ierr = fmat_varcreate('scalingFunction', scalingFunction, matvar)
        ierr = fmat_varwrite(mat, matvar, scalingFunction, useCompression)
        ierr = fmat_varfree(matvar)
    endif

    ierr = fmat_varcreate('cellCount', cellCount, matvar)
    ierr = fmat_varwrite(mat, matvar, cellCount, useCompression)
    ierr = fmat_varfree(matvar)
    ierr = fmat_varcreate('matrix_col_i', matrix_col_i, matvar)
    ierr = fmat_varwrite(mat, matvar, matrix_row_i, useCompression)
    ierr = fmat_varfree(matvar)
    ierr = fmat_varcreate('matrix_row_i', matrix_row_i, matvar)
    ierr = fmat_varwrite(mat, matvar, matrix_row_i, useCompression)
    ierr = fmat_varfree(matvar)
    ierr = fmat_varcreate('matrix_elem', matrix_elem, matvar)
    ierr = fmat_varwrite(mat, matvar, matrix_elem, useCompression)
    ierr = fmat_varfree(matvar)
    ierr = fmat_varcreate('aP', aP_r, matvar)
    ierr = fmat_varwrite(mat, matvar, aP_r, useCompression)
    ierr = fmat_varfree(matvar)
    ierr = fmat_varcreate('aI', aI_r, matvar)
    ierr = fmat_varwrite(mat, matvar, aI_r, useCompression)
    ierr = fmat_varfree(matvar)
    ierr = fmat_varcreate('aX', aX, matvar)
    ierr = fmat_varwrite(mat, matvar, aX, useCompression)
    ierr = fmat_varfree(matvar)
    ierr = fmat_varcreate('sparseMatrix', sparseMatrix, matvar)
    ierr = fmat_varwrite(mat, matvar, sparseMatrix, useCompression)
    ierr = fmat_varfree(matvar)
    ierr = fmat_varcreate('rightSide', rightSide, matvar)
    ierr = fmat_varwrite(mat, matvar, rightSide, useCompression)
    ierr = fmat_varfree(matvar)

    ierr = fmat_close(mat)

    write (6,*) '%    Results written.'

    return
end subroutine saveInitialOutputFile

subroutine loadTFilePhase1(spiceTFile, nZSpice, nYSpice, debyeMultiple, mksB, mksTe, mksMainIonM, mksMainIonQ, mksN0)
    use matio
    implicit none

    integer:: use_compression, ierr, version
    type(MAT_T):: MAT
    type(MATVAR_T):: MATVAR

    character*255, intent(in):: spiceTFile
    integer, intent(out):: nZSpice, nYSpice
    real*8, intent(out):: debyeMultiple, mksB, mksTe, mksMainIonM, mksMainIonQ, mksn0

    write (*,*) '% Loading input file '//trim(spiceTFile)

    call fmat_loginit('spiceInput')
    ierr = fmat_open(trim(spiceTFile)//char(0), mat_acc_rdonly, MAT)


    ! it is switched in matlab file so we switch it back
    ierr = fmat_varreadinfo(MAT, 'Ny', MATVAR)
    ierr = fmat_varreaddata(MAT, MATVAR, nZSpice)

    ! it is switched in matlab file so we switch it back
    ierr = fmat_varreadinfo(MAT, 'Nz', MATVAR)
    ierr = fmat_varreaddata(MAT, MATVAR, nYSpice)

    ierr = fmat_varreadinfo(MAT, 'dz', MATVAR)
    ierr = fmat_varreaddata(MAT, MATVAR, debyeMultiple)

    ierr = fmat_varreadinfo(MAT, 'mksB', MATVAR)
    ierr = fmat_varreaddata(MAT, MATVAR, mksB)

    ierr = fmat_varreadinfo(MAT, 'mksTe', MATVAR)
    ierr = fmat_varreaddata(MAT, MATVAR, mksTe)

    ierr = fmat_varreadinfo(MAT, 'mksmainionm', MATVAR)
    ierr = fmat_varreaddata(MAT, MATVAR, mksMainIonM)

    ierr = fmat_varreadinfo(MAT, 'mksmainionq', MATVAR)
    ierr = fmat_varreaddata(MAT, MATVAR, mksMainIonQ)

    ierr = fmat_varreadinfo(MAT, 'mksn0', MATVAR)
    ierr = fmat_varreaddata(MAT, MATVAR, mksN0)

    ierr = fmat_close(mat)

    write (*,*) '% Input file loaded.'
end subroutine loadTFilePhase1

subroutine loadOFilePhase1(spiceOFile, nSpecSpice)
    use matio
    implicit none

    integer:: use_compression, ierr, version
    type(MAT_T):: MAT
    type(MATVAR_T):: MATVAR

    character*255, intent(in):: spiceOFile
    integer, intent(out):: nSpecSpice

    write (*,*) '% Loading input file '//trim(spiceOFile)

    call fmat_loginit('spiceInput')
    ierr = fmat_open(trim(spiceOFile)//char(0), mat_acc_rdonly, MAT)

    ierr = fmat_varreadinfo(MAT, 'nspec', MATVAR)
    ierr = fmat_varreaddata(MAT, MATVAR, nSpecSpice)

    ierr = fmat_close(mat)

    write (*,*) '% Input file loaded.'
end subroutine loadOFilePhase1

subroutine loadSpiceResults(spiceOFile, spiceTFile, nZSpice, nYSpice, nSpecSpice, objectsEnumSpice, edgesSpice, edgeEnergyFluxSpice, massesSpice, chargesSpice)
    use matio
    implicit none

    character*255, intent(in):: spiceOFile, spiceTFile

    integer, intent(in):: nZSpice, nYSpice, nSpecSpice

    integer, dimension(nZSpice, nYSpice), intent(out):: objectsEnumSpice, edgesSpice
    real*8, dimension(nSpecSpice, nZSpice, nYSpice), intent(out):: edgeEnergyFluxSpice
    real*8, dimension(nZSpice, nYSpice):: edgeEnergyFlux
    real*8, dimension(nSpecSpice):: massesSpice, chargesSpice

    ! we have to load the transposed arrays first
    integer, dimension(nYSpice, nZSpice):: objectsEnumSpiceT, edgesSpiceT
    real*8, dimension(nSpecSpice, nYSpice, nZSpice):: edgeEnergyFluxSpiceT
    real*8, dimension(nYSpice, nZSpice):: edgeEnergyFluxT

    character*2:: specString

    integer:: use_compression, ierr, version
    type(MAT_T):: MAT
    type(MATVAR_T):: MATVAR

    integer:: i

    i = 0

    objectsEnumSpice = 0
    edgesSpice = 0
    edgeEnergyFluxSpice = 0

    write (*,*) '% Loading input file '//trim(spiceTFile)

    call fmat_loginit('spiceInput')
    ierr = fmat_open(trim(spiceTFile)//char(0), mat_acc_rdonly, MAT)


    ierr = fmat_varreadinfo(MAT, 'objectsenum'//char(0), MATVAR)
    write(*,*) '% Matio error:', ierr, 'VarInfo: ', MATVAR
    ierr = fmat_varreaddata(MAT, MATVAR, objectsEnumSpiceT)
    objectsEnumSpice = transpose(objectsEnumSpiceT)
    !    write(*,*) '% Matio read', objectsEnumSpice

    ierr = fmat_varreadinfo(MAT, 'edges'//char(0), MATVAR)
    write(*,*) '% Matio error:', ierr, 'VarInfo: ', MATVAR
    ierr = fmat_varreaddata(MAT, MATVAR, edgesSpiceT)
    edgesSpice = transpose(edgesSpiceT)
    !    write(*,*) '% Matio read', edgesSpice

    ierr = fmat_varreadinfo(MAT, 'm'//char(0), MATVAR)
    write(*,*) '% Matio error:', ierr, 'VarInfo: ', MATVAR
    ierr = fmat_varreaddata(MAT, MATVAR, massesSpice)

    ierr = fmat_varreadinfo(MAT, 'q'//char(0), MATVAR)
    write(*,*) '% Matio error:', ierr, 'VarInfo: ', MATVAR
    ierr = fmat_varreaddata(MAT, MATVAR, chargesSpice)

    ierr = fmat_close(mat)

    write (*,*) '% Loading input file '//trim(spiceOFile)
    ierr = fmat_open(trim(spiceOFile)//char(0), mat_acc_rdonly, MAT)

    do i = 1, nSpecSpice
        specString = '  '
        write(specString(1:2),'(I2.2)') i

        ierr = fmat_varreadinfo(MAT, 'edgeenergyflux'//specString//char(0), MATVAR)
        write(*,*) '% Matio error:', ierr, 'VarInfo: ', MATVAR
        ierr = fmat_varreaddata(MAT, MATVAR, edgeEnergyFluxT)
        edgeEnergyFlux = transpose(edgeEnergyFluxT)

        edgeEnergyFluxSpice(i,:,:) = edgeEnergyFlux
    enddo

    ierr = fmat_close(mat)

end subroutine loadSpiceResults

subroutine loadSpiceResultsTransposed(spiceOFile, spiceTFile, nZSpice, nYSpice, nSpecSpice, objectsEnumSpice, edgesSpice, edgeEnergyFluxSpice, massesSpice, chargesSpice)
    use matio
    implicit none

    character*255, intent(in):: spiceOFile, spiceTFile

    integer, intent(in):: nZSpice, nYSpice, nSpecSpice

    integer, dimension(nZSpice, nYSpice), intent(out):: objectsEnumSpice, edgesSpice
    real*8, dimension(nSpecSpice, nZSpice, nYSpice), intent(out):: edgeEnergyFluxSpice
    real*8, dimension(nZSpice, nYSpice):: edgeEnergyFlux
    real*8, dimension(nSpecSpice):: massesSpice, chargesSpice

    character*2:: specString

    integer:: use_compression, ierr, version
    type(MAT_T):: MAT
    type(MATVAR_T):: MATVAR

    integer:: i

    objectsEnumSpice = 0
    edgesSpice = 0
    edgeEnergyFluxSpice = 0

    write (*,*) '% Loading input file '//trim(spiceTFile)

    call fmat_loginit('spiceInput')
    ierr = fmat_open(trim(spiceTFile)//char(0), mat_acc_rdonly, MAT)

    ierr = fmat_varreadinfo(MAT, 'objectsenum'//char(0), MATVAR)
    write(*,*) '% Matio error:', ierr, 'VarInfo: ', MATVAR
    ierr = fmat_varreaddata(MAT, MATVAR, objectsEnumSpice)
!    write(*,*) '% Matio read', objectsEnumSpice

    ierr = fmat_varreadinfo(MAT, 'edges'//char(0), MATVAR)
    write(*,*) '% Matio error:', ierr, 'VarInfo: ', MATVAR
    ierr = fmat_varreaddata(MAT, MATVAR, edgesSpice)
!    write(*,*) '% Matio read', edgesSpice

    ierr = fmat_varreadinfo(MAT, 'm'//char(0), MATVAR)
    write(*,*) '% Matio error:', ierr, 'VarInfo: ', MATVAR
    ierr = fmat_varreaddata(MAT, MATVAR, massesSpice)

    ierr = fmat_varreadinfo(MAT, 'q'//char(0), MATVAR)
    write(*,*) '% Matio error:', ierr, 'VarInfo: ', MATVAR
    ierr = fmat_varreaddata(MAT, MATVAR, chargesSpice)

    ierr = fmat_close(mat)

    write (*,*) '% Loading input file '//trim(spiceOFile)
    ierr = fmat_open(trim(spiceOFile)//char(0), mat_acc_rdonly, MAT)

    do i = 1, nSpecSpice
        specString = '  '
        write(specString(1:2),'(I2.2)') i

        ierr = fmat_varreadinfo(MAT, 'edgeenergyflux'//specString//char(0), MATVAR)
        write(*,*) '% Matio error:', ierr, 'VarInfo: ', MATVAR
        ierr = fmat_varreaddata(MAT, MATVAR, edgeEnergyFlux)

        edgeEnergyFluxSpice(i,:,:) = edgeEnergyFlux
    enddo

    ierr = fmat_close(mat)

end subroutine loadSpiceResultsTransposed

subroutine loadFinalTemperature(restoreFile, nZ, nY, gridTInit)
    use matio
    implicit none

    character*2048, intent(in):: restoreFile

    integer, intent(in):: nZ, nY

    integer, dimension(nZ, nY), intent(out):: gridTInit

    integer:: use_compression, ierr, version
    type(MAT_T):: MAT
    type(MATVAR_T):: MATVAR

    write (*,*) '% Loading input file '//trim(restoreFile)

    call fmat_loginit('loadInitT')
    ierr = fmat_open(trim(restoreFile)//char(0), mat_acc_rdonly, MAT)

    ierr = fmat_varreadinfo(MAT, 'gridTCurrent'//char(0), MATVAR)
    write(*,*) '% Matio error:', ierr, 'VarInfo: ', MATVAR
    ierr = fmat_varreaddata(MAT, MATVAR, gridTInit)

end subroutine loadFinalTemperature
