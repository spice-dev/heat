module hes_uf
    implicit none

    ! input properties
    integer*8:: umfNumeric

    contains
    
    subroutine HESolverInitialisation(nZ, nY, cellCount, aP, aI, aX)
        implicit none
        
        integer, intent(in):: nZ, nY, cellCount
        integer*8, dimension((nZ + 2)*(nY + 2) + 1), intent(in) :: aP
        integer*8, dimension(cellCount), intent(in) :: aI
        real*8, dimension(cellCount), intent(in) :: aX
        
        ! umfpack
        integer*8 :: n_elem
        integer*8 :: umfSymbolic
        integer*8 :: umfSys
        integer :: umfStatus
        real*8, dimension(20) :: umfControlLu
        real*8, dimension(90) :: umfInfo
            
    !    external umf4def, umf4sym, umf4pcon, umf4pinf, umf4num, umf4fsym
        
        n_elem = int8((nZ + 2)*(nY + 2));
        
        umfNumeric = 0
        umfSymbolic = 0
        umfSys = 0
        umfStatus = 0
        
        write (*,*) '%    Initializing umfpack solver'
        
        write (*,*) '%        Setting default parameters'
        
            
        call umf4def(umfControlLu)                                           !set the default parameters
        umfControlLu(1) = 3

        call umf4sym(n_elem, n_elem, aP, aI, aX, umfSymbolic, umfControlLu, umfInfo, umfStatus)       !symbolic decomposition
        write(6,*) '        Symbolic decomposition (should be == 0):', umfStatus

        write(6,*) '            Control settings'
        call umf4pcon(umfControlLu)

        write(6,*) '            Output'
        call umf4pinf(umfControlLu, umfInfo)

        call umf4num(aP, aI, aX, umfSymbolic, umfNumeric, umfControlLu, umfInfo, umfStatus)   !numeric computation
        write(6,*) '        Numeric decomposition (should be == 0):', umfStatus

        write(6,*) '            Control settings'
        call umf4pcon(umfControlLu)

        write(6,*) '            Output'
        call umf4pinf(umfControlLu, umfInfo)
        
        call umf4fsym(umfSymbolic)                                         !free the symbolic object
        !-----------------------------------------------
        write(6,*) '        Matrix inversion completed'
        
        write(*,*) "        Numeric object:", umfNumeric
        
        write (*,*) '%    Umfpack solver initialized'

    end subroutine HESolverInitialisation

    subroutine HESolverSolve(nZ, nY, rightSide, leftSide, gridTNext)
        implicit none
        
        !integer :: getMatrixIndex
        
        ! input properties
        integer, intent(in):: nZ, nY
        real*8, dimension((nZ + 2)*(nY + 2)), intent(in):: rightSide    
        
        ! output
        real*8, dimension((nZ + 2)*(nY + 2)), intent(out):: leftSide
        real*8, dimension((nZ + 2), (nY + 2)), intent(out):: gridTNext
        
        ! controls
        integer*8 :: umfSys
        real*8, dimension(20) :: umfControlLu
        real*8, dimension(90) :: umfInfo
        
        integer :: z, y, matrixIndex
        
        gridTNext = 0
        leftSide = 0
        
        umfControlLu = 0
        umfSys = 0
        
        call umf4sol(umfSys, leftSide, rightSide, umfNumeric, umfControlLu, umfInfo)   
        
        call umf4pinf(umfControlLu, umfInfo)    
        
        do matrixIndex = 0, (nZ + 2)*(nY + 2) - 1
            z = floor(real(matrixIndex/(nY + 2))) + 1
            y = mod(matrixIndex, nY + 2) + 1
            gridTNext(z, y) = leftSide(matrixIndex + 1)
        enddo
    !     
    !     write (*,*) 'right'
    !     write (*,*) rightSide
    !     write (*,*) 'left'
    !     write (*,*) leftSide
        
    end subroutine HESolverSolve


    subroutine HESolverSolve2(nZ, nY, cellCount, aP, aI, aX, rightSide, leftSide, gridTNext)
        implicit none
        
        !integer :: getMatrixIndex
        
        ! input properties
        
        integer, intent(in):: nZ, nY, cellCount
        integer*8, dimension((nZ + 2)*(nY + 2) + 1), intent(in) :: aP
        integer*8, dimension(cellCount), intent(in) :: aI
        real*8, dimension(cellCount), intent(in) :: aX
        real*8, dimension((nZ + 2)*(nY + 2)), intent(in):: rightSide    
        
        ! output
        real*8, dimension((nZ + 2)*(nY + 2)), intent(out):: leftSide
        real*8, dimension((nZ + 2), (nY + 2)), intent(out):: gridTNext
        
        ! controls
        integer*8 :: umfSys
        real*8, dimension(20) :: umfControlLu
        real*8, dimension(90) :: umfInfo
        
        integer :: z, y, matrixIndex
        
        gridTNext = 0
        leftSide = 0
        
        umfControlLu = 0
        umfSys = 0
        
        call umf4solr(umfSys, aP, aI, aX, leftSide, rightSide, umfNumeric, umfControlLu, umfInfo)   
        
        call umf4pinf(umfControlLu, umfInfo)    
        
        do matrixIndex = 0, (nZ + 2)*(nY + 2) - 1
            z = floor(real(matrixIndex/(nY + 2))) + 1
            y = mod(matrixIndex, nY + 2) + 1
            gridTNext(z, y) = leftSide(matrixIndex + 1)
        enddo
        
        
    end subroutine HESolverSolve2

    subroutine UMFTest()
        implicit none
        
        integer, parameter :: n = 5
        integer, parameter :: cellCount = 12
        real*8, dimension(n) :: x
        real*8, dimension(n) :: b
        integer, dimension(n + 1) :: ap
        integer, dimension(cellcount) :: ai
        real*8, dimension(cellcount) :: ax
        
        
        ! umfpack
        integer :: umfSymbolic, umfSys, umfStatus
        real*8, dimension(20) :: umfControlLu
        real*8, dimension(90) :: umfInfo
        
        character*2048:: outputFile
        
        external umf4def, umf4sym, umf4pcon, umf4pinf, umf4num, umf4fsym
        
        outputFile = 'umftest.mat'
        x = 0
        ap = (/ 0, 2, 5, 9, 10, 12 /)
        ai = (/ 0, 1, 0, 2, 4, 1, 2, 3, 4, 2, 1, 4 /)
        ax = (/ 2., 3., 3., -1., 4., 4., -3., 1., 2., 2., 6., 1. /)
        b = (/ 8, 45, -3, 3, 19 /)
        
        write (*,*) '%    Initializing umfpack solver'
        
        write (*,*) '%        Setting default parameters'
        
            
        call umf4def(umfControlLu)                                           !set the default parameters

        call umf4sym(n, n, ap, ai, ax, umfSymbolic, umfControlLu, umfInfo, umfStatus)       !symbolic decomposition
        write(6,*) '        Symbolic decomposition (should be == 0):', umfStatus

        write(6,*) '            Control settings'
        call umf4pcon(umfControlLu)

        write(6,*) '            Output'
        call umf4pinf(umfInfo)

        call umf4num(ap, ai, ax, umfSymbolic, umfNumeric, umfControlLu, umfInfo, umfStatus)   !numeric computation
        write(6,*) '        Numeric decomposition (should be == 0):', umfStatus

        write(6,*) '            Control settings'
        call umf4pcon(umfControlLu)

        write(6,*) '            Output'
        call umf4pinf(umfInfo)
        
        call umf4fsym(umfSymbolic)                                         !free the symbolic object
        !-----------------------------------------------2
        write(6,*) '        Matrix inversion completed'
        
        write(*,*) "        Numeric object:"
        
        write (*,*) '%    Umfpack solver initialized'
        
        write (*,*) '%    Solving equation'
        
        call umf4sol(umfSys, x, b, umfNumeric, umfControlLu, umfInfo)         !solve the equation A.xpot=chrg (here: A=numeric)

        write (*,*) '%    Solved equation'
        call umf4pinf(umfControlLu)
        
        call saveTestOutputFile(outputFile, n, cellcount, ap, ai, ax, x, b)
        
        
    end subroutine UMFTest
end module