subroutine LoadScalingFunction(scalingFile, realStepCount, dTime, scalingFunction)
    implicit none
    
    character*2048, intent(in):: scalingFile
    integer, intent(in) :: realStepCount
    real*8, intent(in) :: dTime
    real*8, dimension(realStepCount), intent(out) :: scalingFunction
    
    integer :: mode, statusFlag
    
    real*8, allocatable, dimension(:) :: times
    integer, allocatable, dimension(:) :: steps
    real*8, allocatable, dimension(:) :: values
    
    character*2048:: line
    integer :: linesCount, step, lineIndex, stepIndex, stepSubIndex, stepDifference, currentStepStart, currentStepEnd
    real*8 :: time, value, currentTime, currentScalingPrev, currentScalingNext
    
    linesCount = 0
    scalingFunction = 1
    statusFlag = 0
    step = 0
    time = 0
    value = 0
    stepIndex = 0
    currentStepStart = 0
    currentStepEnd = 0
    
    
    open(9, file = scalingFile, STATUS='OLD', ACTION='READ')
    read(9,*) mode
    
    write (*,*) 'Scaling function'
    write (*,*) 'Read', mode
    
    do 
        read(9,*, iostat = statusFlag) time, value
        !write(*,*) statusFlag, time, value
        if (statusFlag /= 0) exit
        linesCount = linesCount + 1
    enddo
    close(9)
    
    
    write (*,*) 'Lines', linesCount
    
    allocate(times(linesCount), stat = statusFlag)    
    if (statusFlag /= 0) then
        write (*,*) 'Not enough memory'
        stop
    endif
    allocate(steps(linesCount), stat = statusFlag)    
    if (statusFlag /= 0) then
        write (*,*) 'Not enough memory'
        stop
    endif
    allocate(values(linesCount), stat = statusFlag)    
    if (statusFlag /= 0) then
        write (*,*) 'Not enough memory'
        stop
    endif
    
    times = 0
    steps = 0
    values = 0
    lineIndex = 1
    
    open(9, file = scalingFile, STATUS='OLD', ACTION='READ')
    read(9,*) mode
    do 
        if (mode == 1) then
            read(9,*, iostat = statusFlag) step, value
            !write(*,*) statusFlag, time, value
            if (statusFlag /= 0) exit
            steps(lineIndex) = step
        else
            read(9,*, iostat = statusFlag) time, value
            !write(*,*) statusFlag, time, value
            if (statusFlag /= 0) exit
            times(lineIndex) = time
        endif
        values(lineIndex) = value
        lineIndex = lineIndex + 1
    enddo
    close(9)
    
    !call printIntegerMatrix(linesCount, 1, steps)
    !call printRealMatrix(linesCount, 1, times)
    !call printRealMatrix(linesCount, 1, values)
    
    if (linesCount < 2) then
        write (*,*) 'The starting and ending point should be specified'
        stop
    else if (mode == 1 .and. steps(1) /= 1) then
        write (*,*) 'The starting point should be specified'
        stop
    else if ((mode == 2 .or. mode == 3) .and. times(1) /= 0) then
        write (*,*) 'The starting point should be specified'
        stop
    endif
    
    lineIndex = 2
    
    if (mode == 1) then
        write (*,*) 'Generating scaling function by steps'
        do stepIndex = 1, realStepCount
            if (stepIndex >= steps(lineIndex)) lineIndex = lineIndex + 1
                
            if (lineIndex <= linesCount) then
                scalingFunction(stepIndex) = values(lineIndex - 1) + (stepIndex - steps(lineIndex - 1)) * (values(lineIndex) - values(lineIndex - 1))/(steps(lineIndex) - steps(lineIndex - 1))
            else
                lineIndex = linesCount
                scalingFunction(stepIndex) = values(linesCount)
            endif            
        enddo
    elseif (mode == 2 .or. mode == 3) then
        write (*,*) 'Generating scaling function by times (absolute)'
                
        do stepIndex = 1, realStepCount
            if (mode == 2) then
                currentTime = (stepIndex - 1) * dTime
            else if (mode == 3) then
                currentTime = (stepIndex - 1.0) / realStepCount
            endif
            
            if (currentTime >= times(lineIndex)) lineIndex = lineIndex + 1
                
            if (lineIndex <= linesCount) then
                scalingFunction(stepIndex) = values(lineIndex - 1) + (currentTime - times(lineIndex - 1)) * (values(lineIndex) - values(lineIndex - 1))/(times(lineIndex) - times(lineIndex - 1))
                
                !write (*,*) 'Coeff', (values(lineIndex) - values(lineIndex - 1))/(times(lineIndex) - times(lineIndex - 1))
            else
                lineIndex = linesCount
                scalingFunction(stepIndex) = values(linesCount)
            endif            
            !write (*,*) 'Current time', currentTime, scalingFunction(stepIndex)
        enddo
    else
        write (*,*) 'Unexpected error - scaling mode not specified'
        stop
    endif
    
    deallocate(times, stat = statusFlag)
    if (statusFlag /= 0) then
        write (*,*) 'Deallocation failed'
        stop
    endif
    deallocate(steps, stat = statusFlag)
    if (statusFlag /= 0) then
        write (*,*) 'Deallocation failed'
        stop
    endif
    deallocate(values, stat = statusFlag)
    if (statusFlag /= 0) then
        write (*,*) 'Deallocation failed'
        stop
    endif
    
end subroutine LoadScalingFunction
