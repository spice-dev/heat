integer function indexOfInt(element, array, arraySize)
    implicit none

    integer, intent(in):: arraySize
    
    integer, intent(in):: element
    integer, intent(in), dimension(arraySize):: array

    integer :: i, compare

    indexOfInt = -1

    findIndex: do i = 1 , arraySize
        compare = array(i)
        if (compare.eq.element) then
            indexOfInt = i
        endif
    enddo findIndex

    return
end function indexOfInt

real*8 function getMass(massInAmu)
    implicit none
    real*8 :: massInAmu
    real*8 :: amu
    
    amu = 1.660538921e-27
    
    getMass = massInAmu * amu
    
    return
end function getMass

real*8 function getCharge(chargeInQ0)
    implicit none
    real*8 :: chargeInQ0
    real*8 :: q0
    
    q0 = 1.60217646e-19
    
    getCharge = chargeInQ0 * q0
    
    return
end function getCharge

real*8 function getLarmorFrequency(massInAmu, magneticB, chargeInQ0)
    implicit none
    real*8:: getMass, getCharge
    
    real*8 :: massInAmu, mass
    real*8 :: chargeInQ0, charge
    real*8 :: magneticB
    
    mass = getMass(massInAmu)
    charge = getCharge(chargeInQ0)

    getLarmorFrequency = abs(charge) * magneticB / mass
    
    return
end function getLarmorFrequency

real*8 function getKelvins(eV)
    implicit none
    real*8, intent(in) :: eV
    real*8:: kB
    real*8 :: q0
    
    q0 = 1.60217646e-19
    kB = 1.3806503e-23
    
    getKelvins = eV * q0 / kB
    
    return
end function getKelvins

real*8 function getDebyeLength(Te, n0)
    implicit none
    real*8:: getKelvins
    
    real*8, intent(in) :: Te, n0
    
    real*8:: q0, kB, epsilon0, TeK 
    
    q0 = 1.60217646e-19
    kB = 1.3806503e-23
    epsilon0 = 8.854187817e-12
    TeK = getKelvins(Te)
    
    getDebyeLength = sqrt((epsilon0 * kB * TeK) / (q0 * q0 * n0))
    
    return
end function getDebyeLength

integer function checkHalfplane(yA, zA, yB, zB, yC, zC, y1, z1)
    implicit none
    
    real*8:: yA, zA, yB, zB, yC, zC, yT, yTC, a, b, bc
    real*8:: y1, z1
        
    logical:: debug
    debug = .false.

    if (debug) then
        write(*,*) 'A: ', zA, yA
        write(*,*) 'B: ', zB, yB
        write(*,*) 'C: ', zC, yC
        write(*,*) 'X: ', z1, y1
    endif

    if (zb.ne.za) then
        !regular line
        a = (yB - yA)/(zB - zA)
        b = - (yB - yA)/(zB - zA)*zA + yA
        bc = -a*zC + yC    
        yT = a*z1  + b
        yTC = a*z1 + bc

        if (abs(yT - y1).le.abs(yT - yTC).and.((yT.ge.y1.and.yT.ge.yTC).or.(yT.le.y1.and.yT.le.yTC))) then
                checkHalfplane = 1
        else 
                checkHalfplane = 0
        endif
    else
        ! straight line/
        if (sign(1.0, real(zC - zA)).eq.sign(1.0, z1 - real(zA)).and.abs(zC - zA) .ge. abs(z1 - zA)) then
            checkHalfplane = 1
        else 
            checkHalfplane = 0
        endif
    endif
    
    return
end function checkHalfplane

real*8 function crossProduct2D(vectorA, vectorB)
    implicit none
    
    real*8, dimension(2):: vectorA, vectorB
    
    crossProduct2D = 0
    
    crossProduct2D = vectorA(1) * vectorB(2) - vectorA(2) * vectorB(1)
    return
end function crossProduct2D


logical function isInTriangle(y, z, yA, zA, yB, zB, yC, zC)
    implicit none
    logical:: areOnSameSide
    
    real*8:: y, z, yA, zA, yB, zB, yC, zC
    
    real*8, dimension(2):: P, A, B, C
    
    P = (/ y, z/)
    A = (/ yA, zA/)
    B = (/ yB, zB/)
    C = (/ yC, zC/)
    
    isInTriangle = .false.
    
    if (areOnSameSide(P, A, B, C).and.areOnSameSide(P, B, C, A).and.areOnSameSide(P, C, A, B)) then 
        isInTriangle = .true.
    endif
    
    return
end function isInTriangle

logical function areOnSameSide(test1, test2, baselineStart, baselineEnd)
    implicit none
    real*8:: crossProduct2D
    
    real*8, dimension(2):: test1, test2, baselineStart, baselineEnd
    
    real*8, dimension(2):: line1, line2, baseline
    real*8:: cross1, cross2
    
    areOnSameSide = .false.
    
    baseline = (/ baselineEnd(1) - baselineStart(1), baselineEnd(2) - baselineStart(2) /)
    line1 = (/ test1(1) - baselineStart(1), test1(2) - baselineStart(2) /)
    line2 = (/ test2(1) - baselineStart(1), test2(2) - baselineStart(2) /)
    
    cross1 = crossProduct2D(baseline, line1)
    cross2 = crossProduct2D(baseline, line2)
    
    if ((cross1 * cross2).ge.0) then 
        areOnSameSide = .true.
    endif
    
    return
end function areOnSameSide

integer function compareRowsInteger(nCols, rowA, rowB, sortIndexCount, sortIndices, direction)
    implicit none
    ! A < B -> -1
    ! A = B -> 0
    ! A > B -> 1
    integer, intent(in) :: nCols
    integer, dimension(1, nCols), intent(in) :: rowA, rowB
    integer, intent(in) :: sortIndexCount
    integer, dimension(sortIndexCount), intent(in) :: sortIndices
    integer, intent(in) :: direction
    
    integer :: colIndex
    
    compareRowsInteger = 0
    !write(*,*) '        $ Comparing'
    
    do colIndex = 1, sortIndexCount
        if (rowA(1, sortIndices(colIndex)) < rowB(1, sortIndices(colIndex))) then
            compareRowsInteger = -1 * direction
            exit
        elseif (rowA(1, sortIndices(colIndex)) > rowB(1, sortIndices(colIndex))) then
            compareRowsInteger = 1 * direction
            exit
        endif
    enddo
    !write(*,*) '        ', rowA, rowB, compareIntegerRows
    
    return
end function compareRowsInteger

subroutine insertSortInteger(rows, cols, array, sortIndexCount, sortIndices, direction)
    implicit none
    
    integer :: compareRowsInteger
    
    integer, intent(in):: rows, cols
    integer, dimension(rows, cols), intent(inout) :: array
    
    integer, intent(in) :: sortIndexCount
    integer, dimension(sortIndexCount) :: sortIndices
    integer, intent(in) :: direction
    
    
    integer, dimension(1, cols) :: row, comparedRow
    integer :: i, j
    
    do i = 2, rows
        row(1,:) = array(i,:)
        !write(*,*) 'inserting', row(1,:)
        j = i - 1
        comparedRow(1,:) = array(j,:)
        do while ((j > 0).and.(compareRowsInteger(cols, row, comparedRow, sortIndexCount, sortIndices, direction) < 0)) 
            !write(*,*) 'trying', array(j,:)
            array(j + 1,:) = array(j,:)
            j = j - 1
            if (j > 0) then
                comparedRow(1,:) = array(j,:)
            endif
        enddo
        
        array(j + 1,:) = row(1,:)
    enddo
end subroutine insertSortInteger

subroutine mergeInteger(nRowsA, nRowsB, nRowsC, nCols, matrixA, matrixB, matrixC, sortIndexCount, sortIndices, direction)
    implicit none
    
    integer :: compareRowsInteger
    
    integer, intent(in) :: nRowsA, nRowsB, nRowsC, nCols
    integer, intent(in), dimension(nRowsA, nCols) :: matrixA
    integer, intent(in), dimension(nRowsB, nCols) :: matrixB
    integer, intent(inout), dimension(nRowsC, nCols) :: matrixC
    
    integer, intent(in) :: sortIndexCount
    integer, dimension(sortIndexCount), intent(in) :: sortIndices
    integer, intent(in) :: direction
    
    integer :: i, j, k, comparisonResult
    
    i = 1
    j = 1
    k = 1
    
    matrixC = 0
    
    do while (i <= nRowsA .and. j <= nRowsB)
        comparisonResult = compareRowsInteger(nCols, matrixA(i,:), matrixB(j,:), sortIndexCount, sortIndices, direction)
        if (comparisonResult <= 0) then
            matrixC(k,:) = matrixA(i,:)
            i = i + 1
        else
            matrixC(k,:) = matrixB(j,:)
            j = j + 1
        endif
        k = k + 1
    enddo
   
    if (i <= nRowsA) then
        do while (i <= nRowsA)
            matrixC(k,:) = matrixA(i,:)
            i = i + 1
            k = k + 1
        enddo
    endif
    
    if (j <= nRowsB) then
        do while (j <= nRowsB)
            matrixC(k,:) = matrixB(j,:)
            j = j + 1
            k = k + 1
        enddo
    endif
        
!     write (*,*) 'Merging C:'
!     call printMatrix(nRowsC, nCols, matrixC)
end subroutine mergeInteger
  
recursive subroutine mergeSortInteger(nRows, nCols, matrix, tempMatrix, sortIndexCount, sortIndices, direction)
 
    integer :: compareRowsInteger
    
    integer, intent(in) :: nRows
    integer, dimension(nRows, nCols), intent(inout) :: matrix
    integer, dimension((nRows + 1)/2, nCols), intent (out) :: tempMatrix
 
    integer, intent(in) :: sortIndexCount
    integer, dimension(sortIndexCount), intent(in) :: sortIndices
    integer, intent(in) :: direction
 
    integer, dimension(nRows - (nRows + 1)/2, nCols):: tempMatrix1
 
    integer, dimension(1, nCols) :: row
    integer :: nRowsA, nRowsB, comparisonResult
    integer :: i
            
    nRowsA = 0
    nRowsB = 0
    comparisonResult = 0
    
    
    if (nRows < 2) return
    if (nRows == 2) then
        comparisonResult = compareRowsInteger(nCols, matrix(1,:), matrix(2,:), sortIndexCount, sortIndices, direction)
        if (comparisonResult > 0) then
            row(1,:) = matrix(1,:)
            matrix(1,:) = matrix(2,:)
            matrix(2,:) = row(1,:)
        endif
        return
    endif 
   
    nRowsA = (nRows + 1)/2
    nRowsB = nRows - nRowsA
 
    call mergeSortInteger(nRowsA, nCols, matrix(1:nRowsA,:), tempMatrix, sortIndexCount, sortIndices, direction)
        
    call mergeSortInteger(nRowsB, nCols, matrix(nRowsA + 1:nRows,:), tempMatrix, sortIndexCount, sortIndices, direction) 
        
    comparisonResult = compareRowsInteger(nCols, matrix(nRowsA,:), matrix(nRowsA + 1,:), sortIndexCount, sortIndices, direction)
    if (comparisonResult > 0) then
        tempMatrix(1:nRowsA,:) = matrix(1:nRowsA,:)
        tempMatrix1(1:nRowsB,:) = matrix(nRowsA + 1:nRows,:)
      
        call mergeInteger(nRowsA, nRowsB, nRows, nCols, tempMatrix, tempMatrix1, matrix, sortIndexCount, sortIndices, direction, recursionDepth)
    endif

end subroutine mergeSortInteger

subroutine printMatrix(nRows, nCols, matrix)
    implicit none
    integer, intent(in) :: nRows, nCols
    integer, intent(in), dimension(nRows, nCols) :: matrix
    integer :: row, col, recursionDepth
    
    integer :: i
        
    do row = 1, nRows
        do col = 1, nCols
            write (*,'(i$)') matrix(row, col) 
        enddo
        write (*,*) ''
    enddo
end subroutine printMatrix

subroutine printIntegerMatrix(nRows, nCols, matrix)
    implicit none
    integer, intent(in) :: nRows, nCols
    integer, intent(in), dimension(nRows, nCols) :: matrix
    integer :: row, col, recursionDepth
    
    integer :: i
        
    do row = 1, nRows
        do col = 1, nCols
            write (*,'(i$)') matrix(row, col) 
        enddo
        write (*,*) ''
    enddo
end subroutine printIntegerMatrix

subroutine printRealMatrix(nRows, nCols, matrix)
    implicit none
    integer, intent(in) :: nRows, nCols
    real*8, intent(in), dimension(nRows, nCols) :: matrix
    integer :: row, col, recursionDepth
    
    integer :: i
        
    do row = 1, nRows
        do col = 1, nCols
            write (*,'(f$)') matrix(row, col) 
        enddo
        write (*,*) ''
    enddo
end subroutine printRealMatrix
