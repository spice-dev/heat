module spice_processing

    use spice_types

    implicit none


    integer, dimension(:,:), pointer :: gridObjects, gridEdges, gridObjectFlags, gridEdgeFlags
    real*8, dimension(:,:), pointer :: gridEdgeFlux
    real*8, dimension(:,:), pointer :: gridTInit, gridEmissivity
    real*8, dimension(:,:,:), pointer :: gridMaterial

    integer :: dim_z, dim_y

    integer :: cellCount = 0
    integer :: bulkNumber = 0

    integer, dimension(:,:), pointer :: objectsEnumSpice, edgesSpice
    real*8, dimension(:,:,:), pointer :: edgeEnergyFluxSpice
    real*8, dimension(:,:), pointer :: denormalizedFlux
    real*8, dimension(:), pointer :: massesSpice, chargesSpice

    !sparse matrix
    integer, dimension(:), pointer :: matrix_row_i
    integer, dimension(:), pointer :: matrix_col_i
    real*8, dimension(:), pointer :: matrix_elem
    integer*8, dimension(:), pointer :: aP
    integer*8, dimension(:), pointer :: aI
    real*8, dimension(:), pointer :: aX
    integer, dimension(:,:), pointer :: sparseMatrix

    contains

    integer function getMatrixIndex(z, y, nZ, nY)
        integer, intent(in):: z, y, nZ, nY

        getMatrixIndex = (z - 1) * nY + y
        return
    end function getMatrixIndex

    subroutine allocate_spice_arrays(proc_no, nSpecSpice, nZSpice, nYSpice)
        implicit none
        integer, intent(in):: proc_no, nSpecSpice, nZSpice, nYSpice

        if (proc_no.eq.0) then
            allocate(objectsEnumSpice(nZSpice, nYSpice))
            allocate(edgesSpice(nZSpice, nYSpice))

            allocate(edgeEnergyFluxSpice(nSpecSpice, nZSpice, nYSpice))
            
            allocate(denormalizedFlux(nZSpice, nYSpice))

            allocate(massesSpice(nSpecSpice))
            allocate(chargesSpice(nSpecSpice))

            write(*,*) 'SPICE arrays allocated', nSpecSpice, nZSpice, nYSpice
        endif
    end subroutine

    subroutine allocate_arrays(proc_no, nZ, nY, nZFlux, nYFlux)
        implicit none
        integer, intent(in):: proc_no, nZ, nY, nZFlux, nYFlux

        dim_z = nZ
        dim_y = nY

        if (proc_no.eq.0) then
            allocate(gridObjects(nZ + 2, nY + 2))
            allocate(gridEdges(nZ + 2, nY + 2))
            allocate(gridObjectFlags(nZ + 2, nY + 2))
            allocate(gridEdgeFlags(nZ + 2, nY + 2))

            allocate(gridEdgeFlux(nZFlux, nYFlux))
            
            allocate(gridTInit(nZ + 2, nY + 2))
            allocate(gridEmissivity(nZ + 2, nY + 2))
            allocate(gridMaterial(3, nZ + 2, nY + 2))

            write(*,*) 'Grid arrays allocated'
        endif
    end subroutine

    subroutine allocate_sparse_arrays(proc_no, nZ, nY)
        implicit none
        integer, intent(in):: proc_no, nZ, nY
        
        if (proc_no.eq.0) then
            allocate(matrix_row_i(cellCount))
            allocate(matrix_col_i(cellCount))
            allocate(matrix_elem(cellCount))

            allocate(aP((nZ + 2)*(nY + 2) + 1))
            
            allocate(aI(cellCount))
            allocate(aX(cellCount))
            allocate(sparseMatrix(cellCount, 7))

            write(*,*) 'Sparse matrix arrays allocated'
        endif
    end subroutine

    subroutine deallocate_arrays(proc_no)
        implicit none
        integer, intent(in):: proc_no

        if (proc_no.eq.0) then
            deallocate(gridObjects)
            write(*,*) 'gridObjects deallocated'
            deallocate(gridEdges)
            write(*,*) 'gridEdges deallocated'
            deallocate(gridObjectFlags)
            write(*,*) 'gridObjectFlags deallocated'
            deallocate(gridEdgeFlags)
            write(*,*) 'gridEdgeFlags deallocated'

            deallocate(gridEdgeFlux)
            write(*,*) 'gridEdgeFlux deallocated'
            
            deallocate(gridTInit)
            write(*,*) 'gridTInit deallocated'
            deallocate(gridEmissivity)
            write(*,*) 'gridEmissivity deallocated'
            deallocate(gridMaterial)
            write(*,*) 'gridMaterial deallocated'

            deallocate(objectsEnumSpice)
            write(*,*) 'objectsEnumSpice deallocated'
            deallocate(edgesSpice)
            write(*,*) 'edgesSpice deallocated'

            deallocate(edgeEnergyFluxSpice)
            write(*,*) 'edgeEnergyFluxSpice deallocated'
            
            deallocate(denormalizedFlux)
            write(*,*) 'denormalizedFlux deallocated'

            deallocate(massesSpice)
            write(*,*) 'massesSpice deallocated'
            deallocate(chargesSpice)
            write(*,*) 'chargesSpice deallocated'

            deallocate(matrix_row_i)
            write(*,*) 'matrix_row_i deallocated'
            deallocate(matrix_col_i)
            write(*,*) 'matrix_col_i deallocated'
            deallocate(matrix_elem)
            write(*,*) 'matrix_elem deallocated'

            deallocate(aP)
            write(*,*) 'aP deallocated'
            
            deallocate(aI)
            write(*,*) 'aI deallocated'
            deallocate(aX)
            write(*,*) 'aX deallocated'
            deallocate(sparseMatrix)
            write(*,*) 'sparseMatrix deallocated'
        endif

        write(*,*) 'Deallocation successful.'
    end subroutine

    subroutine getObjectBounds(nZSpice, nYSpice, objectIds, spiceObjectBounds)
        implicit none
        integer indexOfInt

        integer, intent(in):: nZSpice, nYSpice

        integer, dimension(99), intent(in):: objectIds

        real*8, dimension(4):: spiceObjectBounds

        integer:: z, y
        integer:: minZ, minY, maxZ, maxY, indexOfObjectId

        minZ = nZSpice + 1
        minY = nYSpice + 1
        maxZ = 0
        maxY = 0
        indexOfObjectId = 0

        spiceObjectBounds = (/ 1, 1, nZSpice, nYSpice /)

        do z = 1, nZSpice
            do y = 1, nYSpice
                indexOfObjectId = indexOfInt(objectsEnumSpice(z, y), objectIds, 99)
                if ((indexOfObjectId.ne.-1).and.(objectsEnumSpice(z, y).ne.0)) then
                    if (z.le.minZ) then
                        !    write(*,*) '%    minZ:', minZ
                        minZ = z
                    endif
                    if (y.le.minY) then
                    !    write(*,*) '%    minY:', minY
                        minY = y
                    endif
                    if (z.ge.maxZ) then
                    !    write(*,*) '%    maxZ:', maxZ
                        maxZ = z
                    endif
                    if (y.ge.maxY) then
                    !    write(*,*) '%    maxY:', maxY
                        maxY = y
                    endif
                endif
            enddo
        enddo

        !    write(*,*) '%    Found bounds:', minZ, minY, maxZ, maxY
        if ((minZ.ne.(nZSpice + 1)).and.(minY.ne.(nYSpice + 1)).and.(maxZ.ne.0).and.(maxY.ne.0)) then
            spiceObjectBounds = (/ minZ, minY, maxZ, maxY /)
        endif

    end subroutine getObjectBounds

    subroutine copySpiceData(nZSpice, nYSpice, blockCount, leftObjectIds, rightObjectIds, middleObjectIds, nZ, nY, nZFlux, nYFlux, dX, leftBlockBounds, rightBlockBounds, middleBlockBounds, leftBlockModelPosition, rightBlockModelPosition, middleBlockModelPosition, initTemp, boundaryType, boundaryFluxRight, boundaryFluxBottom, boundaryFluxLeft, boundaryTempRight, boundaryTempBottom, boundaryTempLeft, boundaryPeriodic, boundaryOverwrite, boundaryTopEmissivity)

        implicit none

        ! functions
        integer:: indexOfInt

        ! inputs
        integer, intent(in):: nZSpice, nYSpice

        integer, intent(in):: blockCount
        integer, dimension(99), intent(in):: leftObjectIds, rightObjectIds, middleObjectIds

        integer, intent(in):: nZ, nY, nZFlux, nYFlux
        real*8, intent(in):: dX

        real*8, dimension(4), intent(in):: leftBlockBounds, rightBlockBounds, middleBlockBounds
        real*8, dimension(2), intent(in):: leftBlockModelPosition, rightBlockModelPosition, middleBlockModelPosition

        real*8, intent(in):: initTemp
        integer, intent(in):: boundaryType

        real*8, intent(in):: boundaryFluxRight, boundaryFluxBottom, boundaryFluxLeft, boundaryTempRight, boundaryTempBottom, boundaryTempLeft, boundaryTopEmissivity
        logical, intent(in):: boundaryPeriodic, boundaryOverwrite


        !spiceObjectGroup
        ! spiceObjectGroup
        !    integer objectIds(99) - list of object ids belonging to this object
        !     real*8 positionY
        !     real*8 positionZ
        !        coordinates of the object group within the model grid
        !        coordinates are measured relatively (accepted values 0, 0.5, 1 (both Y, Z))
        !
        ! subroutine variables

        integer:: y, z, zGapleft, zGapright, yGapleft, yGapright, yJoin, yF, zF
        real*8:: copiedFlux, fluxleft, fluxright, aJoin, bJoin
        logical:: blockFound


        gridObjects = 0 !-1
        gridEdges = 0 !-2
        gridEdgeFlags = 0 !-3
        gridObjectFlags = 0 !-4
        gridEdgeFlux = 0 !-5

        cellCount = 0

        y = 0
        z = 0
        zGapleft = 0
        zGapright = 0
        yGapleft = 0
        yGapright = 0

        write(*,*) '......................................................'
        write(*,*) '. Grids loading started                              .'
        write(*,*) '......................................................'


        if (blockCount == 1) then
            write(*,*) 'Block count 1 detected. Placing one block atop the bulk.'

            ! spiceProcessing
            call copyObjectGroup(middleObjectIds, nZ, nY, nZFlux, nYFlux, middleBlockBounds, middleBlockModelPosition, nZSpice, nYSpice)

            bulkNumber = maxval(gridObjects) + 1

            write(*,*) 'Bulk number is ', bulkNumber
            write(*,*) 'middleBlockModelPosition ', middleBlockModelPosition
            do z = 2, nZ + 1
                do y = 2, nY + 1
                    ! if (gridObjects(z, y) == 0 .and. y <= middleBlockModelPosition(2)) then
                    if (gridObjects(z, y) == 0 .and. y > middleBlockModelPosition(2)) then
                        gridObjects(z, y) = bulkNumber
                        gridObjectFlags(z, y) = 1
                    else
                        !write (*,*) 'Found left end at', z, y
                        exit
                    endif
                enddo
            enddo

            ! spiceProcessing
            call findEdges(blockCount, nZ, nY, nZFlux, nYFlux, gridObjects, gridEdges, gridObjectFlags, gridEdgeFlags, gridEdgeFlux)

            ! spiceProcessing
            call processFluxArray(nZFlux, nYFlux, gridEdgeFlux)

            ! spiceProcessing
            call initializeGridT(nZ, nY, gridObjectFlags, gridEdgeFlags, initTemp, gridTInit)

            ! spiceProcessing
            call loadBoundaryConditions(blockCount, initTemp, bulkNumber, boundaryType, boundaryFluxRight, boundaryFluxBottom, boundaryFluxLeft, boundaryTempRight, boundaryTempBottom, boundaryTempLeft, boundaryPeriodic, boundaryOverwrite, boundaryTopEmissivity, nZ, nY, nZFlux, nYFlux)
        elseif (blockCount == 2) then
            write(*,*) 'Block count 2 detected. Placing two blocks in the corners the bulk.'
            ! spiceProcessing
            call copyObjectGroup(leftObjectIds, nZ, nY, nZFlux, nYFlux, leftBlockBounds, leftBlockModelPosition, nZSpice, nYSpice)

            ! spiceProcessing
            call copyObjectGroup(rightObjectIds, nZ, nY, nZFlux, nYFlux, rightBlockBounds, rightBlockModelPosition, nZSpice, nYSpice)


            zGapleft = 1 + leftBlockBounds(3) - leftBlockBounds(1) + 1
            zGapright = rightBlockModelPosition(1) - 1 + 1

            do y = nY + 1, 1, -1
                if ((gridObjectFlags(zGapleft - 1, y + 1) == 0).and.(gridObjectFlags(zGapleft - 1, y) /= 0)) then
                    yGapleft = y
                    exit
                endif
            enddo

            do y = nY + 1, 1, -1
                if ((gridObjectFlags(zGapright + 1, y + 1) == 0).and.(gridObjectFlags(zGapright + 1, y) /= 0)) then
                    yGapright = y
                    exit
                endif
            enddo


            write(*,*) 'Coordinates of the joining line'
            write(*,*) '    left', zGapleft, yGapleft
            write(*,*) '    right', zGapright, yGapright


            aJoin = (yGapleft - yGapright) / (zGapleft - zGapright)
            bJoin = yGapleft - aJoin * zGapleft

            write(*,*) '    y = a * z + b'
            write(*,*) '        a', aJoin
            write(*,*) '        b', bJoin

            blockFound = .false.


            bulkNumber = maxval(gridObjects) + 1

            write(*,*) 'Adding the electrode'
            write(*,*) '    number', bulkNumber

            do z = 2, zGapleft - 1
                do y = 2, nY + 1
                    if (gridObjects(z, y) == 0) then
                        gridObjects(z, y) = bulkNumber
                        gridObjectFlags(z, y) = 1
                    else
                        !write (*,*) 'Found left end at', z, y
                        exit
                    endif
                enddo
            enddo

            do z = zGapleft, zGapright
                yJoin = idint(aJoin * z + bJoin)
                do y = 2, nY + 1
                    if ((gridObjects(z, y) == 0).and.(y <= yJoin)) then
                        gridObjects(z, y) = bulkNumber
                        gridObjectFlags(z, y) = 1
                    else
                        !write (*,*) 'Found middle end at', z, y
                        exit
                    endif
                enddo
            enddo

            do z = zGapright + 1, nZ + 1
                do y = 2, nY + 1
                    if (gridObjects(z, y) == 0) then
                        gridObjects(z, y) = bulkNumber
                        gridObjectFlags(z, y) = 1
                        !write (*,*) 'Found right end at', z, y
                    else
                        exit
                    endif
                enddo
            enddo

            ! spiceProcessing
            call findEdges(blockCount, nZ, nY, nZFlux, nYFlux, gridObjects, gridEdges, gridObjectFlags, gridEdgeFlags, gridEdgeFlux)

            ! spiceProcessing
            call processFluxArray(nZFlux, nYFlux, gridEdgeFlux)


            zF = (zGapleft - 1 - 1) * 2
            yF = (yGapleft - 1) * 2 + 1
            fluxleft = gridEdgeFlux(zF, yF)
            write(*,*) 'Flux', zF, yF

            write(*,*) '    fluxleft', fluxleft

            zF = (zGapright + 1 - 1) * 2
            yF = (yGapright - 1) * 2 + 1
            fluxright = gridEdgeFlux(zF, yF)

            write(*,*) 'Flux', zF, yF
            write(*,*) '    fluxright', fluxright

            copiedFlux = (fluxleft + fluxright) / 2
            write(*,*) '    average', copiedFlux

            y = idint(aJoin * z + bJoin)
            yF = (y - 1) * 2 + 1
            do z = zGapleft, zGapright
                zF = (z - 1) * 2
                gridEdgeFlux(zF, yF) = copiedFlux
                !write(*,*) '        z, y, flux', zF, yF, copiedFlux
            enddo

            ! spiceProcessing
            call initializeGridT(nZ, nY, gridObjectFlags, gridEdgeFlags, initTemp, gridTInit)

            ! spiceProcessing
            call loadBoundaryConditions(blockCount, initTemp, bulkNumber, boundaryType, boundaryFluxRight, boundaryFluxBottom, boundaryFluxLeft, boundaryTempRight, boundaryTempBottom, boundaryTempLeft, boundaryPeriodic, boundaryOverwrite, boundaryTopEmissivity, nZ, nY, nZFlux, nYFlux)
        else
            write(*,*) 'Unsupported block count detected: ', blockCount
            write(*,*) 'Aborting execution'
            stop
        endif

        ! spiceProcessing
        call getMatrixCellCount(nZ, nY, nZFlux, nYFlux)

        write(*,*) '......................................................'
        write(*,*) '. Grids loading finished                             .'
        write(*,*) '......................................................'


    end subroutine copySpiceData

    subroutine copyObjectGroup(blockIds, nZ, nY, nZFlux, nYFlux, blockBounds, blockModelPosition, nZSpice, nYSpice)

        implicit none

        integer:: indexOfInt

        integer, dimension(99), intent(in):: blockIds

        integer, intent(in):: nZ, nY, nZFlux, nYFlux
        real*8, dimension(4), intent(in):: blockBounds
        real*8, dimension(2), intent(in):: blockModelPosition


        integer, intent(in):: nZSpice, nYSpice
        integer:: y, z, yT, zT, yF, zF, yTemp, zTemp
        integer:: id, sumObjects, sumEdges

        y = 0
        z = 0

        write(*,*) '    % Loading block from SPICE data.'
        write(*,*) '    % block model position: ', blockModelPosition
        write(*,*) '    % block bounds: ', blockBounds

        do z = blockBounds(1), blockBounds(3)
            zT = blockModelPosition(1) + (z - blockBounds(1))
            zF = 2 * zT
            zT = zT + 1

            if (zF < 0 .or. zT < 0) then
                write (*,*) 'Negative Z!', zF, zT
                exit
            endif

            do y = blockBounds(2), blockBounds(4)
                yT = blockModelPosition(2) + (y - blockBounds(2))
                yF = 2 * yT
                yT = yT + 1

                if (yF < 0 .or. yT < 0) then
                    write (*,*) 'Negative Y!', yF, yT
                    exit
                endif

                if (z < blockBounds(3) .and. y < blockBounds(4)) then
                    id = objectsEnumSpice(z, y)
                    if (indexOfInt(id, blockIds, 99) /= -1) then
                        sumObjects = 0

                        if (objectsEnumSpice(z, y) > 0) then
                            sumObjects = sumObjects + 1
                        endif
                        if (objectsEnumSpice(z + 1, y) > 0) then
                            sumObjects = sumObjects + 1
                        endif
                        if (objectsEnumSpice(z, y + 1) > 0) then
                            sumObjects = sumObjects + 1
                        endif
                        if (objectsEnumSpice(z + 1, y + 1) > 0) then
                            sumObjects = sumObjects + 1
                        endif

                        sumEdges = edgesSpice(z, y) + edgesSpice(z + 1, y) + edgesSpice(z, y + 1) + edgesSpice(z + 1, y + 1)

                        if (sumObjects == 4) then
                            gridObjects(zT, yT) = id! + 30
                            gridObjectFlags(zT, yT) = 1! + 30
                            gridEdgeFlux(zF, yF) = 2! + 30

                            if (sumEdges > 0) then
                                gridEdges(zT, yT) = id! + 20
                                gridEdgeFlags(zT, yT) = 1! + 20
                                gridEdgeFlux(zF, yF) = 1! + 20
                            endif
                        endif
                    endif
                endif

                gridEdgeFlux(zF - 1, yF - 1) = denormalizedFlux(z, y)

            enddo
        enddo

    end subroutine copyObjectGroup

    subroutine denormalizeEEFlux(nYSpice, nZSpice, nSpecSpice, edgeEnergyFluxSpice, massesSpice, chargesSpice, fluxSource, mksN0, mksTe, mksB, mksMainIonQ, mksMainIonM, denormalizedFlux)
        implicit none

        integer :: indexOfInt
        real*8 :: getDebyeLength, getLarmorFrequency, getMass

        integer, intent(in):: nZSpice, nYSpice, nSpecSpice
        real*8, dimension(nSpecSpice, nZSpice, nYSpice), intent(in):: edgeEnergyFluxSpice
        real*8, dimension(nSpecSpice), intent(in):: massesSpice, chargesSpice

        integer, dimension(99), intent(in):: fluxSource

        real*8, intent(in):: mksN0, mksTe, mksB, mksMainIonQ, mksMainIonM

        real*8, dimension(nZSpice, nYSpice), intent(out):: denormalizedFlux

        real*8:: flux, realFlux, debyeLength, frequency, mass, fluxCoefficient

        integer:: i, y, z

        i = 0
        denormalizedFlux = 0
        fluxCoefficient = 0
        debyeLength = getDebyeLength(mksTe, mksN0)

        frequency = getLarmorFrequency(mksMainIonM, mksB, mksMainIonQ)


        write(*,*) '    % Denormalizing flux.'
        write(*,*) '    %        Frequency:', frequency
        write(*,*) '    %        B:', mksB
        write(*,*) '    %        m:', mksMainIonM
        write(*,*) '    %        q:', mksMainIonQ

        do i = 1, nSpecSpice

            if (indexOfInt(i, fluxSource, 99) == -1) then
                cycle
            endif

            ! the strange number is an attempt to fix flux value when using wrong ion mass (m = 2 in species definition) -- was wrong
            !fluxCoefficient = 0.7243 * mksN0 * getMass(massesSpice(i)) * debyeLength * debyeLength * debyeLength * frequency * frequency * frequency
            fluxCoefficient = mksN0 * getMass(mksMainIonM) * debyeLength * debyeLength * debyeLength * frequency * frequency * frequency

            write(*,*) '    %    Specie flux coefficient:', fluxCoefficient
            write(*,*) '    %        Mass:', getMass(massesSpice(i))
            write(*,*) '    %        Charge:', chargesSpice(i)
            write(*,*) '    %        Debye length:', debyeLength

            do z = 1, nZSpice
                do y = 1, nYSpice
                    flux = edgeEnergyFluxSpice(i, z, y)
                    realFlux = flux * fluxCoefficient
                    denormalizedFlux(z, y) = denormalizedFlux(z, y) + realFlux
                enddo
            enddo

        enddo
        write(*,*) '    % Flux denormalized.'

    end subroutine denormalizeEEFlux

    subroutine findEdges(blockCount, nZ, nY, nZFlux, nYFlux, gridObjects, gridEdges, gridObjectFlags, gridEdgeFlags, gridEdgeFlux)
        implicit none

        integer, intent(in):: blockCount, nZ, nY, nZFlux, nYFlux
        integer, intent(inout), dimension(nZ + 2, nY + 2):: gridObjects, gridEdges, gridObjectFlags, gridEdgeFlags
        real*8, intent(inout), dimension(nZFlux, nYFlux):: gridEdgeFlux

        integer:: y, z, id, yF, zF
        logical:: horizontalFlag, verticalFlag

        write(*,*) '    % Finding object edges.'

        do z = 2, nZ + 1
            do y = 2, nY + 1
                id = gridObjects(z, y)
                if (id /= 0) then
                    horizontalFlag = .false.
                    verticalFlag = .true.

                    yF = (y - 1) * 2
                    zF = (z - 1) * 2

                    if ((gridObjects(z - 1, y) > 0).and.(gridObjects(z + 1, y) > 0).and.(gridObjects(z, y - 1) > 0).and.(gridObjects(z, y + 1) > 0).and.(gridObjects(z - 1, y - 1) > 0).and.(gridObjects(z + 1, y - 1) > 0).and.(gridObjects(z - 1, y + 1) > 0).and.(gridObjects(z + 1, y + 1) > 0)) then
                        gridEdgeFlux(zF, yF) = 2! + 10
                    else
                        gridEdges(z, y) = id
                        if (y == 2 .and. blockCount > 1) then
                            gridEdgeFlags(z, y) = 2! + 10
                        elseif (z == 2 .and. blockCount == 1) then
                            gridEdgeFlags(z, y) = 2! + 10
                        else
                            gridEdgeFlags(z, y) = 1! + 10
                        endif
                        gridEdgeFlux(zF, yF) = 1! + 10
                    endif
                endif
            enddo
        enddo
        write(*,*) '    % Edges found.'

    end subroutine findEdges

    subroutine processFluxArray(nZFlux, nYFlux, gridEdgeFlux)
        implicit none
        integer, intent(in):: nZFlux, nYFlux
        real*8, intent(inout), dimension(nZFlux, nYFlux):: gridEdgeFlux

        integer :: z, y
        real*8 :: flux
        real*8 :: edgeFlagTL, edgeFlagTR, edgeFlagBL, edgeFlagBR

        write(*,*) '    % Calculating real flux.'
        do z = 1, nZFlux, 2
            do y = 1, nYFlux, 2
                flux = gridEdgeFlux(z, y)
                gridEdgeFlux(z, y) = 0

                edgeFlagTL = 0
                edgeFlagTR = 0
                edgeFlagBL = 0
                edgeFlagBR = 0

                if ((z > 1) .and. (y > 1)) then
                    edgeFlagTL = gridEdgeFlux(z - 1, y - 1)
                endif

                if ((z > 1) .and. (y < nYFlux)) then
                    edgeFlagTR = gridEdgeFlux(z - 1, y + 1)
                endif

                if ((z < nZFlux) .and. (y > 1)) then
                    edgeFlagBL = gridEdgeFlux(z + 1, y - 1)
                endif

                if ((z < nZFlux) .and. (y < nYFlux)) then
                    edgeFlagBR = gridEdgeFlux(z + 1, y + 1)
                endif

                if ((edgeFlagTL == 1) .and. (edgeFlagTR == 1) .and. (edgeFlagBR == 1)) then
                    gridEdgeFlux(z, y - 1) = gridEdgeFlux(z, y - 1) - 0.5*flux
                    gridEdgeFlux(z + 1, y) = gridEdgeFlux(z + 1, y) + 0.5*flux
                elseif ((edgeFlagTR == 1) .and. (edgeFlagBR == 1) .and. (edgeFlagBL == 1)) then
                    gridEdgeFlux(z, y - 1) = gridEdgeFlux(z, y - 1) + 0.5*flux
                    gridEdgeFlux(z - 1, y) = gridEdgeFlux(z - 1, y) + 0.5*flux
                elseif ((edgeFlagBR == 1) .and. (edgeFlagBL == 1) .and. (edgeFlagTL == 1)) then
                    gridEdgeFlux(z - 1, y) = gridEdgeFlux(z - 1, y) - 0.5*flux
                    gridEdgeFlux(z, y + 1) = gridEdgeFlux(z, y + 1) + 0.5*flux
                elseif ((edgeFlagBL == 1) .and. (edgeFlagTL == 1) .and. (edgeFlagTR == 1)) then
                    gridEdgeFlux(z, y + 1) = gridEdgeFlux(z, y + 1) - 0.5*flux
                    gridEdgeFlux(z + 1, y) = gridEdgeFlux(z + 1, y) - 0.5*flux

                elseif ((edgeFlagTL == 1) .and. (edgeFlagTR == 1)) then
                    gridEdgeFlux(z, y - 1) = gridEdgeFlux(z, y - 1) - 0.5*flux
                    gridEdgeFlux(z, y + 1) = gridEdgeFlux(z, y + 1) - 0.5*flux
                elseif ((edgeFlagTL == 1) .and. (edgeFlagBL == 1)) then
                    gridEdgeFlux(z - 1, y) = gridEdgeFlux(z - 1, y) - 0.5*flux
                    gridEdgeFlux(z + 1, y) = gridEdgeFlux(z + 1, y) - 0.5*flux
                elseif ((edgeFlagBL == 1) .and. (edgeFlagBR == 1)) then
                    gridEdgeFlux(z, y - 1) = gridEdgeFlux(z, y - 1) + 0.5*flux
                    gridEdgeFlux(z, y + 1) = gridEdgeFlux(z, y + 1) + 0.5*flux
                elseif ((edgeFlagTR == 1) .and. (edgeFlagBR == 1)) then
                    gridEdgeFlux(z - 1, y) = gridEdgeFlux(z - 1, y) + 0.5*flux
                    gridEdgeFlux(z + 1, y) = gridEdgeFlux(z + 1, y) + 0.5*flux

                elseif ((edgeFlagBR == 1) .and. (edgeFlagTL == 1)) then
                    gridEdgeFlux(z - 1, y) = gridEdgeFlux(z - 1, y) - 0.25*flux
                    gridEdgeFlux(z + 1, y) = gridEdgeFlux(z + 1, y) + 0.25*flux
                    gridEdgeFlux(z, y - 1) = gridEdgeFlux(z, y - 1) - 0.25*flux
                    gridEdgeFlux(z, y + 1) = gridEdgeFlux(z, y + 1) + 0.25*flux
                elseif ((edgeFlagTR == 1) .and. (edgeFlagBL == 1)) then
                    gridEdgeFlux(z - 1, y) = gridEdgeFlux(z - 1, y) + 0.25*flux
                    gridEdgeFlux(z + 1, y) = gridEdgeFlux(z + 1, y) - 0.25*flux
                    gridEdgeFlux(z, y - 1) = gridEdgeFlux(z, y - 1) + 0.25*flux
                    gridEdgeFlux(z, y + 1) = gridEdgeFlux(z, y + 1) - 0.25*flux

                elseif (edgeFlagTL == 1) then
                    gridEdgeFlux(z - 1, y) = gridEdgeFlux(z - 1, y) - 0.5*flux
                    gridEdgeFlux(z, y - 1) = gridEdgeFlux(z, y - 1) - 0.5*flux
                elseif (edgeFlagTR == 1) then
                    gridEdgeFlux(z - 1, y) = gridEdgeFlux(z - 1, y) + 0.5*flux
                    gridEdgeFlux(z, y + 1) = gridEdgeFlux(z, y + 1) - 0.5*flux
                elseif (edgeFlagBR == 1) then
                    gridEdgeFlux(z + 1, y) = gridEdgeFlux(z + 1, y) + 0.5*flux
                    gridEdgeFlux(z, y + 1) = gridEdgeFlux(z, y + 1) + 0.5*flux
                elseif (edgeFlagBL == 1) then
                    gridEdgeFlux(z + 1, y) = gridEdgeFlux(z + 1, y) - 0.5*flux
                    gridEdgeFlux(z, y - 1) = gridEdgeFlux(z, y - 1) + 0.5*flux
                endif
            enddo
        enddo


        write(*,*) '    % Real flux calculated.'
    end subroutine processFluxArray

    subroutine initializeGridT(nZ, nY, gridObjectFlags, gridEdgeFlags, initTemp, gridTInit)
        implicit none
        integer, intent(in) :: nZ, nY
        integer, intent(in), dimension(nZ + 2, nY + 2) :: gridObjectFlags, gridEdgeFlags
        real*8, intent(in) :: initTemp
        real*8, intent(inout), dimension(nZ + 2, nY + 2) ::gridTInit

        integer :: z, y, zF, yF, objectFlag, edgeFlag

        gridTInit = 0

        write(*,*) '    % Initializing temperature grid.'
        do z = 1, nZ + 2
            do y = 1, nY + 2
                objectFlag = gridObjectFlags(z, y)
                edgeFlag = gridEdgeFlags(z, y)

                if (objectFlag > 0) then
                    gridTInit(z, y) = initTemp
                    if (edgeFlag == 0) then ! inside object
                        ! no extra conditions added
                    elseif (edgeFlag == 1) then ! edge -- flux boundary condition
                        ! no extra conditions added
                    else if (edgeFlag == 2) then
                        ! add the init temp value to the neighbouring empty cells
                        if ((z > 1).and.(z < nZ + 2).and.(y > 1).and.(y < nY + 2)) then
                            ! only the object, not the bounds
                            if (gridObjectFlags(z - 1, y) == 0) then
                                gridTInit(z - 1, y) = initTemp
                            endif
                            if (gridObjectFlags(z + 1, y) == 0) then
                                gridTInit(z + 1, y) = initTemp
                            endif
                            if (gridObjectFlags(z, y - 1) == 0) then
                                gridTInit(z, y - 1) = initTemp
                            endif
                            if (gridObjectFlags(z, y + 1) == 0) then
                                gridTInit(z, y + 1) = initTemp
                            endif
                        endif
                    else
                        ! this should not happen
                    endif
                endif
            enddo
        enddo

        write(*,*) '    % Temperature grid initialized.'
    end subroutine initializeGridT

    subroutine initMaterialGrid(nZ, nY, gridObjectFlags, gridMaterial, thermalConductivity, specificHeat, density, coating, coatingThermalConductivity, coatingSpecificHeat, coatingDensity)
        implicit none

        integer, intent(in):: nZ, nY

        integer, dimension(nZ + 2, nY + 2), intent(inout):: gridObjectFlags
        real*8, dimension(3, nZ + 2, nY + 2):: gridMaterial
        real*8, intent(in):: thermalConductivity, specificHeat, density
        integer, intent(in) :: coating
        real*8, intent(in):: coatingThermalConductivity, coatingSpecificHeat, coatingDensity

        integer :: z, y

        do z = 2, nZ + 1
            do y = 2, nY + 1
                if (gridObjectFlags(z, y) > 0) then
                    gridMaterial(1, z, y) = thermalConductivity
                    gridMaterial(2, z, y) = specificHeat
                    gridMaterial(3, z, y) = density
                endif
                if (coating > 0 .and. z > 1 + Nz - coating) then
                    gridMaterial(1, z, y) = coatingThermalConductivity
                    gridMaterial(2, z, y) = coatingSpecificHeat
                    gridMaterial(3, z, y) = coatingDensity
                endif
            enddo
        enddo

        write(*,*) '    % Material grid initialized. Coating depth: ', coating
    end subroutine initMaterialGrid

    subroutine getMatrixCellCount(nZ, nY, nZFlux, nYFlux)
        implicit none
        integer, intent(in):: nZ, nY, nZFlux, nYFlux

        real*8 :: centerCoefficient, leftCoefficient, rightCoefficient, topCoefficient, bottomCoefficient, mu, leftT, rightT, topT, bottomT, leftF, rightF, topF, bottomF
        integer :: z, y, zF, yF, edgeFlag, objectFlag

        logical :: use_center, use_left, use_right, use_top, use_bottom

        cellCount = 0

        do z = 1, nZ + 2
            do y = 1, nY + 2
                use_center = .true.
                use_left = .false.
                use_right = .false.
                use_top = .false.
                use_bottom = .false.

                edgeFlag = gridEdgeFlags(z, y)
                objectFlag = gridObjectFlags(z, y)

                if ((objectFlag > 0) .and. ((edgeFlag == 0) .or. (edgeFlag == 2))) then
                    use_center = .true.
                    use_left = .true.
                    use_right = .true.
                    use_top = .true.
                    use_bottom = .true.
                elseif ((objectFlag > 0) .and. (edgeFlag == 1)) then
                    zF = 2*(z - 1)
                    yF = 2*(y - 1)

                    use_center = .true.
                    use_left = .true.
                    use_right = .true.
                    use_top = .true.
                    use_bottom = .true.

                    leftT = gridTInit(z, y - 1)
                    rightT = gridTInit(z, y + 1)
                    topT = gridTInit(z - 1, y)
                    bottomT = gridTInit(z + 1, y)

                    if (leftT == 0) then
                        use_left = .false.
                    endif
                    if (rightT == 0) then
                        use_right = .false.
                    endif
                    if (topT == 0) then
                        use_top = .false.
                    endif
                    if (bottomT == 0) then
                        use_bottom = .false.
                    endif
                endif

                if (use_center) then
                    cellCount = cellCount + 1
                endif
                if (use_left) then
                    cellCount = cellCount + 1
                endif
                if (use_right) then
                    cellCount = cellCount + 1
                endif
                if (use_top) then
                    cellCount = cellCount + 1
                endif
                if (use_bottom) then
                    cellCount = cellCount + 1
                endif
            enddo
        enddo

        return
    end subroutine getMatrixCellCount

    subroutine getSparseMatrixUpdate(nZ, nY, nZFlux, nYFlux, dTime, dX)
        implicit none

        integer, intent(in):: nZ, nY, nZFlux, nYFlux
        real*8, intent(in) :: dTime, dX

        real*8 :: centerCoefficient, leftCoefficient, rightCoefficient, topCoefficient, bottomCoefficient, alpha, mu, leftT, rightT, topT, bottomT, leftF, rightF, topF, bottomF
        integer :: centerMuCount, leftMuCount, rightMuCount, topMuCount, bottomMuCount
        integer :: z, y, zF, yF, edgeFlag, objectFlag, linearIndex, zIndex, centerYIndex, leftYIndex, rightYIndex, topYIndex, bottomYIndex

        integer :: cellIndex, lastYIndex, yIndex, apIndex

        write(*,*) 'Sparse matrix loaded'
    end subroutine getSparseMatrixUpdate

    subroutine getSparseMatrixVectors(nZ, nY, nZFlux, nYFlux, dTime, dX)
        implicit none

        integer, intent(in):: nZ, nY, nZFlux, nYFlux
        real*8, intent(in) :: dTime, dX

        integer, dimension(cellCount, 2) :: muCoefficientArray

        integer, dimension(cellCount, 7) :: sorterArray

        real*8, dimension(cellCount) :: coefficientArrayBackup

        real*8 :: centerCoefficient, leftCoefficient, rightCoefficient, topCoefficient, bottomCoefficient, alpha, mu, leftT, rightT, topT, bottomT, leftF, rightF, topF, bottomF
        integer :: centerMuCount, leftMuCount, rightMuCount, topMuCount, bottomMuCount
        integer :: z, y, zF, yF, edgeFlag, objectFlag, linearIndex, zIndex, centerYIndex, leftYIndex, rightYIndex, topYIndex, bottomYIndex

        integer :: cellIndex, lastYIndex, yIndex, apIndex

        integer, dimension((cellCount + 1)/2, 7) :: tempMatrix

        logical :: use_center, use_left, use_right, use_top, use_bottom


        write(*,*) '    % Getting sparse matrix'
        write(*,*) '    % dT', dTime
        write(*,*) '    % dX', dX

        linearIndex = 1

        matrix_row_i = 0
        matrix_col_i = 0
        matrix_elem = 0
        muCoefficientArray = 0
        coefficientArrayBackup = 0
        aP = 0
        aI = 0
        aX = 0
        sorterArray = 0
        sparseMatrix = 0

        cellIndex = 0
        lastYIndex = 0
        yIndex = 0
        apIndex = 0

        centerMuCount = 0
        leftMuCount = 0
        rightMuCount = 0
        topMuCount = 0
        bottomMuCount = 0

        do z = 1, nZ + 2
            do y = 1, nY + 2
                alpha = gridMaterial(1, z, y) / (gridMaterial(2, z, y) * gridMaterial(3, z, y))

                mu = alpha*dTime/(dX * dX)

                centerCoefficient = 1
                leftCoefficient = 0
                rightCoefficient = 0
                topCoefficient = 0
                bottomCoefficient = 0

                use_center = .true.
                use_left = .false.
                use_right = .false.
                use_top = .false.
                use_bottom = .false.

                centerMuCount = 0
                leftMuCount = 0
                rightMuCount = 0
                topMuCount = 0
                bottomMuCount = 0

                zIndex = getMatrixIndex(z, y, nZ + 2, nY + 2)

                centerYIndex = getMatrixIndex(z,     y,     nZ + 2, nY + 2)
                leftYIndex =   getMatrixIndex(z,     y - 1, nZ + 2, nY + 2)
                rightYIndex =  getMatrixIndex(z,     y + 1, nZ + 2, nY + 2)
                topYIndex =    getMatrixIndex(z - 1, y,     nZ + 2, nY + 2)
                bottomYIndex = getMatrixIndex(z + 1, y,     nZ + 2, nY + 2)

                edgeFlag = gridEdgeFlags(z, y)
                objectFlag = gridObjectFlags(z, y)

                if ((objectFlag > 0) .and. ((edgeFlag == 0) .or. (edgeFlag == 2))) then
                    centerCoefficient = 1 + 4*mu
                    leftCoefficient = - mu
                    rightCoefficient = - mu
                    topCoefficient = - mu
                    bottomCoefficient = - mu

                    centerMuCount = 4
                    leftMuCount = -1
                    rightMuCount = -1
                    topMuCount = -1
                    bottomMuCount = -1

                    use_center = .true.
                    use_left = .true.
                    use_right = .true.
                    use_top = .true.
                    use_bottom = .true.

                elseif ((objectFlag > 0) .and. (edgeFlag == 1)) then
                    zF = 2*(z - 1)
                    yF = 2*(y - 1)

                    centerCoefficient = 1 + 4*mu
                    leftCoefficient = - mu
                    rightCoefficient = - mu
                    topCoefficient = - mu
                    bottomCoefficient = - mu

                    use_center = .true.
                    use_left = .true.
                    use_right = .true.
                    use_top = .true.
                    use_bottom = .true.

                    centerMuCount = 4
                    leftMuCount = -1
                    rightMuCount = -1
                    topMuCount = -1
                    bottomMuCount = -1

                    leftT = gridTInit(z, y - 1)
                    rightT = gridTInit(z, y + 1)
                    topT = gridTInit(z - 1, y)
                    bottomT = gridTInit(z + 1, y)

                    leftF = gridEdgeFlux(zF, yF - 1)
                    rightF = gridEdgeFlux(zF, yF + 1)
                    topF = gridEdgeFlux(zF - 1, yF)
                    bottomF = gridEdgeFlux(zF + 1, yF)

                    if (leftT == 0) then
                        centerCoefficient = centerCoefficient - mu
                        leftCoefficient = 0
                        use_left = .false.

                        centerMuCount = centerMuCount - 1
                        leftMuCount = 0
                    endif
                    if (rightT == 0) then
                        centerCoefficient = centerCoefficient - mu
                        rightCoefficient = 0
                        use_right = .false.

                        centerMuCount = centerMuCount - 1
                        rightMuCount = 0
                    endif
                    if (topT == 0) then
                        centerCoefficient = centerCoefficient - mu
                        topCoefficient = 0
                        use_top = .false.

                        centerMuCount = centerMuCount - 1
                        topMuCount = 0
                    endif
                    if (bottomT == 0) then
                        centerCoefficient = centerCoefficient - mu
                        bottomCoefficient = 0
                        use_bottom = .false.

                        centerMuCount = centerMuCount - 1
                        bottomMuCount = 0
                    endif

                endif

                if (centerCoefficient /= 0) then
                    matrix_row_i(linearIndex) = zIndex
                    matrix_col_i(linearIndex) = centerYIndex
                    matrix_elem(linearIndex) = centerCoefficient
                    coefficientArrayBackup(linearIndex) = centerCoefficient
                    sorterArray(linearIndex,:) = (/ zIndex, centerYIndex, linearIndex, z, y, 1, centerMuCount /)

                    muCoefficientArray(linearIndex,:) = (/1, centerMuCount /)
                    linearIndex = linearIndex + 1


                    !write (*,*) zIndex, centerYIndex
                endif
                if (use_left) then
                    matrix_row_i(linearIndex) = zIndex
                    matrix_col_i(linearIndex) = leftYIndex
                    matrix_elem(linearIndex) = leftCoefficient
                    coefficientArrayBackup(linearIndex) = leftCoefficient
                    sorterArray(linearIndex,:) = (/ zIndex, leftYIndex, linearIndex, z, y, 0, leftMuCount /)

                    muCoefficientArray(linearIndex,:) = (/0, leftMuCount /)
                    linearIndex = linearIndex + 1
                    !write (*,*) zIndex, leftYIndex
                endif
                if (use_right) then
                    matrix_row_i(linearIndex) = zIndex
                    matrix_col_i(linearIndex) = rightYIndex
                    matrix_elem(linearIndex) = rightCoefficient
                    coefficientArrayBackup(linearIndex) = rightCoefficient
                    sorterArray(linearIndex,:) = (/ zIndex, rightYIndex, linearIndex, z, y, 0, rightMuCount /)

                    muCoefficientArray(linearIndex,:) = (/0, rightMuCount /)
                    linearIndex = linearIndex + 1
                    !write (*,*) zIndex, rightYIndex
                endif
                if (use_top) then
                    matrix_row_i(linearIndex) = zIndex
                    matrix_col_i(linearIndex) = topYIndex
                    matrix_elem(linearIndex) = topCoefficient
                    coefficientArrayBackup(linearIndex) = topCoefficient
                    sorterArray(linearIndex,:) = (/ zIndex, topYIndex, linearIndex, z, y, 0, topMuCount /)

                    muCoefficientArray(linearIndex,:) = (/0, topMuCount /)
                    linearIndex = linearIndex + 1
                    !write (*,*) zIndex, topYIndex
                endif
                if (use_bottom) then
                    matrix_row_i(linearIndex) = zIndex
                    matrix_col_i(linearIndex) = bottomYIndex
                    matrix_elem(linearIndex) = bottomCoefficient
                    coefficientArrayBackup(linearIndex) = bottomCoefficient
                    sorterArray(linearIndex,:) = (/ zIndex, bottomYIndex, linearIndex, z, y, 0, bottomMuCount /)

                    muCoefficientArray(linearIndex,:) = (/0, bottomMuCount /)
                    linearIndex = linearIndex + 1
                    !write (*,*) zIndex, bottomYIndex
                endif
            enddo
        enddo
     !
     !     write(*,*) 'unsorted'
     !     write(*,*) sorterArray
     !
     !     write(*,*) 'sorted'
        write(*,*) 'Sparse matrix sorting', cellCount
        !call insertSortInteger(cellCount, 3, sorterArray, 2, (/ 2, 1/), 1)
        call mergeSortInteger(cellCount, 3, sorterArray, tempMatrix, 2, (/ 2, 1/), 1)
     !     write(*,*) sorterArray
        sparseMatrix = sorterArray

        lastYIndex = 1;
        apIndex = 2;

        write(*,*) 'Sparse matrix sorted'
        do cellIndex = 1, cellCount
            yIndex = sparseMatrix(cellIndex, 2)

            if (yIndex /= lastYIndex) then
                aP(apIndex) = int8(cellIndex - 1)
                apIndex = apIndex + 1
            endif

            lastYIndex = yIndex
            aI(cellIndex) = int8(sparseMatrix(cellIndex, 1) - 1)
            aX(cellIndex) = matrix_elem(sparseMatrix(cellIndex, 3))

            muCoefficientArray(cellIndex,:) = sparseMatrix(cellIndex, 6:7)
        enddo

        aP((nZ + 2)*(nY + 2) + 1) = cellCount

        write(*,*) 'Sparse matrix loaded'
    end subroutine getSparseMatrixVectors

    real*8 function getDeltaT(flux, density, specificHeat, dTime, dX, emissivity, pointT, backgroundT)
        implicit none
        real*8 :: sigma
        real*8, intent (in) :: flux, density, specificHeat, dTime, dX, emissivity, pointT, backgroundT

        sigma = 5.670367e-8; !5.670367(13)×10−8 W⋅m−2⋅K−4.[5]

        getDeltaT = ((flux - emissivity * sigma * (pointT * pointT * pointT * pointT) + (1 - emissivity) * sigma * (backgroundT * backgroundT * backgroundT * backgroundT) ) / (density * specificHeat)) * (dTime / dX)

        return
    end function getDeltaT

    subroutine getRightSide(nZ, nY, nZFlux, nYFlux, boundaryPeriodic, dTime, dX, initTemp, stepIndex, bulkIndex, realStepCount, gridT, enableScaling, scalingFunction, rightSide, deltaTData)
        implicit none

        integer, intent(in):: nZ, nY, nZFlux, nYFlux
        logical, intent(in) :: boundaryPeriodic
        real*8, intent(in) :: dTime, dX, initTemp
        integer, intent(in) :: stepIndex, bulkIndex, realStepCount
        real*8, dimension(nZ +2 , nY + 2), intent(in):: gridT

        logical, intent(in) :: enableScaling
        real*8, dimension(realStepCount) :: scalingFunction

        real*8, intent(out), dimension((nZ + 2)*(nY + 2)):: rightSide, deltaTData


        integer :: edgeFlag, objectFlag, matrixIndex, fluxZ, fluxY, z, y

        real*8 :: leftT, rightT, topT, bottomT
        real*8 :: leftF, rightF, topF, bottomF
        real*8 :: flux, deltaTemp, sigmabulkIndex
        integer :: nTiles

        rightSide = 0;
        deltaTData = 0;

        write(*,*) '    % Generating right equation side.'
        write(*,*) '    % nZ, nY:', nZ, nY

        do z = 1, nZ + 2
            do y = 1, nY + 2
                edgeFlag = gridEdges(z, y)
                objectFlag = gridObjects(z, y)

                matrixIndex = getMatrixIndex(z, y, nZ + 2, nY + 2)

                if ((objectFlag > 0).and.(edgeFlag > 0)) then
                    fluxZ = 2 * (z - 1);
                    fluxY = 2 * (y - 1);


                    leftT = gridT(z, y - 1);
                    rightT = gridT(z, y + 1);
                    topT = gridT(z - 1, y);
                    bottomT = gridT(z + 1, y);

                    leftF = gridEdgeFlux(fluxZ, fluxY - 1);
                    rightF = gridEdgeFlux(fluxZ, fluxY + 1);
                    topF = gridEdgeFlux(fluxZ - 1, fluxY);
                    bottomF = gridEdgeFlux(fluxZ + 1, fluxY);

                    flux = 0
                    nTiles = 0

                    if ((leftT == 0).and.(leftF /= 0)) then
                        flux = flux + leftF
                        nTiles = nTiles + 1
                    endif
                    if ((rightT == 0).and.(rightF /= 0)) then
                        flux = flux - rightF
                        nTiles = nTiles + 1
                    endif
                    if ((topT == 0).and.(topF /= 0)) then
                        flux = flux + topF
                        nTiles = nTiles + 1
                    endif
                    if ((bottomT == 0).and.(bottomF /= 0)) then
                        flux = flux - bottomF
                        nTiles = nTiles + 1
                    endif

                    if (enableScaling .and. objectFlag .ne. bulkIndex) flux = flux * scalingFunction(stepIndex)

                    deltaTemp = getDeltaT(flux, gridMaterial(3, z, y), gridMaterial(2, z, y), dTime, dX, gridEmissivity(z, y), gridT(z, y), initTemp)
                    !write(*,*) 'dTemp', deltaTemp, fluxZ, fluxY, gridTCurrent(z, y), gridTCurrent(z, y) + deltaTemp
                    rightSide(matrixIndex) = gridT(z, y) + deltaTemp
                    deltaTData(matrixIndex) = deltaTemp
                else
                    rightSide(matrixIndex) = gridT(z, y)
                endif


                if (boundaryPeriodic) then
                    if (z == 2) then
                        rightSide(matrixIndex) = gridT(nZ - 1, y)
                    else if (z == nZ - 1) then
                        rightSide(matrixIndex) = gridT(2, y)
                    endif
                endif

            enddo
        enddo

    end subroutine getRightSide

    subroutine loadBoundaryConditions(blockCount, initTemp, bulkNumber, boundaryType, boundaryFluxRight, boundaryFluxBottom, boundaryFluxLeft, boundaryTempRight, boundaryTempBottom, boundaryTempLeft, boundaryPeriodic, boundaryOverwrite, boundaryTopEmissivity, nZ, nY, nZFlux, nYFlux)
        implicit none

        integer, intent(in) :: blockCount
        real*8, intent(in):: initTemp
        integer, intent(in):: bulkNumber, boundaryType

        real*8, intent(in):: boundaryFluxRight, boundaryFluxBottom, boundaryFluxLeft, boundaryTempRight, boundaryTempBottom, boundaryTempLeft, boundaryTopEmissivity
        logical, intent(in):: boundaryPeriodic, boundaryOverwrite

        integer, intent(in):: nZ, nY, nZFlux, nYFlux

        integer :: z, y

        z = 0
        y = 0

        write (*,*) 'Setting boundary conditions'

        if (boundaryType == 1) then
            write (*,*) 'Using flux boundary conditions'
        else if (boundaryType == 2) then
            write (*,*) 'Using temperature boundary conditions'
        else if (boundaryType == 3) then
            write (*,*) 'Using combined boundary conditions'
        else
            write (*,*) 'No particular boundary type was set. Using default.'
        endif

        if (blockCount == 1) then
            ! right
            y = 2
            do z = 2, nZ + 1
                if ((boundaryOverwrite == .true. .and. gridObjects(z, y) > 0) .or. (boundaryOverwrite == .false. .and. gridObjects(z, y) == bulkNumber)) then
                    if (boundaryType == 1 .or. boundaryType == 3) then
                        gridEdgeFlags(z, y) = 1
                        gridTInit(z, y - 1) = 0
                        if (boundaryFluxRight /= 0) then
                            gridEdgeFlux((z - 1) * 2, (y - 1) * 2 - 1) = boundaryFluxRight
                        endif
                    else if (boundaryType == 2) then
                        gridEdgeFlags(z, y) = 2
                        gridTInit(z, y - 1) = boundaryTempRight
                    endif
                endif
            enddo

            ! left
            y = nY + 1
            do z = 2, nZ + 1
                if ((boundaryOverwrite == .true. .and. gridObjects(z, y) > 0) .or. (boundaryOverwrite == .false. .and. gridObjects(z, y) == bulkNumber)) then
                    if (boundaryType == 1 .or. boundaryType == 3) then
                        gridEdgeFlags(z, y) = 1
                        gridTInit(z, y + 1) = 0
                        if (boundaryFluxLeft /= 0) then
                            gridEdgeFlux((z - 1) * 2 , (y - 1) * 2 + 1) =  - boundaryFluxLeft
                        endif
                    else if (boundaryType == 2) then
                        gridEdgeFlags(z, y) = 2
                        gridTInit(z, y + 1) = boundaryTempLeft
                    endif
                endif
            enddo

            ! bottom
            z = 2
            do y = 2, nY + 1
                if ((boundaryOverwrite == .true. .and. gridObjects(z, y) > 0) .or. (boundaryOverwrite == .false. .and. gridObjects(z, y) == bulkNumber)) then
                    if (boundaryType == 1) then
                        gridEdgeFlags(z, y) = 1
                        gridTInit(z - 1, y) = 0
                        if (boundaryFluxBottom /= 0) then
                            gridEdgeFlux((z - 1) * 2 - 1, (y - 1) * 2) = boundaryFluxBottom
                        endif
                    else if (boundaryType == 2 .or. boundaryType == 3) then
                        gridEdgeFlags(z, y) = 2
                        gridTInit(z - 1, y) = boundaryTempBottom
                    endif
                endif
            enddo

            ! if the boundary condition is se to have constant temperature somewhere, we need to take care of the corners
            if (boundaryType == 2 .or. boundaryType == 3) then
                gridTInit(2, 1) = boundaryTempBottom
                gridTInit(2, nY + 2) = boundaryTempBottom
            endif

            do y = 2, nY + 1
                do z = nZ + 1, 2, -1
                    if (gridEdgeFlags(z, y) > 0) then
                        gridEmissivity(z, y) = boundaryTopEmissivity
                        exit
                    endif
                enddo
            enddo
        else

            ! right
            z = 2
            do y = 2, nY + 1
                if ((boundaryOverwrite == .true. .and. gridObjects(z, y) > 0) .or. (boundaryOverwrite == .false. .and. gridObjects(z, y) == bulkNumber)) then
                    if (boundaryType == 1 .or. boundaryType == 3) then
                        gridEdgeFlags(z, y) = 1
                        gridTInit(z - 1, y) = 0
                        if (boundaryFluxRight /= 0) then
                            gridEdgeFlux((z - 1) * 2 - 1, (y - 1) * 2) = boundaryFluxRight
                        endif
                    else if (boundaryType == 2) then
                        gridEdgeFlags(z, y) = 2
                        gridTInit(z - 1, y) = boundaryTempRight
                    endif
                endif
            enddo

            ! left
            z = nZ + 1
            do y = 2, nY + 1
                if ((boundaryOverwrite == .true. .and. gridObjects(z, y) > 0) .or. (boundaryOverwrite == .false. .and. gridObjects(z, y) == bulkNumber)) then
                    if (boundaryType == 1 .or. boundaryType == 3) then
                        gridEdgeFlags(z, y) = 1
                        gridTInit(z + 1, y) = 0
                        if (boundaryFluxLeft /= 0) then
                            gridEdgeFlux((z - 1) * 2 + 1, (y - 1) * 2) =  - boundaryFluxLeft
                        endif
                    else if (boundaryType == 2) then
                        gridEdgeFlags(z, y) = 2
                        gridTInit(z + 1, y) = boundaryTempLeft
                    endif
                endif
            enddo

            ! bottom
            y = 2
            do z = 2, nZ + 1
                if ((boundaryOverwrite == .true. .and. gridObjects(z, y) > 0) .or. (boundaryOverwrite == .false. .and. gridObjects(z, y) == bulkNumber)) then
                    if (boundaryType == 1) then
                        gridEdgeFlags(z, y) = 1
                        gridTInit(z, y - 1) = 0
                        if (boundaryFluxBottom /= 0) then
                            gridEdgeFlux((z - 1) * 2, (y - 1) * 2 - 1) = boundaryFluxBottom
                        endif
                    else if (boundaryType == 2 .or. boundaryType == 3) then
                        gridEdgeFlags(z, y) = 2
                        gridTInit(z, y - 1) = boundaryTempBottom
                    endif
                endif
            enddo

            ! if the boundary condition is se to have constant temperature somewhere, we need to take care of the corners
            if (boundaryType == 2 .or. boundaryType == 3) then
                gridTInit(1, 2) = boundaryTempBottom
                gridTInit(nZ + 2, 2) = boundaryTempBottom
            endif
        endif
    end subroutine loadBoundaryConditions

    subroutine add_cooling_pipe(pipe_y, pipe_z, pipe_r, pipe_T)
        implicit none
        integer, intent(in) :: pipe_y, pipe_z, pipe_r
        real*8, intent(in) :: pipe_T

        integer :: z, y
        integer :: sum_flags
        real*8 :: r

        write(*,*) 'Adding cooling pipe at ', pipe_z, pipe_y, 'r = ', pipe_r

        do y = pipe_y - pipe_r - 1, pipe_y + pipe_r + 1
            do z = pipe_z - pipe_r - 1, pipe_z + pipe_r + 1
                if (z > 0 .and. y > 0 .and. z <= dim_z .and. y <= dim_y) then
                    r = sqrt(dble(z - pipe_z) * dble(z - pipe_z) + dble(y - pipe_y) * dble(y - pipe_y))
                    if (r .le. pipe_r) then
                        gridObjectFlags(z, y) = 0
                        gridObjects(z, y) = 0
                        gridTInit(z, y) = pipe_T
                        gridEdgeFlux((z - 1) * 2, (y - 1) * 2) = 0
                    endif
                endif
            enddo
        enddo
        
        do y = pipe_y - pipe_r - 1, pipe_y + pipe_r + 1
            do z = pipe_z - pipe_r - 1, pipe_z + pipe_r + 1
                if (z > 1 .and. y > 1 .and. z <= dim_z - 1 .and. y <= dim_y - 1) then
                    sum_flags = gridObjectFlags(z - 1, y) + gridObjectFlags(z + 1, y) + gridObjectFlags(z, y - 1) + gridObjectFlags(z, y + 1)
                    if (sum_flags > 0 .and. sum_flags < 4) then
                        gridEdges(z, y) = 1
                        gridEdgeFlags(z, y) = 2
                        gridEdgeFlux((z - 1) * 2, (y - 1) * 2) = 1
                    endif
                endif
            enddo
        enddo
    end subroutine add_cooling_pipe
end module