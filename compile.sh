#!/bin/bash

source config/environment.sh

FILES="src/utils.f90 src/HESS.f90 src/namelistRead.f90 src/getoptions.f90 src/matlabInterface.f90 src/spice_processing.f90 src/HESolver.f90 src/dataLoad.f90"

BIN_FILE=HESS-1-5.bin

mkdir -p bin



CMD="$FC -CB -o bin/$BIN_FILE $FILES $LIB_CLOCK $LIB_UMFPACK $LIB_MATIO $LIB_OTHERS $INC_MATIO"


echo $CMD
eval $CMD