#!/bin/bash

HESS_BIN=../bin/hes-2.0.bin
HESS_IN=w-W.hin

NP=4
ODIR=./out/$1/$NP
rm -rf $ODIR
mkdir -p $ODIR

CMD="mpirun --outfile-pattern=$ODIR/cr-W.log.%r -np $NP $HESS_BIN -i $HESS_IN -o $ODIR/cr-W -T cr-t.mat -O cr-o.mat > $ODIR/cr-W.log"

echo $CMD
eval $CMD
