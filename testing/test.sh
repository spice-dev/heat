#!/bin/bash

HESS_BIN=../bin/hes-2.0.bin
HESS_IN=cr-W.hin
HESS_IN=cr-W-short.hin
HESS_IN=cr-W-wide.hin

NP=32
ODIR=./out/$1/$NP
rm -rf $ODIR
mkdir -p $ODIR

CMD="mpirun --outfile-pattern=$ODIR/cr-W.log.%r -np $NP $HESS_BIN -i $HESS_IN -o $ODIR/cr-W -T cr-t.mat -O cr-o.mat > $ODIR/cr-W.log"

echo $CMD
eval $CMD

NP=24
ODIR=./out/$1/$NP
rm -rf $ODIR
mkdir -p $ODIR

CMD="mpirun --outfile-pattern=$ODIR/cr-W.log.%r -np $NP $HESS_BIN -i $HESS_IN -o $ODIR/cr-W -T cr-t.mat -O cr-o.mat > $ODIR/cr-W.log"

echo $CMD
eval $CMD

NP=16
ODIR=./out/$1/$NP
rm -rf $ODIR
mkdir -p $ODIR

CMD="mpirun --outfile-pattern=$ODIR/cr-W.log.%r -np $NP $HESS_BIN -i $HESS_IN -o $ODIR/cr-W -T cr-t.mat -O cr-o.mat > $ODIR/cr-W.log"

echo $CMD
eval $CMD

NP=8
ODIR=./out/$1/$NP
rm -rf $ODIR
mkdir -p $ODIR

CMD="mpirun --outfile-pattern=$ODIR/cr-W.log.%r -np $NP $HESS_BIN -i $HESS_IN -o $ODIR/cr-W -T cr-t.mat -O cr-o.mat > $ODIR/cr-W.log"

echo $CMD
eval $CMD

NP=4
ODIR=./out/$1/$NP
rm -rf $ODIR
mkdir -p $ODIR

CMD="mpirun --outfile-pattern=$ODIR/cr-W.log.%r -np $NP $HESS_BIN -i $HESS_IN -o $ODIR/cr-W -T cr-t.mat -O cr-o.mat > $ODIR/cr-W.log"

echo $CMD
eval $CMD

NP=2
ODIR=./out/$1/$NP
rm -rf $ODIR
mkdir -p $ODIR

CMD="mpirun --outfile-pattern=$ODIR/cr-W.log.%r -np $NP $HESS_BIN -i $HESS_IN -o $ODIR/cr-W -T cr-t.mat -O cr-o.mat > $ODIR/cr-W.log"

echo $CMD
eval $CMD


NP=1
ODIR=./out/$1/$NP
rm -rf $ODIR
mkdir -p $ODIR

CMD="mpirun --outfile-pattern=$ODIR/cr-W.log.%r -np $NP $HESS_BIN -i $HESS_IN -o $ODIR/cr-W -T cr-t.mat -O cr-o.mat > $ODIR/cr-W.log"

echo $CMD
eval $CMD
