#!/bin/bash

module purge
module load intelstudio/15
module load openmpi/1.10.2-intel15

LIB_PREFIX=/scratch/podolnik/spice2/externals

LIB_CLOCK="$LIB_PREFIX/lib/libclock.a"
LIB_PREFIX_UF=$LIB_PREFIX/_source/UF
LIB_UMFPACK="$LIB_PREFIX_UF/UMFPACK/Demo/umf4_f77wrapper.o $LIB_PREFIX_UF/UMFPACK/Lib/libumfpack.a $LIB_PREFIX_UF/AMD/Lib/libamd.a $LIB_PREFIX/_source/blas/OpenBLAS-0.2.20/libopenblas.a $LIB_PREFIX/_source/f2c/f2c/libf2c.a"
LIB_MATIO="$LIB_PREFIX/_source/matio/matio-1.3.4/src/.libs/libmatio.a"
LIB_OTHERS="-lz"

INC_MATIO="-I$LIB_PREFIX/_source/matio/matio-1.3.4/src"

FC=ifort
