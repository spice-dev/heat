# you should source config/environment.sh and config/output.sh before running makefile

# all library definitions
include ./config/Makefile.inc

SRC_DIR=src
BIN_DIR=bin
BIN_FILE=hes-2.0.bin

SRC_FILES=src/utils.f90 src/HESS.f90 src/namelistRead.f90 src/matlabInterface.f90 src/dataLoad.f90
OBJ_FILES=src/getoptions.o src/spice_processing.o src/spice_types.o src/he_parallel_solver.o src/HESolver.o

ALL_LIBS=$(CLOCK_LIB) $(UMFPACK_LIBS_BASIC) $(MUMPS_LIBS) $(MATH_LIBS) $(MATIO_LIBS) $(MEMUSAGE_LIB)

clean:
	rm src/*.mod
	rm src/*.o
	rm $(BIN_DIR)/$(BIN_FILE)

%.mod: src/%.f90
	$(MPIF) $(MPIF_FLAGS_RELEASE) $(MPIF_INC) -c -o src/$*.o -module src $<

release: spice_types.mod getoptions.mod spice_processing.mod he_parallel_solver.mod HESolver.mod
	$(MPIF) $(MPIF_FLAGS_RELEASE) $(MPIF_INC) -I./src $(SRC_FILES) $(OBJ_FILES) $(ALL_LIBS) -o $(BIN_DIR)/$(BIN_FILE)

debug: spice_types.mod getoptions.mod spice_processing.mod he_parallel_solver.mod HESolver.mod
	$(MPIF) $(MPIF_FLAGS_RELEASE) $(MPIF_INC) -I./src $(SRC_FILES) $(OBJ_FILES) $(ALL_LIBS) -o $(BIN_DIR)/$(BIN_FILE)
